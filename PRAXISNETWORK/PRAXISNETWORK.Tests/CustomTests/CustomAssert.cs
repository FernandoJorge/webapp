﻿using PagedList;
using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRAXISNETWORK.Tests.CustomTests
{
    public class CustomAssert
    {
        public static Boolean verifyPropoposalInfoRecordsIds(List<PRAXISNETWORK.Models.ProposalInfo> actualRecords, List<PRAXISNETWORK.Models.Proposal> expectRecords)
        {
            List<Boolean> status = null;

            foreach (Proposal expRecord in expectRecords)
            {
                status = new List<Boolean>();

                foreach (PRAXISNETWORK.Models.ProposalInfo actRecord in actualRecords)
                {
                    if (expRecord.id == actRecord.id)
                        status.Add(true);
                    else status.Add(false);
                }

                if (!status.Contains(true))
                    return false;
            }

            return true;
        }

        public static Boolean verifyAllProvidersRecordsIds(PagedList<PRAXISNETWORK.Models.AllProviders> actualRecords, List<PRAXISNETWORK.Models.User> expectRecords)
        {
            List<Boolean> status = null;

            foreach (User expRecord in expectRecords)
            {
                status = new List<Boolean>();

                foreach (PRAXISNETWORK.Models.AllProviders actRecord in actualRecords)
                {
                    if (expRecord.id == actRecord.id)
                        status.Add(true);
                    else status.Add(false);
                }

                if (!status.Contains(true))
                    return false;
            }

            return true;
        }

        public static Boolean verifyAllStudentsRecordsIds(PagedList<PRAXISNETWORK.Models.AllStudents> actualRecords, List<PRAXISNETWORK.Models.User> expectRecords)
        {
            List<Boolean> status = null;

            foreach (User expRecord in expectRecords)
            {
                status = new List<Boolean>();

                foreach (PRAXISNETWORK.Models.AllStudents actRecord in actualRecords)
                {
                    if (expRecord.id == actRecord.id)
                        status.Add(true);
                    else status.Add(false);
                }

                if (!status.Contains(true))
                    return false;
            }

            return true;
        }

        public static Boolean verifyAllProposalApplicationsRecordsIds(PagedList<PRAXISNETWORK.Models.ProposalApplications> actualRecords, List<PRAXISNETWORK.Models.Application> expectRecords)
        {
            List<Boolean> status = null;

            foreach (Application expRecord in expectRecords)
            {
                status = new List<Boolean>();

                foreach (PRAXISNETWORK.Models.ProposalApplications actRecord in actualRecords)
                {
                    if (expRecord.proposalId == actRecord.proposalId && expRecord.userId == actRecord.userId)
                        status.Add(true);
                    else status.Add(false);
                }

                if (!status.Contains(true))
                    return false;
            }

            return true;
        }

        public static Boolean verifyAllProviderProposalsRecordsIds(PagedList<PRAXISNETWORK.Models.ProviderProposals> actualRecords, List<PRAXISNETWORK.Models.Proposal> expectRecords)
        {
            List<Boolean> status = null;

            foreach (Proposal expRecord in expectRecords)
            {
                status = new List<Boolean>();

                foreach (PRAXISNETWORK.Models.ProviderProposals actRecord in actualRecords)
                {
                    if (expRecord.id == actRecord.id)
                        status.Add(true);
                    else status.Add(false);
                }

                if (!status.Contains(true))
                    return false;
            }

            return true;
        }

        public static Boolean verifyAllStudentApplicationsRecordsIds(PagedList<PRAXISNETWORK.Models.StudentApplications> actualRecords, List<PRAXISNETWORK.Models.Application> expectRecords)
        {
            List<Boolean> status = null;

            foreach (Application expRecord in expectRecords)
            {
                status = new List<Boolean>();

                foreach (PRAXISNETWORK.Models.StudentApplications actRecord in actualRecords)
                {
                    if (expRecord.proposalId == actRecord.id)
                        status.Add(true);
                    else status.Add(false);
                }

                if (!status.Contains(true))
                    return false;
            }

            return true;
        }
    }
}
