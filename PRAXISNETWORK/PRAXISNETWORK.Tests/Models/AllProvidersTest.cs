﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PRAXISNETWORK;
using PRAXISNETWORK.Models;
using PagedList;

namespace PRAXISNETWORK.Tests.Models
{
    [TestClass]
    public class AllProvidersTest
    {
        [TestMethod]
        public void AllProviders_Test()
        {
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<User> providers = from users in db.Users
                                          where users.userTypeId == 1
                                          select users;

            List<User> allProviders = null;

            try
            {
                allProviders = providers.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Class: AllProvidersTest\nMethod: AllProvidersTest\nErrorMessage: \n" + ex.InnerException.Message);
                return;
            }

            PRAXISNETWORK.Models.AllProviders actualProviders = new PRAXISNETWORK.Models.AllProviders();
            actualProviders.page = 1;
            actualProviders.pageSize = allProviders.Count();

            
            if (allProviders.Count() > 0)
            {
                // Type test
                Assert.IsInstanceOfType(actualProviders.allProviders(), typeof(PagedList<AllProviders>), "[ERROR MESSAGE => TYPE OF OBJECT INVALID]");

                // The same number records test
                Assert.AreEqual(Convert.ToString(allProviders.Count()), Convert.ToString(actualProviders.allProviders().Count()), "[ERROR MESSAGE => NUMBER OF RECORDS INCORRECT]");

                // The same providers´ records identification
                Assert.IsTrue(CustomTests.CustomAssert.verifyAllProvidersRecordsIds(actualProviders.allProviders(), allProviders), "[ERROR MESSAGE => PROVIDERS´ RECORDS INFORMATION MISMATCH]");
            }

        }
    }
}
