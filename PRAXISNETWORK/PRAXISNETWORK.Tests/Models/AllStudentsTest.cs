﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PRAXISNETWORK;
using PRAXISNETWORK.Models;
using PagedList;

namespace PRAXISNETWORK.Tests.Models
{
    [TestClass]
    public class AllStudentsTest
    {
        [TestMethod]
        public void AllStudents_Test()
        {
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<User> students = from users in db.Users
                                         where users.userTypeId == 2
                                         select users;

            List<User> allStudents = null;

            try
            {
                allStudents = students.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Class: AllStudentsTest\nMethod: AllStudentsTest\nErrorMessage: \n" + ex.InnerException.Message);
                return;
            }

            PRAXISNETWORK.Models.AllStudents actualStudents = new PRAXISNETWORK.Models.AllStudents();
            actualStudents.page = 1;
            actualStudents.pageSize = allStudents.Count();

            if (allStudents.Count() > 0)
            {
                // Type test
                Assert.IsInstanceOfType(actualStudents.allProviders(), typeof(PagedList<AllStudents>), "[ERROR MESSAGE => TYPE OF OBJECT INVALID]");

                // The same number records test
                Assert.AreEqual(Convert.ToString(allStudents.Count()), Convert.ToString(actualStudents.allProviders().Count()), "[ERROR MESSAGE => NUMBER OF RECORDS INCORRECT]");

                // The same students´ records identification
                Assert.IsTrue(CustomTests.CustomAssert.verifyAllStudentsRecordsIds(actualStudents.allProviders(), allStudents), "[ERROR MESSAGE => STUDENTS´ RECORDS INFORMATION MISMATCH]");
            }
        }
    }
}
