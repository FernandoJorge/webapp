﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PRAXISNETWORK;
using PRAXISNETWORK.Models;
using PagedList;

namespace PRAXISNETWORK.Tests.Models
{
    [TestClass]
    public class ProposalApplicationsTest
    {
        [TestMethod]
        public void allProposalApplications()
        {
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<Proposal> allProposals = from props in db.Proposals
                                                 select props;

            IEnumerable<User> allApplicants = from appts in db.Users
                                              where appts.userTypeId == 2
                                              select appts;

            int iteration = 0;

            if (allProposals.ToList() != null && allApplicants.ToList() != null && allProposals.Count() > 0 && allApplicants.Count() > 0)
            {
                foreach (Proposal proposal in allProposals)
                    foreach (User applicant in allApplicants)
                    {
                        IEnumerable<Application> userProposalApplications = from upas in db.Applications
                                                                            where upas.proposalId == proposal.id && upas.userId == applicant.id
                                                                            select upas;

                        List<Application> allUserProposalApplications = null;

                        try
                        {
                            allUserProposalApplications = userProposalApplications.ToList();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Class: ProposalApplicationsTest\nMethod: allProposalApplications\nErrorMessage: \n" + ex.InnerException.Message);
                            return;
                        }

                        ProposalApplications actualProposalApplications = new ProposalApplications();
                        actualProposalApplications.page = 1;
                        actualProposalApplications.pageSize = allUserProposalApplications.Count();
                        actualProposalApplications.proposalId = proposal.id;
                        actualProposalApplications.userId = applicant.id;

                        if (allUserProposalApplications.Count() > 0)
                        {
                            // Type test
                            Assert.IsInstanceOfType(actualProposalApplications.allProposalApplications(), typeof(PagedList<ProposalApplications>), "[ERROR MESSAGE => TYPE OF OBJECT INVALID; ITERATION = " + iteration + "]");

                            // The same number records test
                            Assert.AreEqual(Convert.ToString(allUserProposalApplications.Count()), Convert.ToString(actualProposalApplications.allProposalApplications().Count()), "[ERROR MESSAGE => NUMBER OF RECORDS INCORRECT; ITERATION = " + iteration + "]");

                            // The same proposal applications´ records identification
                            Assert.IsTrue(CustomTests.CustomAssert.verifyAllProposalApplicationsRecordsIds(actualProposalApplications.allProposalApplications(), allUserProposalApplications), "[ERROR MESSAGE => PROPOSALS APPLICATIONS´ RECORDS INFORMATION MISMATCH; ITERATION = " + (iteration++) + "]");
                        }
                    }
            }
        }
    }
}
