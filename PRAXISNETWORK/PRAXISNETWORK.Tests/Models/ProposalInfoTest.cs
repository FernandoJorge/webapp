﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PRAXISNETWORK;

namespace PRAXISNETWORK.Tests.Models
{
    [TestClass]
    public class ProposalInfoTest
    {
        [TestMethod]
        public void AllProposalsTest()
        {
            PRAXISNETWORK.Models.PraxisDBEntities db = new PRAXISNETWORK.Models.PraxisDBEntities();

            IEnumerable<PRAXISNETWORK.Models.Proposal> proposals = from props in db.Proposals
                                                                   select props;

            // Get all proposals in the database
            List<PRAXISNETWORK.Models.Proposal> allProposals = null;

            try
            {
                allProposals = proposals.ToList();
            }
            catch (Exception ex)
            {

                Console.WriteLine("Class: ProposalInfoTest\nMethod: AllProposalsTest\nErrorMessage: \n" + ex.InnerException.Message);
                return;
            }

            PRAXISNETWORK.Models.ProposalInfo proposalInfo = new PRAXISNETWORK.Models.ProposalInfo();

            // Same type return test
            Assert.IsInstanceOfType(proposalInfo.AllProposals(), typeof(List<PRAXISNETWORK.Models.ProposalInfo>), "[ERROR MESSAGE => TYPE OF OBJECT INVALID]");

            // The same number of records test
            Assert.AreEqual(Convert.ToString(allProposals.Count()), Convert.ToString(proposalInfo.AllProposals().Count()), "[ERROR MESSAGE => NUMBER OF RECORDS INCORRECT]");

            // The same proposals´ records identification
            if (allProposals.Count() > 0)
                Assert.IsTrue(CustomTests.CustomAssert.verifyPropoposalInfoRecordsIds(proposalInfo.AllProposals(), allProposals), "[ERROR MESSAGE => PROPOSALS´ RECORDS INFORMATION MISMATCH]");
        }
    }
}
