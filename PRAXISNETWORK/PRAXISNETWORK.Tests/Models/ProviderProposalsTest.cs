﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PRAXISNETWORK;
using PRAXISNETWORK.Models;
using PagedList;

namespace PRAXISNETWORK.Tests.Models
{
    [TestClass]
    public class ProviderProposalsTest
    {
        [TestMethod]
        public void allProviderProposals()
        {
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<User> allProviders = from provs in db.Users
                                             where provs.userTypeId == 1
                                             select provs;

            int iteration = 0;
                
            if (allProviders.ToList() != null && allProviders.Count() > 0)
            {
                foreach (User provider in allProviders)
                {
                    IEnumerable<Proposal> allProposals = from props in db.Proposals
                                                         where props.providerId == provider.id
                                                         select props;

                    ProviderProposals actualProviderProposals = new ProviderProposals();
                    actualProviderProposals.page = 1;
                    actualProviderProposals.pageSize = allProposals.Count();
                    actualProviderProposals.userId = provider.id;

                    if (allProposals.Count() > 0)
                    {
                        // Type test
                        Assert.IsInstanceOfType(actualProviderProposals.allProviderProposals(), typeof(PagedList<ProviderProposals>), "[ERROR MESSAGE => TYPE OF OBJECT INVALID; ITERATION = " + iteration + "]");

                        // The same number records test
                        Assert.AreEqual(Convert.ToString(allProposals.ToList().Count()), Convert.ToString(actualProviderProposals.allProviderProposals().Count()), "[ERROR MESSAGE => NUMBER OF RECORDS INCORRECT; ITERATION = " + iteration + "]");

                        // The same provider proposals´ records identification
                        Assert.IsTrue(CustomTests.CustomAssert.verifyAllProviderProposalsRecordsIds(actualProviderProposals.allProviderProposals(), allProposals.ToList()), "[ERROR MESSAGE => PROVIDER PROPOSALS´ RECORDS INFORMATION MISMATCH; ITERATION = " + (iteration++) + "]");
                    }
                }

            }
        }
    }
}
