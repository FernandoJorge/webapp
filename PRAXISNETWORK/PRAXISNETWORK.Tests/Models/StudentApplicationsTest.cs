﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PRAXISNETWORK;
using PRAXISNETWORK.Models;
using PagedList;

namespace PRAXISNETWORK.Tests.Models
{
    [TestClass]
    public class StudentApplicationsTest
    {
        [TestMethod]
        public void allStudentApplicationsTest()
        {
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<User> allStudents = from studs in db.Users
                                            where studs.userTypeId == 2
                                            select studs;

            int iteration = 0;

            if (allStudents.ToList() != null && allStudents.Count() > 0)
                foreach (User student in allStudents)
                {
                    IEnumerable<Application> allApplications = from applications in db.Applications
                                                               where applications.userId == student.id
                                                               select applications;

                    PRAXISNETWORK.Models.StudentApplications actualStudentApplications = new PRAXISNETWORK.Models.StudentApplications();
                    actualStudentApplications.page = 1;
                    actualStudentApplications.pageSize = allApplications.Count();
                    actualStudentApplications.userId = student.id;

                    if (allApplications.Count() > 0)
                    {
                        // Type test
                        Assert.IsInstanceOfType(actualStudentApplications.allStudentApplications(), typeof(PagedList<StudentApplications>), "[ERROR MESSAGE => TYPE OF OBJECT INVALID; ITERATION = " + iteration + "]");

                        // The same number records test
                        Assert.AreEqual(Convert.ToString(allApplications.ToList().Count()), Convert.ToString(actualStudentApplications.allStudentApplications().Count()), "[ERROR MESSAGE => NUMBER OF RECORDS INCORRECT; ITERATION = " + iteration + "]");

                        // The same student applications´ records identification
                        Assert.IsTrue(CustomTests.CustomAssert.verifyAllStudentApplicationsRecordsIds(actualStudentApplications.allStudentApplications(), allApplications.ToList()), "[ERROR MESSAGE => STUDENT APPLICATIONS´ RECORDS INFORMATION MISMATCH; ITERATION = " + (iteration++) + "]");
                    }
                }
        }
    }
}
