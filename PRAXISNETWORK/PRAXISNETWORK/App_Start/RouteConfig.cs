﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PRAXISNETWORK
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "AdminProviderProposals",
                "Administrator/ProviderProposals/{providerId}",
                new { controller = "Administrator", action = "ProviderProposals", providerId = "" }
            );

            routes.MapRoute(
                "AdminStudentProposals",
                "Administrator/StudentProposals/{studentId}",
                new { controller = "Administrator", action = "StudentProposals", studentId = "" }
            );

            routes.MapRoute(
                "ProposalShow",
                "Proposals/ProposalShow/{Id}",
                new { controller = "Proposals", action = "ProposalShow", Id = "" }
            );

            routes.MapRoute(
                "ProposalEdition",
                "Proposals/ProposalEdition/{proposalId}",
                new { controller = "Proposals", action = "ProposalEdition", proposalId = "" }
            );

            routes.MapRoute(
                "DeleteProposal",
                "Proposals/DeleteProposal/{proposalId}",
                new { controller = "Proposals", action = "DeleteProposal", proposalId = "" }
            );

            routes.MapRoute(
                "ShowApplicant",
                "Application/ShowApplicant/{applicantId}",
                new { controller = "Application", action = "ShowApplicant", applicantId = "" }
            );

            routes.MapRoute(
                "ProposalApplications",
                "Application/ProposalApplications/{proposalId}/{page}/{pageSize}",
                new { controller = "Application", action = "ProposalApplications", proposalId = "", page = UrlParameter.Optional, pageSize = UrlParameter.Optional }
            );

            routes.MapRoute(
                "ShowApplication",
                "Application/ShowApplication/{proposalId}/{userId}",
                new { controller = "Application", action = "ShowApplication", proposalId = "", userId = "" }
            );

            routes.MapRoute(
                "EditOfferApplication",
                "Application/EditOfferApplication/{proposalId}",
                new { controller = "Application", action = "EditOfferApplication", proposalId = "" }
            );

            routes.MapRoute(
                "DeleteApplicationDocument",
                "Application/DeleteApplicationDocument/{docId}/{proposalId}",
                new { controller = "Application", action = "DeleteApplicationDocument", docId = "", proposalId = "" }
            );

            routes.MapRoute(
                "DeleteOfferApplication",
                "Application/DeleteOfferApplication/{proposalId}",
                new { controller = "Application", action = "DeleteOfferApplication", proposalId = "" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
            );           
        }
    }
}