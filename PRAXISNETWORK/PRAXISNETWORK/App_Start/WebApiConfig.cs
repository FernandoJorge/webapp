﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PRAXISNETWORK
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                "PraxisOffers",
                "api/PraxisWebService/PraxisOffers/{startDate}/{endDate}",
                new { controller = "PraxisWebService", action = "PraxisOffers", startDate = "", endDate = "" }
            );

            config.Routes.MapHttpRoute(
                "OfferById",
                "api/PraxisWebService/OfferById/{id}",
                new { controller = "PraxisWebService", action = "OfferById", id = "" }
            );

            config.Routes.MapHttpRoute(
                "OfferByWord",
                "api/PraxisWebService/OfferByWord/{word}",
                new { controller = "PraxisWebService", action = "OfferByWord", word = "" }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();
        }
    }
}