﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Controllers
{
    /// <summary>
    /// Handles administrator´s requests
    /// </summary>
    public class AdministratorController : Controller
    {
        /// <summary>
        /// Shows administrator´s management borad
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Session["userId"] != null && (int)Session["userType"] == 3)
            {

                ViewBag.Title = "Administrator board";

                return View();
            }
            else
            {
                return RedirectToAction("AdminAuthentication", "Login");
            }
        }

        public ActionResult ProviderProposals(int providerId, int page = 1, int pageSize = 5)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 3)
            {

                ViewBag.Title = "Provider proposals";
                Models.ProviderProposals providerProps = new Models.ProviderProposals()
                {

                    userId = providerId,
                    page = page,
                    pageSize = pageSize
                };

                return View(providerProps);
            }
            else
            {
                return RedirectToAction("AdminLogin", "Login");
            }
        }

        public ActionResult StudentProposals(int studentId, int page = 1, int pageSize = 5)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 3)
            {

                ViewBag.Title = "Student proposals applications";
                Models.StudentApplications studentApps = new Models.StudentApplications()
                {

                    userId = studentId,
                    page = page,
                    pageSize = pageSize
                };

                return View(studentApps);
            }
            else
            {
                return RedirectToAction("AdminLogin", "Login");
            }
        }
    }
}
