﻿using PagedList;
using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Controllers
{
    /// <summary>
    /// Handles all web requests concerned with the proposal´s offer applications
    /// </summary>
    public class ApplicationController : Controller
    {
        /// <summary>
        /// Shows the a student´s application information for a proposal´s offer
        /// </summary>
        /// <param name="proposalId">Proposal´s identification number</param>
        /// <param name="userId">Student´s identification number</param>
        /// <returns>Html page with the user´s application infromation</returns>
        public ActionResult ShowApplication(int proposalId, int userId)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 1)
            {
                PraxisDBEntities db = new PraxisDBEntities();
                ModelViews.Application.ApplicationShow appShow = new ModelViews.Application.ApplicationShow();

                IEnumerable<Application> applications = from apps in db.Applications
                                                        where apps.proposalId == proposalId
                                                        select apps;

                IEnumerable<ApplicationDocs> applicationDocs = from appDocs in db.ApplicationDocs
                                                               where appDocs.proposalId == proposalId && appDocs.userId == userId
                                                               select appDocs;

                IEnumerable<User> users = from usrs in db.Users
                                          where usrs.id == userId
                                          select usrs;

                Application application = null;
                User user = null;

                try
                {
                    application = applications.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error while acessing proposal´s application...Please try again or contact the administrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }

                try
                {
                    user = users.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error while acessing application user information...Please try again or contact the administrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }

                appShow.username = user.firstName + " " + user.lastName;
                appShow.proposalId = application.proposalId;
                appShow.userId = application.userId;
                appShow.message = application.message;
                appShow.docs = applicationDocs.ToList();

                ViewBag.Title = "Application information";

                return View(appShow);
            }
            else
            {

                return RedirectToAction("Index", "Login");
            }
            return null;
        }

        /// <summary>
        /// Shows a student´s information
        /// </summary>
        /// <param name="applicantId">Student´s identification number</param>
        /// <returns>Html page about student´s information</returns>
        public ActionResult ShowApplicant(int applicantId)
        {

            if (Session["userId"] != null && (int)Session["userType"] == 1)
            {

                PraxisDBEntities db = new PraxisDBEntities();
                ModelViews.Application.ApplicantShow applicant = new ModelViews.Application.ApplicantShow();

                IEnumerable<User> users = from usrs in db.Users
                                          where usrs.id == applicantId
                                          select usrs;

                User user = null;

                try
                {
                    user = users.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error trying to access the applicant information...Please try again or contact the adminsitrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }

                applicant.about = user.about;
                applicant.birthDate = user.birthDate;
                applicant.gender = user.gender;
                applicant.email = user.email;
                applicant.id = user.id;
                applicant.name = user.firstName + " " + user.lastName;
                applicant.picture = user.picture;

                if (user.cityId != null)
                {
                    var citiesAndCountries = from cities in db.Cities
                                             join countries in db.Countries on cities.countryId equals countries.id
                                             where cities.id == user.cityId
                                             select new
                                             {

                                                 countryName = countries.name,
                                                 cityname = cities.name
                                             };

                    try
                    {
                        applicant.city = citiesAndCountries.FirstOrDefault().cityname;
                        applicant.country = citiesAndCountries.FirstOrDefault().countryName;
                    }
                    catch (NullReferenceException ex)
                    {
                        ViewBag.Error = "Occurred an error trying to access the applicant´s city and country information...Please try again or contact the adminsitrator.";
                        Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                        return View("ApplicationResponse");
                    }
                }

                ViewBag.Title = "Applicant information";

                return View(applicant);

            }
            else
            {

                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Shows all applications made for the a specific proposal´s offer
        /// </summary>
        /// <param name="proposalId">Proposal´s offer identification number</param>
        /// <param name="page">Pagination´s page number</param>
        /// <param name="pageSize">Pagination´s number of applications per page</param>
        /// <returns>Html page with all applications for a specific proposal´s offer</returns>
        public ActionResult ProposalApplications(int proposalId, int page = 1, int pageSize = 5)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 1)
            {
                Models.PraxisDBEntities db = new Models.PraxisDBEntities();

                IEnumerable<Models.Proposal> proposals = from props in db.Proposals
                                                         where props.id == proposalId
                                                         select props;

                Models.ProposalApplications applications = new Models.ProposalApplications();
                applications.page = page;
                applications.pageSize = pageSize;
                applications.proposalId = proposalId;

                ViewBag.Title = "\"" + proposals.FirstOrDefault().title + "\" - Proposal´s applications";

                return View(applications);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Shows form fields to apply to a specific proposal´s offer
        /// </summary>
        /// <param name="id">Proposal´s offer identification number</param>
        /// <returns>Html page to fill the information required for the proposal´s offer</returns>
        public ActionResult ApplyOffer(int id = 0)
        {
            // Tests if the user is authenticated
            if (Session["userId"] != null)
            {

                // Tests if user is a student
                if ((int)Session["userType"] == 2)
                {
                    ModelViews.Application.Application application = new ModelViews.Application.Application();
                    application.proposalId = id;
                    application.userId = (int)Session["userId"];

                    ViewBag.Title = "Offer application";

                    return View(application);
                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    return RedirectToAction("Index", "Login");
                }


            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Saves an proposal´s offer application made by a student
        /// </summary>
        /// <param name="application">Model object with the application´s data</param>
        /// <returns>Html page with the process´ result</returns>
        public ActionResult SaveApplication(ModelViews.Application.Application application)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 2)
            {
                if (ModelState.IsValidField("docsUploaded"))
                {
                    Models.PraxisDBEntities db = new Models.PraxisDBEntities();
                    Models.Application applicationRecord = new Models.Application();

                    ViewBag.Title = "Application result";

                    // Application
                    applicationRecord.proposalId = application.proposalId;
                    applicationRecord.userId = application.userId;

                    if (application.message != null)
                        applicationRecord.message = application.message;

                    applicationRecord.status = Models.Utils.ApplicationStatus.PENDING.ToString();
                    applicationRecord.applicationDate = DateTime.Now;

                    db.Applications.Add(applicationRecord);

                    // Application documents
                    if (application.docsUploaded != null && application.docsUploaded.Count() > 0)
                    {
                        String relativePath = "~/Files/ApplicantDocs/Applicant_user_" + application.userId + "_prop_" + application.proposalId;
                        String dir = Server.MapPath(relativePath);

                        try
                        {

                            if (!Directory.Exists(dir))
                                Directory.CreateDirectory(dir);
                        }
                        catch (Exception ex)
                        {
                            ViewBag.Error = "Occurred an error trying to save your application documents! Please try again or contact the adminsitrator.";
                            Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                            return View("ApplicationResponse");
                        }

                        foreach (var file in application.docsUploaded)
                        {

                            if (file != null && file.ContentLength > 0)
                            {
                                var fileName = Path.GetFileName(file.FileName);
                                var path = Path.Combine(dir, fileName);

                                if (!System.IO.File.Exists(path))
                                {
                                    try
                                    {

                                        file.SaveAs(path);
                                    }
                                    catch (Exception ex)
                                    {
                                        ViewBag.Error = "Occurred an error while saving one of your apllication documents! Please try again or contact the adminsitrator.";
                                        Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                                        return View("ApplicationResponse");
                                    }

                                    db.ApplicationDocs.Add(new Models.ApplicationDocs()
                                    {

                                        proposalId = application.proposalId,
                                        userId = application.userId,
                                        description = "Application document",
                                        path = relativePath + System.IO.Path.DirectorySeparatorChar + fileName,
                                        submitDate = DateTime.Now
                                    });
                                }
                            }
                        }

                    }

                    try
                    {

                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = "Occurred an error while saving your application! Please try again or contact the adminsitrator.";
                        Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                        return View("ApplicationResponse");
                    }

                    ViewBag.Success = "YOUR APPLICATION WAS SAVED!!!";

                    return View("ApplicationResponse");
                }
                else
                {
                    return View("ApplyOffer", application);
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Updates an proposal´s application of a student
        /// </summary>
        /// <param name="application">Model obeject with all updates application´s data</param>
        /// <returns>Html page with the process´ result</returns>
        public ActionResult UpdateApplication(ModelViews.Application.Application application)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 2)
            {

                Models.PraxisDBEntities db = new Models.PraxisDBEntities();

                IEnumerable<Models.Application> applications = from apps in db.Applications
                                                               where apps.proposalId == application.proposalId && apps.userId == application.userId
                                                               select apps;

                Models.Application app;

                try
                {
                    app = applications.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error trying to access the application to perform application changes! Please try again or contact the adminsitrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }

                app.message = application.message;

                // Specify as an update
                db.Entry(app).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error trying to save your application changes! Please try again or contact the adminsitrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }

                ViewBag.Success = "YOUR APPLICATION CHANGES WERE SAVED!!!";

                return View("ApplicationResponse");
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

        }

        /// <summary>
        /// Shows form fields to edit a current proposal´s offer application of a student
        /// </summary>
        /// <param name="proposalId">Proposal´s offer identitfication number</param>
        /// <returns>Html page with all the appropriate form fields to fill by the student</returns>
        public ActionResult EditOfferApplication(int proposalId)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 2)
            {
                int userId = (int)Session["userId"];
                Models.PraxisDBEntities db = new Models.PraxisDBEntities();
                ModelViews.Application.Application offer = new ModelViews.Application.Application();

                IEnumerable<Models.Application> applications = from apps in db.Applications
                                                               where apps.proposalId == proposalId && apps.userId == userId
                                                               select apps;

                IEnumerable<Models.ApplicationDocs> appDocs = from docs in db.ApplicationDocs
                                                              where docs.proposalId == proposalId && docs.userId == userId
                                                              select docs;

                offer.proposalId = proposalId;
                offer.userId = userId;
                offer.message = applications.FirstOrDefault().message;
                offer.docs = appDocs.ToList();

                ViewBag.Title = "Application edition";

                return View(offer);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Deletes a proposal´s offer application of a student
        /// </summary>
        /// <param name="proposalId">Proposal´s offer identification number</param>
        /// <returns>Html page with process´ result</returns>
        public ActionResult DeleteOfferApplication(int proposalId)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 2)
            {
                int userId = (int)Session["userId"];
                Models.PraxisDBEntities db = new Models.PraxisDBEntities();

                ViewBag.Title = "Application deletion result";

                IEnumerable<Models.ApplicationDocs> applicationDocs = from appDocs in db.ApplicationDocs
                                                                      where appDocs.proposalId == proposalId && appDocs.userId == userId
                                                                      select appDocs;

                // Remove all physical documents of the application from server
                if (applicationDocs.ToList() != null && applicationDocs.ToList().Count() > 0)
                {
                    foreach (Models.ApplicationDocs appDoc in applicationDocs)
                    {
                        try
                        {
                            String fullPath = Server.MapPath(appDoc.path);

                            if (System.IO.File.Exists(fullPath))
                                System.IO.File.Delete(fullPath);
                        }
                        catch (Exception ex)
                        {
                            ViewBag.Error = "Occurred an error while deleting your application documents files...";
                            Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                            return View("ApplicationResponse");
                        }
                    }

                    // Remove all documents of the application from database
                    foreach (Models.ApplicationDocs appDoc in applicationDocs)
                        db.ApplicationDocs.Remove(appDoc);
                }

                // Delete application
                IEnumerable<Models.Application> applications = from apps in db.Applications
                                                               where apps.proposalId == proposalId && apps.userId == userId
                                                               select apps;

                db.Applications.Remove(applications.FirstOrDefault());

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error while deleting your application..."; ;
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }

                ViewBag.Success = "YOUR APPLICATION WAS DELETED!!!";
                return View("ApplicationResponse");
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Deletes a proposal´s offer document application document, image, etc
        /// </summary>
        /// <param name="docId">Application´s document identification number</param>
        /// <param name="proposalId">Proposal´s offer identification number</param>
        /// <returns>Redirects to the 'EditOfferApplication' action method</returns>
        public ActionResult DeleteApplicationDocument(int docId, int proposalId)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 2)
            {
                int userId = (int)Session["userId"];
                Models.PraxisDBEntities db = new Models.PraxisDBEntities();

                IEnumerable<Models.ApplicationDocs> docs = from appDocs in db.ApplicationDocs
                                                           where appDocs.id == docId
                                                           select appDocs;

                Models.ApplicationDocs appDoc;

                try
                {
                    appDoc = docs.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error while accessing database for the selected application document file...";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }

                String path = appDoc.path;
                db.ApplicationDocs.Remove(appDoc);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error while deleting the selected application document file...";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }

                try
                {
                    String fullPath = Server.MapPath(path);

                    if (System.IO.File.Exists(fullPath))
                        System.IO.File.Delete(fullPath);
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error while deleting the selected application document file from system...";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }

                return RedirectToAction("EditOfferApplication/" + proposalId);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        // JQUERY REQUESTS

        /// <summary>
        /// Adds a new application´s document to the current proposal´s application displayed
        /// </summary>
        /// <param name="docsUploaded">New proposal´s application document uploaded</param>
        /// <param name="proposalId">Proposal´s offer identification number</param>
        /// <returns>Redirects to the 'EditOfferApplication' action method</returns>
        public ActionResult AddApplicationDocument(HttpPostedFileBase docsUploaded, int proposalId = -1)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 2)
            {
                List<String> fileFormats = Models.Utils.uploadFileFormats(Models.Utils.FileFormats.ALL);

                int userId = (int)Session["userId"];
                Models.PraxisDBEntities db = new Models.PraxisDBEntities();

                if (docsUploaded != null && docsUploaded.ContentLength > 0 && fileFormats.Contains(Path.GetFileName(docsUploaded.FileName).Split('.').Last().ToLower()))
                {
                    String relativePath = "~/Files/ApplicantDocs/Applicant_user_" + userId + "_prop_" + proposalId;
                    String dir = Server.MapPath("~/Files/ApplicantDocs/Applicant_user_" + userId + "_prop_" + proposalId);
                    var fileName = Path.GetFileName(docsUploaded.FileName);
                    var path = Path.Combine(dir, fileName);

                    if (!System.IO.File.Exists(path))
                    {
                        try
                        {

                            if (!Directory.Exists(dir))
                                Directory.CreateDirectory(dir);


                            docsUploaded.SaveAs(path);
                        }
                        catch (Exception ex)
                        {
                            ViewBag.Error = "Occurred an error while adding new document...Please reurn to the application edition.";
                            Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                            return View("ApplicationResponse");
                        }

                        db.ApplicationDocs.Add(new Models.ApplicationDocs()
                        {

                            proposalId = proposalId,
                            userId = userId,
                            description = "Application document",
                            path = relativePath + System.IO.Path.DirectorySeparatorChar + fileName,
                            submitDate = DateTime.Now
                        });
                    }
                }
                else
                {
                    return RedirectToAction("EditOfferApplication", "Application", new { @proposalId = proposalId });
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error while adding new document...Please reurn to the application edition.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }

                //IEnumerable<Models.ApplicationDocs> appDocs = from docs in db.ApplicationDocs
                //                                              where docs.proposalId == proposalId && docs.userId == userId
                //                                              select docs;

                return RedirectToAction("EditOfferApplication", "Application", new { @proposalId = proposalId });

            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Changes the proposal´s offer application status {PENDING, REJECTED, ASSIGNED}
        /// </summary>
        /// <param name="info">Information about the new status - String format: "STATUS,PROPOSAL_ID,USER_ID"</param>
        /// <returns>Partial View with the updated application´s status</returns>
        public ActionResult ChangeApplicationStatus(String info)
        {
            int proposalId = Convert.ToInt32(info.Split(',')[1]);

            if (Session["userId"] != null && (int)Session["userType"] == 1)
            {
                PraxisDBEntities db = new PraxisDBEntities();
                IEnumerable<Application> allApps = from apps in db.Applications
                                                   select apps;

                String newStatus = info.Split(',')[0];
                int userId = Convert.ToInt32(info.Split(',')[2]);

                Application application = null;
                List<Application> othersApplications = null;

                switch (newStatus)
                {
                    case "PENDING":

                        List<Application> appsWithAssignedStatus = allApps.Where(app => app.proposalId == proposalId && app.userId != userId && app.status.Equals("ASSIGNED")).ToList();

                        if (appsWithAssignedStatus == null || appsWithAssignedStatus.Count() == 0)
                        {
                            application = allApps.Where(app => app.proposalId == proposalId && app.userId == userId).FirstOrDefault();

                            if (application.status.Equals("ASSIGNED"))
                            {

                                othersApplications = allApps.Where(app => app.proposalId == proposalId && app.userId != userId).ToList();

                                if (othersApplications != null && othersApplications.Count() > 0)
                                    foreach (Application app in othersApplications)
                                    {
                                        app.status = Utils.ApplicationStatus.PENDING.ToString();
                                        db.Entry(app).State = System.Data.Entity.EntityState.Modified;
                                    }
                            }

                            application.status = Utils.ApplicationStatus.PENDING.ToString();
                        }
                        break;

                    case "REJECTED":

                        application = allApps.Where(app => app.proposalId == proposalId && app.userId == userId).FirstOrDefault();

                        if (application.status.Equals("ASSIGNED"))
                        {

                            othersApplications = allApps.Where(app => app.proposalId == proposalId && app.userId != userId).ToList();

                            if (othersApplications != null && othersApplications.Count() > 0)
                                foreach (Application app in othersApplications)
                                {
                                    app.status = Utils.ApplicationStatus.PENDING.ToString();
                                    db.Entry(app).State = System.Data.Entity.EntityState.Modified;
                                }
                        }

                        application.status = Utils.ApplicationStatus.REJECTED.ToString();
                        db.Entry(application).State = System.Data.Entity.EntityState.Modified;
                        break;

                    case "ASSIGNED":

                        othersApplications = allApps.Where(app => app.proposalId == proposalId && app.userId != userId).ToList();
                        application = allApps.Where(app => app.proposalId == proposalId && app.userId == userId).FirstOrDefault();

                        if (othersApplications != null && othersApplications.Count() > 0)
                            foreach (Application app in othersApplications)
                            {
                                app.status = Utils.ApplicationStatus.REJECTED.ToString();
                                db.Entry(app).State = System.Data.Entity.EntityState.Modified;
                            }

                        application.status = Utils.ApplicationStatus.ASSIGNED.ToString();
                        db.Entry(application).State = System.Data.Entity.EntityState.Modified;
                        break;
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error while updating the application status...Please try again or contact the administrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("ApplicationResponse");
                }


                Models.ProposalApplications applications = new Models.ProposalApplications();
                applications.page = 1;
                applications.pageSize = 5;
                applications.proposalId = proposalId;

                return PartialView("ProposalApplicationsPartialView", applications);

            }
            else
            {
                Models.ProposalApplications applications = new Models.ProposalApplications();
                applications.page = 1;
                applications.pageSize = 5;
                applications.proposalId = proposalId;

                return PartialView("ProposalApplicationsPartialView", applications);
            }
        }

    }
}
