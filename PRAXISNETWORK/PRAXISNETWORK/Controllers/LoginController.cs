﻿using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Controllers
{
    /// <summary>
    /// Handles all requests about user´s autenthication´s
    /// </summary>
    public class LoginController : Controller
    {
        /// <summary>
        /// Shows the front end web application user´s login page
        /// </summary>
        /// <returns>Html page user´s login</returns>
        public ActionResult Index()
        {
            // If there's a session active it will deactivate it
            if (Session["userId"] != null)
            {
                Session.Clear();
                Session.Abandon();
            }

            ModelViews.Login.Login login = new ModelViews.Login.Login();

            ViewBag.Title = "Login";

            return View(login);

        }

        /// <summary>
        /// Handles the process of the user´s authentication - Student or Provider
        /// </summary>
        /// <param name="login">Model object with authentication´s credentials data</param>
        /// <returns>Redirects to a specific html page depending the user type</returns>
        public ActionResult Authentication(ModelViews.Login.Login login)
        {
            if (ModelState.IsValid)
            {

                PraxisDBEntities db = new PraxisDBEntities();
                int userType;

                // Applicant
                if (login.isApplicant)
                    userType = 2;
                else userType = 1; // Provider

                IEnumerable<User> users = from usr in db.Users
                                          where (login.usernameEmail == usr.username || login.usernameEmail == usr.email) && usr.userTypeId == userType
                                          select usr;

                User user;

                // Tests if exists a record
                try
                {
                    user = users.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Title = "Authentication result";
                    ViewBag.Error = "Occurred and error while authenticating. Try again please.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("AuthenticationFailed");
                }

                if (user == null)
                {
                    ViewBag.Title = "Authentication result";

                    if (userType == 1)
                    {
                        ViewBag.Error = "You´re not registered in our system... Please register as Provider to access our services.";
                    }
                    else if (userType == 2)
                    {
                        ViewBag.Error = "You´re not registered in our system... Please register as Student to access our services.";
                    }

                    return View("AuthenticationFailed");
                }

                // Test if password matches
                if (!login.password.Equals(Utils.decrypt(user.password)))
                {
                    ViewBag.Title = "Authentication result";

                    if (userType == 1)
                    {
                        ViewBag.Error = "Your authentication as Provider failed...";
                    }
                    else if (userType == 2)
                    {
                        ViewBag.Error = "Your authentication as Student failed...";
                    }

                    return View("AuthenticationFailed");
                }

                // Tests if user is enabled
                if (!(Boolean)user.enabled)
                {
                    ViewBag.Error = "You´re currently disabled... Contact the administrator.";
                    return View("AuthenticationFailed");
                }

                // Tests if user is locked
                if ((Boolean)user.locked)
                {

                    ViewBag.Error = "You´re currently locked... Contact the administrator.";
                    return View("AuthenticationFailed");
                }

                // Update last login access
                user.lastLogin = DateTime.Now;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Title = "Authentication result";
                    ViewBag.Error = "Occurred an error while authenticating... Please try again...";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("AuthenticationFailed");
                }

                Session["userName"] = user.firstName + " " + user.lastName;
                Session["userId"] = user.id;
                Session["userType"] = user.userTypeId;

                // Provider
                if (userType == 1)
                {
                    return RedirectToAction("Index", "Provider");
                }
                else if (userType == 2)
                { // Student

                    return RedirectToAction("Index", "Student");
                }

            }
            else
            {
                ViewBag.Title = "Login";

                return View("Index", login);
            }

            return null;
        }

        public ActionResult AdminLogin()
        {
            // If there's a session active it will deactivate it
            if (Session["userId"] != null)
            {
                Session.Clear();
                Session.Abandon();
            }

            ModelViews.Login.Login login = new ModelViews.Login.Login();

            ViewBag.Title = "Administrator login";

            return View(login);

        }

        /// <summary>
        /// Handles the process authentication of the Administrator
        /// </summary>
        /// <param name="login">Model object with authentication´s credentials data</param>
        /// <returns>Redirects to the administrator´s html page management board</returns>
        public ActionResult AdminAuthentication(ModelViews.Login.Login login)
        {
            if (ModelState.IsValid)
            {

                PraxisDBEntities db = new PraxisDBEntities();

                IEnumerable<User> users = from usr in db.Users
                                          where (login.usernameEmail == usr.username || login.usernameEmail == usr.email) && usr.userTypeId == 3
                                          select usr;

                User user;

                // Tests if exists a record
                try
                {
                    user = users.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Title = "Authentication result";
                    ViewBag.Error = "Occurred and error while authenticating. Try again please.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("AuthenticationFailed");
                }

                if (user == null)
                {
                    ViewBag.Title = "Authentication result";
                    ViewBag.Error = "You don´t have permition to administer the system.";

                    return View("AuthenticationFailed");
                }

                // Test if password matches
                if (!login.password.Equals(Utils.decrypt(user.password)))
                {
                    ViewBag.Title = "Authentication result";
                    ViewBag.Error = "Your authentication as Administrator failed...";

                    return View("AuthenticationFailed");
                }

                // Tests if user is enabled
                if (!(Boolean)user.enabled)
                {
                    ViewBag.Error = "You´re currently disabled.";
                    return View("AuthenticationFailed");
                }

                // Tests if user is locked
                if ((Boolean)user.locked)
                {

                    ViewBag.Error = "You´re currently locked.";
                    return View("AuthenticationFailed");
                }

                // Update last login access
                user.lastLogin = DateTime.Now;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Title = "Authentication result";
                    ViewBag.Error = "Occurred an error while authenticating... Please try again...";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("AuthenticationFailed");
                }

                Session["userName"] = user.firstName + " " + user.lastName;
                Session["userId"] = user.id;
                Session["userType"] = 3;

                return RedirectToAction("Index", "Administrator");
            }
            else
            {
                ViewBag.Title = "Login";

                return View("AdminLogin");
            }
        }

        /// <summary>
        /// Logs off the current user and ends his session
        /// </summary>
        /// <returns>Redirects to the user´s authentication html page</returns>
        public ActionResult Logout()
        {
            if ((int)Session["userType"] == 1 || (int)Session["userType"] == 2)
            {
                Session.Clear();
                Session.Abandon();
                return RedirectToAction("Index");
            }
            else if ((int)Session["userType"] == 3)
            {
                Session.Clear();
                Session.Abandon();
                return RedirectToAction("AdminLogin");
            }

            return RedirectToAction("Index");
        }

    }
}
