﻿using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace PRAXISNETWORK.Controllers
{
    /// <summary>
    /// Handles all information about news feeds and newsletters requests
    /// </summary>
    public class NewsController : Controller
    {
        /// <summary>
        /// Shows a html page to subscribe the newsletter mechanism
        /// </summary>
        /// <returns>Html page fill with the subscriber´s email address</returns>
        public ActionResult NewSubscriber()
        {
            ViewBag.Title = "New subscriber";
            ModelViews.News.NewSubscriber subscriber = new ModelViews.News.NewSubscriber();
            return View(subscriber);
        }

        /// <summary>
        /// Saves new newsletter´s subscriber
        /// </summary>
        /// <param name="newSubscriber">Model object with the current ´subscriber´s data</param>
        /// <returns>Html page process' result</returns>
        public ActionResult SaveSubscriber(ModelViews.News.NewSubscriber newSubscriber)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Title = "Subscription result";
                Models.PraxisDBEntities db = new Models.PraxisDBEntities();

                Models.Subscriber subscriberRecord = new Models.Subscriber()
                {
                    name = (newSubscriber.name != null ? newSubscriber.name : null),
                    email = newSubscriber.email,
                    subscriptionDate = DateTime.Now
                };

                db.Subscribers.Add(subscriberRecord);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error while saving your subscription...Please try again or contact the administrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("NewsResponse");
                }

                ViewBag.Success = "YOUR SUBSCRIPTION WAS SUCCESSFUL!!!";
                return View("NewsResponse");
            }
            else
            {
                ViewBag.Title = "New subscriber";
                return View("NewSubscriber", newSubscriber);
            }
        }

        /// <summary>
        /// Handles requet to send Rss Feed to Rss Feed´s clients with current proposal´s offers
        /// </summary>
        /// <returns>Standard Rss Feed´s XML Document</returns>
        public XDocument RssFeed()
        {
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<Proposal> allProposals = from props in db.Proposals
                                                 where props.validTo.CompareTo(DateTime.Now) >= 0
                                                 select props;

            XElement channel = new XElement("channel",
                                    new XElement("title", "Proposals´s Rss Feeds"),
                                    new XElement("link", "http://localhost:63178/Proposals"),
                                    new XElement("Description", "This Rss Feed has information about proposals that are up to the date valid for application"));

            if (allProposals.ToList() != null && allProposals.ToList().Count() > 0)
            {
                foreach (Proposal proposal in db.Proposals)
                {
                    int end = (int)(proposal.description.Length * 0.3);
                    String title = proposal.title;
                    String description = proposal.description.Substring(0, end) + "\n(Click the link below to see offer´s information...)";
                    String OfferLink = "http://localhost:63178/Proposals/ProposalShow/" + proposal.id;

                    XElement item = new XElement("item",
                                        new XElement("title", title),
                                        new XElement("description", description),
                                        new XElement("link", OfferLink));

                    channel.Add(item);
                }

                XElement rssFeed = new XElement("rss",
                                    new XAttribute("version", "2.0"),
                                    channel);

                XDocument doc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"), rssFeed);



                return doc;
            }
            else return null;
        }
    }
}
