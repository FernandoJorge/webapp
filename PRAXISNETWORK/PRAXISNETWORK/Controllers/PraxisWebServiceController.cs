﻿using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Xml.Linq;

namespace PRAXISNETWORK.Controllers
{
    public class PraxisWebServiceController : ApiController
    {
        /// <summary>
        /// Retrieves all registered offers´ information
        /// </summary>
        /// <param name="startDate">Start date for the search(optional)</param>
        /// <param name="endDate">end date for the search(optional)</param>
        /// <returns>Xml document with all offers´ information</returns>
        [HttpGet]
        public XDocument PraxisOffers(String startDate, String endDate)
        {
            XDocument xmlAllProposals = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"));
            String dateRegex = @"\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])";

            if ((!String.IsNullOrEmpty(startDate) && !Regex.IsMatch(startDate, dateRegex, RegexOptions.IgnoreCase) || (!String.IsNullOrEmpty(endDate) && !Regex.IsMatch(endDate, dateRegex, RegexOptions.IgnoreCase))))
            {

                xmlAllProposals.Add(new XElement("error", "dates must follow the correct format: Ex: 2012-03-17"));

                return xmlAllProposals;
            }

            DateTime stDate = Convert.ToDateTime(startDate);
            DateTime edDate = Convert.ToDateTime(endDate);

            if (!String.IsNullOrEmpty(endDate) && stDate.CompareTo(edDate) > 0)
            {
                xmlAllProposals.Add(new XElement("error", "End date must be earlier than start date..."));

                return xmlAllProposals;
            }

            Models.PraxisDBEntities db = new Models.PraxisDBEntities();

            List<ProposalInfo> proposals = null;

            if (String.IsNullOrEmpty(startDate) && String.IsNullOrEmpty(endDate))
            {
                try
                {
                    proposals = (from props in db.Proposals
                                 join cities in db.Cities on props.cityId equals cities.id
                                 join countries in db.Countries on cities.countryId equals countries.id
                                 join institituions in db.Institutions on props.institutionId equals institituions.id
                                 join hosts in db.Institutions on props.hostId equals hosts.id
                                 join providers in db.Users on props.providerId equals providers.id
                                 into info
                                 from p in info.DefaultIfEmpty()
                                 select new ProposalInfo()
                                 {
                                     id = props.id,
                                     title = props.title,
                                     description = props.description,
                                     city = cities.name,
                                     country = countries.name,
                                     institution = institituions.name,
                                     host = hosts.name,
                                     startDate = props.startDate,
                                     endDate = props.endDate,
                                     validTo = props.validTo,
                                     supervisorFirstName = props.supervisorFirstName,
                                     superVisorLastName = props.supervisorLastName,
                                     superVisorEmail = props.supervisorEmail,
                                     payment = props.payment,
                                     vacancies = props.vacancies,
                                     globalSkills = props.globalSkills,
                                     jobRelatedSkills = props.jobRelatedSkills,
                                     benefits = props.benefits,
                                     contact = props.contact
                                 }).ToList();

                }
                catch (Exception ex)
                {

                    Utils.writeLog("[Controller: PraxisWebservice]\n[Action method: PraxisOffers]\n[Error conditions: Start date and end date are null]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    xmlAllProposals.Add(new XElement("error", "It was not possible to retrieve information..."));
                }
            }
            else if (!String.IsNullOrEmpty(startDate) && String.IsNullOrEmpty(endDate))
            {
                try
                {
                    proposals = (from props in db.Proposals
                                 join cities in db.Cities on props.cityId equals cities.id
                                 join countries in db.Countries on cities.countryId equals countries.id
                                 join institituions in db.Institutions on props.institutionId equals institituions.id
                                 join hosts in db.Institutions on props.hostId equals hosts.id
                                 join providers in db.Users on props.providerId equals providers.id
                                 into info
                                 from p in info.DefaultIfEmpty()
                                 select new ProposalInfo()
                                 {
                                     id = props.id,
                                     title = props.title,
                                     description = props.description,
                                     city = cities.name,
                                     country = countries.name,
                                     institution = institituions.name,
                                     host = hosts.name,
                                     startDate = props.startDate,
                                     endDate = props.endDate,
                                     validTo = props.validTo,
                                     supervisorFirstName = props.supervisorFirstName,
                                     superVisorLastName = props.supervisorLastName,
                                     superVisorEmail = props.supervisorEmail,
                                     payment = props.payment,
                                     vacancies = props.vacancies,
                                     globalSkills = props.globalSkills,
                                     jobRelatedSkills = props.jobRelatedSkills,
                                     benefits = props.benefits,
                                     contact = props.contact
                                 }).Where(p => p.startDate.CompareTo(stDate) >= 0).ToList();

                }
                catch (Exception ex)
                {
                    Utils.writeLog("[Controller: PraxisWebservice]\n[Action method: PraxisOffers]\n[Error conditions: Only end date is null]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    xmlAllProposals.Add(new XElement("error", "It was not possible to retrieve information..."));
                }

            }
            else if (!String.IsNullOrEmpty(startDate) && !String.IsNullOrEmpty(endDate))
            {
                try
                {
                    proposals = (from props in db.Proposals
                                 join cities in db.Cities on props.cityId equals cities.id
                                 join countries in db.Countries on cities.countryId equals countries.id
                                 join institituions in db.Institutions on props.institutionId equals institituions.id
                                 join hosts in db.Institutions on props.hostId equals hosts.id
                                 join providers in db.Users on props.providerId equals providers.id
                                 into info
                                 from p in info.DefaultIfEmpty()
                                 select new ProposalInfo()
                                 {
                                     id = props.id,
                                     title = props.title,
                                     description = props.description,
                                     city = cities.name,
                                     country = countries.name,
                                     institution = institituions.name,
                                     host = hosts.name,
                                     startDate = props.startDate,
                                     endDate = props.endDate,
                                     validTo = props.validTo,
                                     supervisorFirstName = props.supervisorFirstName,
                                     superVisorLastName = props.supervisorLastName,
                                     superVisorEmail = props.supervisorEmail,
                                     payment = props.payment,
                                     vacancies = props.vacancies,
                                     globalSkills = props.globalSkills,
                                     jobRelatedSkills = props.jobRelatedSkills,
                                     benefits = props.benefits,
                                     contact = props.contact
                                 }).Where(p => p.startDate.CompareTo(stDate) >= 0 && p.endDate.CompareTo(edDate) <= 0).ToList();
                }
                catch (Exception ex)
                {
                    Utils.writeLog("[Controller: PraxisWebservice]\n[Action method: PraxisOffers]\n[Error conditions: Start date and end date are not null]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    xmlAllProposals.Add(new XElement("error", "It was not possible to retrieve information..."));
                }
            }
            else if (String.IsNullOrEmpty(startDate) && !String.IsNullOrEmpty(endDate))
            {
                try
                {
                    proposals = (from props in db.Proposals
                                 join cities in db.Cities on props.cityId equals cities.id
                                 join countries in db.Countries on cities.countryId equals countries.id
                                 join institituions in db.Institutions on props.institutionId equals institituions.id
                                 join hosts in db.Institutions on props.hostId equals hosts.id
                                 join providers in db.Users on props.providerId equals providers.id
                                 into info
                                 from p in info.DefaultIfEmpty()
                                 select new ProposalInfo()
                                 {
                                     id = props.id,
                                     title = props.title,
                                     description = props.description,
                                     city = cities.name,
                                     country = countries.name,
                                     institution = institituions.name,
                                     host = hosts.name,
                                     startDate = props.startDate,
                                     endDate = props.endDate,
                                     validTo = props.validTo,
                                     supervisorFirstName = props.supervisorFirstName,
                                     superVisorLastName = props.supervisorLastName,
                                     superVisorEmail = props.supervisorEmail,
                                     payment = props.payment,
                                     vacancies = props.vacancies,
                                     globalSkills = props.globalSkills,
                                     jobRelatedSkills = props.jobRelatedSkills,
                                     benefits = props.benefits,
                                     contact = props.contact
                                 }).Where(p => p.endDate.CompareTo(edDate) <= 0).ToList();
                }
                catch (Exception ex)
                {
                    Utils.writeLog("[Controller: PraxisWebservice]\n[Action method: PraxisOffers]\n[Error conditions: Only start date is null]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    xmlAllProposals.Add(new XElement("error", "It was not possible to retrieve information..."));
                }
            }

            if (proposals != null && proposals.Count() > 0)
            {
                XElement xmlProposals = new XElement("proposals");

                foreach (ProposalInfo proposal in proposals)
                {
                    IEnumerable<Models.Target> allTargets = from targets in db.Targets
                                                            join propTargets in db.ProposalsTargets on targets.id equals propTargets.targetId
                                                            where propTargets.proposalId == proposal.id
                                                            select targets;

                    IEnumerable<Models.StudyDegree> allDegrees = from degrees in db.StudyDegrees
                                                                 join propDegrees in db.ProposalStudyDegrees on degrees.id equals propDegrees.studyDegreeId
                                                                 where propDegrees.proposalId == proposal.id
                                                                 select degrees;

                    IEnumerable<Models.StudyArea> allAreas = from areas in db.StudyAreas
                                                             join propAreas in db.ProposalStudyAreas on areas.id equals propAreas.studyAreaId
                                                             where propAreas.proposalId == proposal.id
                                                             select areas;

                    IEnumerable<Models.Language> allLanguages = from languages in db.Languages
                                                                join propLanguages in db.ProposalLanguages on languages.id equals propLanguages.languageId
                                                                where propLanguages.proposalId == proposal.id
                                                                select languages;

                    IEnumerable<Models.Skill> allPromotedSkills = from skills in db.Skills
                                                                  join promotedSkills in db.PromotedSkills on skills.id equals promotedSkills.skillId
                                                                  where promotedSkills.proposalId == proposal.id
                                                                  select skills;

                    IEnumerable<Models.Skill> allRequiredSkills = from skills in db.Skills
                                                                  join requiredSkills in db.RequiredSkills on skills.id equals requiredSkills.skillId
                                                                  where requiredSkills.proposalId == proposal.id
                                                                  select skills;

                    XElement xmlTargets = new XElement("targets");
                    XElement xmlDegrees = new XElement("study_degrees");
                    XElement xmlAreas = new XElement("study_areas");
                    XElement xmlLanguages = new XElement("languages");
                    XElement xmlPromotedSkills = new XElement("skills_to_promote");
                    XElement xmlRequiredSkills = new XElement("required_skills");


                    if (allTargets != null && allTargets.Count() > 0)
                        foreach (Models.Target target in allTargets)
                            xmlTargets.Add(new XElement("target", target.name));

                    if (allDegrees != null && allDegrees.Count() > 0)
                        foreach (Models.StudyDegree degree in allDegrees)
                            xmlDegrees.Add(new XElement("study_degree", degree.name));

                    if (allAreas != null && allAreas.Count() > 0)
                        foreach (Models.StudyArea area in allAreas)
                            xmlAreas.Add(new XElement("study_area", area.name));

                    if (allLanguages != null && allLanguages.Count() > 0)
                        foreach (Models.Language language in allLanguages)
                            xmlLanguages.Add(new XElement("language", language.name));

                    if (allPromotedSkills != null && allPromotedSkills.Count() > 0)
                        foreach (Models.Skill skill in allPromotedSkills)
                            xmlPromotedSkills.Add(new XElement("skill_to_promote", skill.name));

                    if (allRequiredSkills != null && allRequiredSkills.Count() > 0)
                        foreach (Models.Skill skill in allRequiredSkills)
                            xmlRequiredSkills.Add(new XElement("required_skill", skill.name));

                    XElement xmlProposal = new XElement("proposal",
                                            new XElement("proposal_id", Convert.ToString(proposal.id)),
                                            new XElement("title", proposal.title),
                                            new XElement("description", proposal.description),
                                            new XElement("city", proposal.city),
                                            new XElement("country", proposal.country),
                                            new XElement("institution", proposal.institution),
                                            new XElement("host", proposal.host),
                                            new XElement("start_date", proposal.startDate.ToString("yyyy-MM-dd")),
                                            new XElement("end_date", proposal.endDate.ToString("yyyy-MM-dd")),
                                            new XElement("validTo", proposal.validTo.ToString("yyyy-MM-dd")),
                                            new XElement("payment", proposal.payment),
                                            new XElement("vacancies", proposal.vacancies),
                                            new XElement("global_skills", proposal.globalSkills),
                                            new XElement("job_related_skills", proposal.jobRelatedSkills),
                                            new XElement("benefits", proposal.benefits),
                                            new XElement("contact", proposal.contact),
                                            new XElement("supervisor_firstname", proposal.supervisorFirstName),
                                            new XElement("supervisor_lastname", proposal.superVisorLastName),
                                            new XElement("supervisor_email", proposal.superVisorEmail),
                                            xmlTargets,
                                            xmlDegrees,
                                            xmlAreas,
                                            xmlLanguages,
                                            xmlPromotedSkills,
                                            xmlRequiredSkills);

                    xmlProposals.Add(xmlProposal);
                }

                xmlAllProposals.Add(xmlProposals);
            }
            else
            {
                xmlAllProposals.Add(new XElement("proposals", "There is no proposals"));

                return xmlAllProposals;
            }

            return xmlAllProposals;
        }

        /// <summary>
        /// Retrieves a offer´s information by identification number
        /// </summary>
        /// <param name="id">Offer´s identification number</param>
        /// <returns>Xml document with offer´s information</returns>
        [HttpGet]
        public XDocument OfferById(int id)
        {
            XDocument xmlDoc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"));

            if (id <= 0)
            {
                xmlDoc.Add(new XElement("error", "Incorrect offer´s number identification..."));

                return xmlDoc;
            }

            PraxisDBEntities db = new PraxisDBEntities();

            ProposalInfo proposal = null;

            try
            {
                proposal = (from props in db.Proposals
                            join cities in db.Cities on props.cityId equals cities.id
                            join countries in db.Countries on cities.countryId equals countries.id
                            join institituions in db.Institutions on props.institutionId equals institituions.id
                            join hosts in db.Institutions on props.hostId equals hosts.id
                            join providers in db.Users on props.providerId equals providers.id
                            into info
                            from p in info.DefaultIfEmpty()
                            select new ProposalInfo()
                            {
                                id = props.id,
                                title = props.title,
                                description = props.description,
                                city = cities.name,
                                country = countries.name,
                                institution = institituions.name,
                                host = hosts.name,
                                startDate = props.startDate,
                                endDate = props.endDate,
                                validTo = props.validTo,
                                supervisorFirstName = props.supervisorFirstName,
                                superVisorLastName = props.supervisorLastName,
                                superVisorEmail = props.supervisorEmail,
                                payment = props.payment,
                                vacancies = props.vacancies,
                                globalSkills = props.globalSkills,
                                jobRelatedSkills = props.jobRelatedSkills,
                                benefits = props.benefits,
                                contact = props.contact
                            }).Where(p => p.id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Utils.writeLog("[Controller: PraxisWebservice]\n[Action method: OfferById]\n[Error conditions: Start date and end date are null]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                xmlDoc.Add(new XElement("error", "It was not possible to retrieve information..."));
            }

            if (proposal != null)
            {
                IEnumerable<Models.Target> allTargets = from targets in db.Targets
                                                        join propTargets in db.ProposalsTargets on targets.id equals propTargets.targetId
                                                        where propTargets.proposalId == proposal.id
                                                        select targets;

                IEnumerable<Models.StudyDegree> allDegrees = from degrees in db.StudyDegrees
                                                             join propDegrees in db.ProposalStudyDegrees on degrees.id equals propDegrees.studyDegreeId
                                                             where propDegrees.proposalId == proposal.id
                                                             select degrees;

                IEnumerable<Models.StudyArea> allAreas = from areas in db.StudyAreas
                                                         join propAreas in db.ProposalStudyAreas on areas.id equals propAreas.studyAreaId
                                                         where propAreas.proposalId == proposal.id
                                                         select areas;

                IEnumerable<Models.Language> allLanguages = from languages in db.Languages
                                                            join propLanguages in db.ProposalLanguages on languages.id equals propLanguages.languageId
                                                            where propLanguages.proposalId == proposal.id
                                                            select languages;

                IEnumerable<Models.Skill> allPromotedSkills = from skills in db.Skills
                                                              join promotedSkills in db.PromotedSkills on skills.id equals promotedSkills.skillId
                                                              where promotedSkills.proposalId == proposal.id
                                                              select skills;

                IEnumerable<Models.Skill> allRequiredSkills = from skills in db.Skills
                                                              join requiredSkills in db.RequiredSkills on skills.id equals requiredSkills.skillId
                                                              where requiredSkills.proposalId == proposal.id
                                                              select skills;

                XElement xmlTargets = new XElement("targets");
                XElement xmlDegrees = new XElement("study_degrees");
                XElement xmlAreas = new XElement("study_areas");
                XElement xmlLanguages = new XElement("languages");
                XElement xmlPromotedSkills = new XElement("skills_to_promote");
                XElement xmlRequiredSkills = new XElement("required_skills");


                if (allTargets != null && allTargets.Count() > 0)
                    foreach (Models.Target target in allTargets)
                        xmlTargets.Add(new XElement("target", target.name));

                if (allDegrees != null && allDegrees.Count() > 0)
                    foreach (Models.StudyDegree degree in allDegrees)
                        xmlDegrees.Add(new XElement("study_degree", degree.name));

                if (allAreas != null && allAreas.Count() > 0)
                    foreach (Models.StudyArea area in allAreas)
                        xmlAreas.Add(new XElement("study_area", area.name));

                if (allLanguages != null && allLanguages.Count() > 0)
                    foreach (Models.Language language in allLanguages)
                        xmlLanguages.Add(new XElement("language", language.name));

                if (allPromotedSkills != null && allPromotedSkills.Count() > 0)
                    foreach (Models.Skill skill in allPromotedSkills)
                        xmlPromotedSkills.Add(new XElement("skill_to_promote", skill.name));

                if (allRequiredSkills != null && allRequiredSkills.Count() > 0)
                    foreach (Models.Skill skill in allRequiredSkills)
                        xmlRequiredSkills.Add(new XElement("required_skill", skill.name));

                XElement xmlProposal = new XElement("proposal",
                                        new XElement("proposal_id", Convert.ToString(proposal.id)),
                                        new XElement("title", proposal.title),
                                        new XElement("description", proposal.description),
                                        new XElement("city", proposal.city),
                                        new XElement("country", proposal.country),
                                        new XElement("institution", proposal.institution),
                                        new XElement("host", proposal.host),
                                        new XElement("start_date", proposal.startDate.ToString("yyyy-MM-dd")),
                                        new XElement("end_date", proposal.endDate.ToString("yyyy-MM-dd")),
                                        new XElement("validTo", proposal.validTo.ToString("yyyy-MM-dd")),
                                        new XElement("payment", proposal.payment),
                                        new XElement("vacancies", proposal.vacancies),
                                        new XElement("global_skills", proposal.globalSkills),
                                        new XElement("job_related_skills", proposal.jobRelatedSkills),
                                        new XElement("benefits", proposal.benefits),
                                        new XElement("contact", proposal.contact),
                                        new XElement("supervisor_firstname", proposal.supervisorFirstName),
                                        new XElement("supervisor_lastname", proposal.superVisorLastName),
                                        new XElement("supervisor_email", proposal.superVisorEmail),
                                        xmlTargets,
                                        xmlDegrees,
                                        xmlAreas,
                                        xmlLanguages,
                                        xmlPromotedSkills,
                                        xmlRequiredSkills);

                xmlDoc.Add(xmlProposal);
            }
            else
            {
                xmlDoc.Add(new XElement("proposal", "No proposal found..."));

                return xmlDoc;
            }

            return xmlDoc;
        }

        /// <summary>
        /// Retrieves all offer´s by word search criteria
        /// </summary>
        /// <param name="word">String pattern to search(optional)</param>
        /// <returns>Xml document with offers´ information</returns>
        [HttpGet]
        public XDocument OfferByWord(String word)
        {
            XDocument xmlAllProposals = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"));

            Models.PraxisDBEntities db = new Models.PraxisDBEntities();

            List<ProposalInfo> proposals = null;

            if (String.IsNullOrEmpty(word))
            {
                try
                {
                    proposals = (from props in db.Proposals
                                 join cities in db.Cities on props.cityId equals cities.id
                                 join countries in db.Countries on cities.countryId equals countries.id
                                 join institituions in db.Institutions on props.institutionId equals institituions.id
                                 join hosts in db.Institutions on props.hostId equals hosts.id
                                 join providers in db.Users on props.providerId equals providers.id
                                 into info
                                 from p in info.DefaultIfEmpty()
                                 select new ProposalInfo()
                                 {
                                     id = props.id,
                                     title = props.title,
                                     description = props.description,
                                     city = cities.name,
                                     country = countries.name,
                                     institution = institituions.name,
                                     host = hosts.name,
                                     startDate = props.startDate,
                                     endDate = props.endDate,
                                     validTo = props.validTo,
                                     supervisorFirstName = props.supervisorFirstName,
                                     superVisorLastName = props.supervisorLastName,
                                     superVisorEmail = props.supervisorEmail,
                                     payment = props.payment,
                                     vacancies = props.vacancies,
                                     globalSkills = props.globalSkills,
                                     jobRelatedSkills = props.jobRelatedSkills,
                                     benefits = props.benefits,
                                     contact = props.contact
                                 }).ToList();

                }
                catch (Exception ex)
                {

                    Utils.writeLog("[Controller: PraxisWebservice]\n[Action method: PraxisOffers]\n[Error conditions: Start date and end date are null]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    xmlAllProposals.Add(new XElement("error", "It was not possible to retrieve information..."));
                }
            }
            else
            {

                try
                {
                    proposals = (from props in db.Proposals
                                 join cities in db.Cities on props.cityId equals cities.id
                                 join countries in db.Countries on cities.countryId equals countries.id
                                 join institituions in db.Institutions on props.institutionId equals institituions.id
                                 join hosts in db.Institutions on props.hostId equals hosts.id
                                 join providers in db.Users on props.providerId equals providers.id
                                 into info
                                 from p in info.DefaultIfEmpty()
                                 select new ProposalInfo()
                                 {
                                     id = props.id,
                                     title = props.title,
                                     description = props.description,
                                     city = cities.name,
                                     country = countries.name,
                                     institution = institituions.name,
                                     host = hosts.name,
                                     startDate = props.startDate,
                                     endDate = props.endDate,
                                     validTo = props.validTo,
                                     supervisorFirstName = props.supervisorFirstName,
                                     superVisorLastName = props.supervisorLastName,
                                     superVisorEmail = props.supervisorEmail,
                                     payment = props.payment,
                                     vacancies = props.vacancies,
                                     globalSkills = props.globalSkills,
                                     jobRelatedSkills = props.jobRelatedSkills,
                                     benefits = props.benefits,
                                     contact = props.contact
                                 }).Where(p =>
                                     p.benefits.IndexOf(word) != -1 || p.city.IndexOf(word) != -1 ||
                                     p.country.IndexOf(word) != -1 || p.description.IndexOf(word) != -1 ||
                                     p.globalSkills.IndexOf(word) != -1 || p.jobRelatedSkills.IndexOf(word) != -1 ||
                                     p.payment.IndexOf(word) != -1 || p.title.IndexOf(word) != -1 ||
                                     p.vacancies.IndexOf(word) != -1
                                     ).Distinct().ToList();

                }
                catch (Exception ex)
                {

                    Utils.writeLog("[Controller: PraxisWebservice]\n[Action method: PraxisOffers]\n[Error conditions: Start date and end date are null]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    xmlAllProposals.Add(new XElement("error", "It was not possible to retrieve information..."));
                }
            }

            if (proposals != null && proposals.Count() > 0)
            {
                XElement xmlProposals = new XElement("proposals");

                foreach (ProposalInfo proposal in proposals)
                {
                    IEnumerable<Models.Target> allTargets = from targets in db.Targets
                                                            join propTargets in db.ProposalsTargets on targets.id equals propTargets.targetId
                                                            where propTargets.proposalId == proposal.id
                                                            select targets;

                    IEnumerable<Models.StudyDegree> allDegrees = from degrees in db.StudyDegrees
                                                                 join propDegrees in db.ProposalStudyDegrees on degrees.id equals propDegrees.studyDegreeId
                                                                 where propDegrees.proposalId == proposal.id
                                                                 select degrees;

                    IEnumerable<Models.StudyArea> allAreas = from areas in db.StudyAreas
                                                             join propAreas in db.ProposalStudyAreas on areas.id equals propAreas.studyAreaId
                                                             where propAreas.proposalId == proposal.id
                                                             select areas;

                    IEnumerable<Models.Language> allLanguages = from languages in db.Languages
                                                                join propLanguages in db.ProposalLanguages on languages.id equals propLanguages.languageId
                                                                where propLanguages.proposalId == proposal.id
                                                                select languages;

                    IEnumerable<Models.Skill> allPromotedSkills = from skills in db.Skills
                                                                  join promotedSkills in db.PromotedSkills on skills.id equals promotedSkills.skillId
                                                                  where promotedSkills.proposalId == proposal.id
                                                                  select skills;

                    IEnumerable<Models.Skill> allRequiredSkills = from skills in db.Skills
                                                                  join requiredSkills in db.RequiredSkills on skills.id equals requiredSkills.skillId
                                                                  where requiredSkills.proposalId == proposal.id
                                                                  select skills;

                    XElement xmlTargets = new XElement("targets");
                    XElement xmlDegrees = new XElement("study_degrees");
                    XElement xmlAreas = new XElement("study_areas");
                    XElement xmlLanguages = new XElement("languages");
                    XElement xmlPromotedSkills = new XElement("skills_to_promote");
                    XElement xmlRequiredSkills = new XElement("required_skills");


                    if (allTargets != null && allTargets.Count() > 0)
                        foreach (Models.Target target in allTargets)
                            xmlTargets.Add(new XElement("target", target.name));

                    if (allDegrees != null && allDegrees.Count() > 0)
                        foreach (Models.StudyDegree degree in allDegrees)
                            xmlDegrees.Add(new XElement("study_degree", degree.name));

                    if (allAreas != null && allAreas.Count() > 0)
                        foreach (Models.StudyArea area in allAreas)
                            xmlAreas.Add(new XElement("study_area", area.name));

                    if (allLanguages != null && allLanguages.Count() > 0)
                        foreach (Models.Language language in allLanguages)
                            xmlLanguages.Add(new XElement("language", language.name));

                    if (allPromotedSkills != null && allPromotedSkills.Count() > 0)
                        foreach (Models.Skill skill in allPromotedSkills)
                            xmlPromotedSkills.Add(new XElement("skill_to_promote", skill.name));

                    if (allRequiredSkills != null && allRequiredSkills.Count() > 0)
                        foreach (Models.Skill skill in allRequiredSkills)
                            xmlRequiredSkills.Add(new XElement("required_skill", skill.name));

                    XElement xmlProposal = new XElement("proposal",
                                            new XElement("proposal_id", Convert.ToString(proposal.id)),
                                            new XElement("title", proposal.title),
                                            new XElement("description", proposal.description),
                                            new XElement("city", proposal.city),
                                            new XElement("country", proposal.country),
                                            new XElement("institution", proposal.institution),
                                            new XElement("host", proposal.host),
                                            new XElement("start_date", proposal.startDate.ToString("yyyy-MM-dd")),
                                            new XElement("end_date", proposal.endDate.ToString("yyyy-MM-dd")),
                                            new XElement("validTo", proposal.validTo.ToString("yyyy-MM-dd")),
                                            new XElement("payment", proposal.payment),
                                            new XElement("vacancies", proposal.vacancies),
                                            new XElement("global_skills", proposal.globalSkills),
                                            new XElement("job_related_skills", proposal.jobRelatedSkills),
                                            new XElement("benefits", proposal.benefits),
                                            new XElement("contact", proposal.contact),
                                            new XElement("supervisor_firstname", proposal.supervisorFirstName),
                                            new XElement("supervisor_lastname", proposal.superVisorLastName),
                                            new XElement("supervisor_email", proposal.superVisorEmail),
                                            xmlTargets,
                                            xmlDegrees,
                                            xmlAreas,
                                            xmlLanguages,
                                            xmlPromotedSkills,
                                            xmlRequiredSkills);

                    xmlProposals.Add(xmlProposal);
                }

                xmlAllProposals.Add(xmlProposals);
            }
            else
            {
                xmlAllProposals.Add(new XElement("proposals", "There is no proposals"));

                return xmlAllProposals;
            }

            return xmlAllProposals;
        }

        private class ProposalInfo
        {
            public int id;
            public String title;
            public String description;
            public String city;
            public String country;
            public String institution;
            public String host;
            public DateTime startDate;
            public DateTime endDate;
            public DateTime validTo;
            public String payment;
            public String vacancies;
            public String globalSkills;
            public String jobRelatedSkills;
            public String benefits;
            public String contact;
            public String supervisorFirstName;
            public String superVisorLastName;
            public String superVisorEmail;
        }
    }
}