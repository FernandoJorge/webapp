﻿using PRAXISNETWORK.Models;
using PRAXISNETWORK.ModelViews.Proposals;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Controllers
{
    /// <summary>
    /// Handles requests concerned about proposal´s offers
    /// </summary>
    public class ProposalsController : Controller
    {
        /// <summary>
        /// Shows main html page with all proposals´ offers available
        /// </summary>
        /// <param name="page">Pagination´s page number</param>
        /// <param name="pageSize">Pagination´s number of proposals displayed by page</param>
        /// <returns>Html page with all proposals´ offers</returns>
        public ActionResult Index(int page = 1, int pageSize = 5)
        {
            ProposalSearch pSearch = new ProposalSearch();
            pSearch.page = page;
            pSearch.pageSize = pageSize;
            ViewBag.Title = "Find project/Internship";

            return View(pSearch);
        }

        /// <summary>
        /// Shows a specific proposal´s offer information
        /// </summary>
        /// <param name="Id">Proposal´s offer identification number</param>
        /// <returns>Html page with proposal´s offer information</returns>
        public ActionResult ProposalShow(int Id)
        {
            PraxisDBEntities db = new PraxisDBEntities();

            ProposalShow currentProposal = (from proposals in db.Proposals
                                            join cities in db.Cities on proposals.cityId equals cities.id
                                            join countries in db.Countries on cities.countryId equals countries.id
                                            join institutions in db.Institutions on proposals.institutionId equals institutions.id
                                            join hosts in db.Institutions on proposals.hostId equals hosts.id
                                            where proposals.id == Id && proposals.visible == true && proposals.deleted == false && proposals.applyOnPraxis == true
                                            select new ProposalShow
                                            {
                                                id = proposals.id,
                                                providerId = proposals.providerId,
                                                title = proposals.title,
                                                description = proposals.description,
                                                city = cities.name,
                                                country = countries.name,
                                                supervisorFirtLastName = proposals.supervisorFirstName + " " + proposals.supervisorLastName,
                                                supervisorEmail = proposals.supervisorEmail,
                                                startDate = proposals.startDate,
                                                endDate = proposals.endDate,
                                                validationDate = proposals.validTo,
                                                payment = proposals.payment,
                                                vacancies = proposals.vacancies,
                                                benefits = proposals.benefits,
                                                globalsSkills = proposals.globalSkills,
                                                jobRelatedSkills = proposals.jobRelatedSkills,
                                                contact = proposals.contact,
                                                Institution = institutions.name,
                                                host = hosts.name
                                            }).FirstOrDefault();

            IEnumerable<Language> allLanguages = from languages in db.Languages
                                                 join propLanguages in db.ProposalLanguages on languages.id equals propLanguages.languageId
                                                 where propLanguages.proposalId == Id
                                                 select languages;

            IEnumerable<Skill> allPromotedSkills = from skills in db.Skills
                                                   join promoteSkills in db.PromotedSkills on skills.id equals promoteSkills.skillId
                                                   where promoteSkills.proposalId == Id
                                                   select skills;

            IEnumerable<Target> allTargets = from targets in db.Targets
                                             join propTargets in db.ProposalsTargets on targets.id equals propTargets.targetId
                                             where propTargets.proposalId == Id
                                             select targets;

            IEnumerable<StudyArea> allAreas = from areas in db.StudyAreas
                                              join propAreas in db.ProposalStudyAreas on areas.id equals propAreas.studyAreaId
                                              where propAreas.proposalId == Id
                                              select areas;

            IEnumerable<StudyDegree> allDegrees = from degrees in db.StudyDegrees
                                                  join propDegrees in db.ProposalStudyDegrees on degrees.id equals propDegrees.studyDegreeId
                                                  where propDegrees.proposalId == Id
                                                  select degrees;

            // Languages
            if (allLanguages.ToList() != null && allLanguages.ToList().Count() > 0)
                foreach (Language language in allLanguages)
                    currentProposal.languages += language.name + "; ";

            // Skills to promote
            if (allPromotedSkills.ToList() != null && allPromotedSkills.ToList().Count() > 0)
                foreach (Skill skill in allPromotedSkills)
                    currentProposal.promotedSkills += skill.name + "; ";

            // Targets
            if (allTargets.ToList() != null && allTargets.ToList().Count() > 0)
                foreach (Target target in allTargets)
                    currentProposal.targets += target.name + "; ";

            // Study areas
            if (allAreas.ToList() != null && allAreas.ToList().Count() > 0)
                foreach (StudyArea area in allAreas)
                    currentProposal.studyAreas += area.name + "; ";

            // Study degrees
            if (allDegrees.ToList() != null && allDegrees.ToList().Count() > 0)
                foreach (StudyDegree degree in allDegrees)
                    currentProposal.studyDegrees += degree.name + "; ";

            // Format dates
            currentProposal.startDateText = currentProposal.startDate.ToString("yyyy-MM-dd");
            currentProposal.endDateText = currentProposal.endDate.ToString("yyyy-MM-dd");
            currentProposal.validationDateText = currentProposal.validationDate.ToString("yyyy-MM-dd");

            // Null values
            if (String.IsNullOrEmpty(currentProposal.globalsSkills))
                currentProposal.globalsSkills = "None";

            if (String.IsNullOrEmpty(currentProposal.jobRelatedSkills))
                currentProposal.jobRelatedSkills = "None";

            if (String.IsNullOrEmpty(currentProposal.payment))
                currentProposal.payment = "None";

            if (String.IsNullOrEmpty(currentProposal.vacancies))
                currentProposal.vacancies = "None";


            if (Session["userId"] != null)
            {
                int userId = (int)Session["userId"];

                // Check if the provider user has applications for this proposal
                if ((int)Session["userType"] == 1)
                {
                    IEnumerable<Models.Proposal> allProposals = from props in db.Proposals
                                                                join apps in db.Applications on props.id equals apps.proposalId
                                                                where props.providerId == userId
                                                                select props;

                    if (allProposals != null && allProposals.Count() > 0)
                        currentProposal.hasApplications = true;
                    else currentProposal.hasApplications = false;
                }

                // Check if the user as a student already has applied to the proposal
                if ((int)Session["userType"] == 2)
                {

                    IEnumerable<Application> applications = from apps in db.Applications
                                                            where apps.userId == userId && apps.proposalId == currentProposal.id
                                                            select apps;

                    try
                    {
                        int propId = -1;
                        propId = applications.FirstOrDefault().proposalId;

                        if (propId != -1)
                            currentProposal.userAlreadyApplied = true;
                    }
                    catch (NullReferenceException ex)
                    {
                        currentProposal.userAlreadyApplied = false;
                    }
                }
                else
                {
                    currentProposal.userAlreadyApplied = false;
                }
            }


            ViewBag.Title = currentProposal.title;

            return View("ProposalShow", currentProposal);
        }

        /// <summary>
        /// Shows html page to add a new proposal´s offer by a provider
        /// </summary>
        /// <returns>Html page for the new proposal´s offer</returns>
        public ActionResult NewProposal()
        {
            if (Session["userId"] != null && (int)Session["userType"] == 1)
            {
                ModelViews.Proposals.Proposal proposal = new ModelViews.Proposals.Proposal();
                ViewBag.Title = "New proposal";
                ViewBag.UserName = (String)Session["userName"];

                return View(proposal);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Saves a new proposal´s offer made by a provider
        /// </summary>
        /// <param name="proposal">Model object with new proposal´s offer data</param>
        /// <returns>Html page with process´ result</returns>
        public ActionResult SaveProposal(ModelViews.Proposals.Proposal proposal)
        {
            // Tests if exists a session
            if (Session["userId"] == null || (int)Session["userType"] != 1)
                return RedirectToAction("Index", "Login");

            if (ModelState.IsValid)
            {
                int proposalNewrecordId;

                try
                {
                    using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
                    {

                        PraxisDBEntities db1 = new PraxisDBEntities();
                        PraxisDBEntities db2 = new PraxisDBEntities();

                        Models.Proposal proposalRecord = new Models.Proposal();

                        proposalRecord.supervisorFirstName = proposal.superVisorFirstName;
                        proposalRecord.supervisorLastName = proposal.superVisorLastName;
                        proposalRecord.supervisorEmail = proposal.superVisorEmail;
                        proposalRecord.title = proposal.title;
                        proposalRecord.description = proposal.description;
                        proposalRecord.startDate = proposal.startDate;
                        proposalRecord.endDate = proposal.endDate;
                        proposalRecord.submitDate = DateTime.Now.Date;
                        proposalRecord.validTo = proposal.validTo;

                        if (proposal.payment != null)
                            proposalRecord.payment = proposal.payment;

                        if (proposal.vacancies != null)
                            proposalRecord.vacancies = proposal.vacancies;

                        if (proposal.globalSkills != null)
                            proposalRecord.globalSkills = proposal.globalSkills;

                        if (proposalRecord.jobRelatedSkills != null)
                            proposalRecord.jobRelatedSkills = proposal.jobRelatedSkills;

                        if (proposal.benefits != null)
                            proposalRecord.benefits = proposal.benefits;

                        proposalRecord.providerId = Convert.ToInt32(Session["userId"]);
                        proposalRecord.visible = true;
                        proposalRecord.applyOnPraxis = true;
                        proposalRecord.deleted = false;
                        proposalRecord.contact = proposal.contact;
                        proposalRecord.cityId = Convert.ToInt32(proposal.city.Split(',')[0]);
                        proposalRecord.countryId = Convert.ToInt32(proposal.city.Split(',')[1]);

                        if (!proposal.institution.Equals("null institution"))
                            proposalRecord.institutionId = Convert.ToInt32(proposal.institution);

                        if (!proposal.host.Equals("null institution"))
                            proposalRecord.hostId = Convert.ToInt32(proposal.host);

                        db1.Proposals.Add(proposalRecord);
                        db1.SaveChanges();
                        proposalNewrecordId = proposalRecord.id;

                        // Required Skills
                        foreach (String skill in proposal.requiredSkills)
                        {

                            db2.RequiredSkills.Add(new RequiredSkill
                            {

                                proposalId = proposalNewrecordId,
                                skillId = Convert.ToInt32(skill)
                            });
                        }

                        // Promoted Skills
                        foreach (String skill in proposal.promotedSkills)
                        {

                            db2.PromotedSkills.Add(new PromotedSkill
                            {

                                proposalId = proposalNewrecordId,
                                skillId = Convert.ToInt32(skill)
                            });
                        }

                        // Languages
                        foreach (String languageId in proposal.languages)
                        {

                            db2.ProposalLanguages.Add(new ProposalLanguage
                            {

                                proposalId = proposalNewrecordId,
                                languageId = Convert.ToInt32(languageId)
                            });
                        }

                        // Study Areas
                        foreach (String area in proposal.studyAreas)
                        {
                            db2.ProposalStudyAreas.Add(new ProposalStudyArea
                            {

                                proposalId = proposalNewrecordId,
                                studyAreaId = Convert.ToInt32(area)
                            });
                        }

                        // Study degrees
                        foreach (String degree in proposal.studyDegrees)
                        {

                            db2.ProposalStudyDegrees.Add(new ProposalStudyDegree
                            {

                                proposalId = proposalNewrecordId,
                                studyDegreeId = Convert.ToInt32(degree)
                            });
                        }

                        // Targets
                        foreach (String target in proposal.targets)
                        {

                            db2.ProposalsTargets.Add(new ProposalTarget
                            {

                                proposalId = proposalNewrecordId,
                                targetId = Convert.ToInt32(target)
                            });
                        }

                        db2.SaveChanges();

                        ts.Complete();
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Title = "New Proposal´s register result";
                    ViewBag.Error = "Occurred an error while saving your proposal...";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterResponse");
                }

                // Newsletter to all subscribers
                Task t = Task.Factory.StartNew(() => SendNewsletter(proposalNewrecordId));

            }
            else
            {

                return View("NewProposal", proposal);
            }

            ViewBag.Title = "New Proposal´s register result";
            ViewBag.Success = "The new proposal was successfuly saved!";
            return View("RegisterResponse");

        }

        /// <summary>
        /// Handles the request to search proposal´s offer from Master page field
        /// </summary>
        /// <returns>Redirects to 'ProposalsSearch' action method to return the filtered proposals</returns>
        public ActionResult ProposalsSearchMaster()
        {
            ModelViews.Proposals.ProposalSearch pSearch = new ModelViews.Proposals.ProposalSearch();
            pSearch.page = 1;
            pSearch.pageSize = 5;
            pSearch.searchField = Request.Form["searchField"];

            return View("ProposalsSearch", pSearch);
        }

        /// <summary>
        /// Handles the request for the search of proposal´s offers by word
        /// </summary>
        /// <param name="model">Model object about pagination and search data to filter the proposal´s offer</param>
        /// <param name="search">Word to filter the search</param>
        /// <param name="page">Pagination´s page number</param>
        /// <param name="pageSize">Pagination´s number of proposal´s offers by page</param>
        /// <returns>Html page with filtered proposal´s offers</returns>
        public ActionResult ProposalsSearch(ModelViews.Proposals.ProposalSearch model, String search, int page = 1, int pageSize = 5)
        {
            ViewBag.Title = "Find project/Internship";

            if (page == 1)
            {
                model.page = page;
                model.pageSize = pageSize;

                return View(model);
            }
            else
            {

                ModelViews.Proposals.ProposalSearch pSearch = new ModelViews.Proposals.ProposalSearch();
                pSearch.page = page;
                pSearch.pageSize = pageSize;
                pSearch.searchField = search;

                return View(pSearch);
            }

        }

        /// <summary>
        /// Handles the request for the search of proposal´s offers by account of specific parameters
        /// </summary>
        /// <param name="model">Model object about pagination and search data to filter the proposal´s offer</param>
        /// <param name="targets">Type of proposal´s targets constraints</param>
        /// <param name="degrees">Type of proposal´s study degrees constraints</param>
        /// <param name="durations">Duration of proposal</param>
        /// <param name="country">Country for the proposal</param>
        /// <param name="area">Type of proposal´s study area</param>
        /// <param name="page">Pagination´s page number</param>
        /// <param name="pageSize">Pagination´s number of proposal´s offers by page</param>
        /// <returns>Html page with all the filtered proposals</returns>
        public ActionResult ProposalsFilter(ModelViews.Proposals.ProposalSearch model, String[] targets, String[] degrees, String[] durations, String country, String area, int page = 1, int pageSize = 5)
        {
            ViewBag.Title = "Find project/Internship";

            if (page == 1)
            {
                model.page = page;
                model.pageSize = pageSize;

                return View(model);
            }
            else
            {

                ModelViews.Proposals.ProposalSearch pSearch = new ModelViews.Proposals.ProposalSearch();
                pSearch.page = page;
                pSearch.pageSize = pageSize;
                pSearch.targets = targets;
                pSearch.studyDegrees = degrees;
                pSearch.durations = durations;
                pSearch.country = country;
                pSearch.studyArea = area;

                return View(pSearch);
            }
        }

        /// <summary>
        /// Deletes a proposal by a provider
        /// </summary>
        /// <param name="proposalId">Proposal´s identification number</param>
        /// <returns>Html page with the process´ result</returns>
        public ActionResult DeleteProposal(int proposalId)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 1)
            {

                int userId = (int)Session["userId"];
                ViewBag.Title = "Proposal deletion´s result";
                PraxisDBEntities db = new PraxisDBEntities();

                // Delete all proposal´s applications docs
                IEnumerable<ApplicationDocs> allApplicationDocs = from appDocs in db.ApplicationDocs
                                                                  where appDocs.proposalId == proposalId
                                                                  select appDocs;

                if (allApplicationDocs.ToList() != null && allApplicationDocs.ToList().Count() > 0)
                {
                    List<String> fullPaths = new List<String>();

                    foreach (ApplicationDocs appDoc in allApplicationDocs)
                    {
                        // Delete physical document from server
                        String path = Server.MapPath(appDoc.path);

                        if (!fullPaths.Contains(System.IO.Path.GetDirectoryName(path)))
                            fullPaths.Add(System.IO.Path.GetDirectoryName(path));

                        try
                        {
                            if (System.IO.File.Exists(path))
                                System.IO.File.Delete(path);
                        }
                        catch (Exception ex)
                        {
                            ViewBag.Error = "Occurred an error while deleting all proposal´s application documents...Please try again or contact the administrator.";
                            Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                            return View("RegisterResponse");
                        }

                        // Remove from database document
                        db.ApplicationDocs.Remove(appDoc);
                    }

                    // Delete all diretories of the previous documents
                    foreach (String path in fullPaths)
                    {
                        try
                        {
                            if (!Directory.Exists(path))
                                Directory.Delete(path);
                        }
                        catch (Exception ex)
                        {
                            ViewBag.Error = "Occurred an error while deleting all proposal´s directories application documents...Please try again or contact the administrator.";
                            Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                            return View("RegisterResponse");
                        }
                    }
                }

                // Delete all proposal´s applications from database
                IEnumerable<Application> allApplications = from apps in db.Applications
                                                           where apps.proposalId == proposalId
                                                           select apps;

                if (allApplications.ToList() != null && allApplications.ToList().Count() > 0)
                    foreach (Application app in allApplications)
                        db.Applications.Remove(app);

                // Delete all skills to promote for the proposal
                IEnumerable<PromotedSkill> allPromotedSkills = from skills in db.PromotedSkills
                                                               where skills.proposalId == proposalId
                                                               select skills;

                if (allPromotedSkills.ToList() != null && allPromotedSkills.ToList().Count() > 0)
                    foreach (PromotedSkill skill in allPromotedSkills)
                        db.PromotedSkills.Remove(skill);

                // Delete all proposal´s languages
                IEnumerable<ProposalLanguage> allProposalLanguages = from langs in db.ProposalLanguages
                                                                     where langs.proposalId == proposalId
                                                                     select langs;

                if (allProposalLanguages.ToList() != null && allProposalLanguages.ToList().Count() > 0)
                    foreach (ProposalLanguage language in allProposalLanguages)
                        db.ProposalLanguages.Remove(language);

                // Delete all proposal´s study areas
                IEnumerable<ProposalStudyArea> allProposalAreas = from areas in db.ProposalStudyAreas
                                                                  where areas.proposalId == proposalId
                                                                  select areas;

                if (allProposalAreas.ToList() != null && allProposalAreas.ToList().Count() > 0)
                    foreach (ProposalStudyArea area in allProposalAreas)
                        db.ProposalStudyAreas.Remove(area);

                // Delete all proposal´s study degrees
                IEnumerable<ProposalStudyDegree> allProposalDegrees = from degrees in db.ProposalStudyDegrees
                                                                      where degrees.proposalId == proposalId
                                                                      select degrees;

                if (allProposalDegrees.ToList() != null && allProposalDegrees.ToList().Count() > 0)
                    foreach (ProposalStudyDegree degree in allProposalDegrees)
                        db.ProposalStudyDegrees.Remove(degree);

                // Delete all proposal´s targets
                IEnumerable<ProposalTarget> allProposalTargets = from targets in db.ProposalsTargets
                                                                 where targets.proposalId == proposalId
                                                                 select targets;

                if (allProposalTargets.ToList() != null && allProposalTargets.ToList().Count() > 0)
                    foreach (ProposalTarget target in allProposalTargets)
                        db.ProposalsTargets.Remove(target);

                // Delete all proposal´s required skills
                IEnumerable<RequiredSkill> allRequiredSkills = from skills in db.RequiredSkills
                                                               where skills.proposalId == proposalId
                                                               select skills;

                if (allRequiredSkills.ToList() != null && allRequiredSkills.ToList().Count() > 0)
                    foreach (RequiredSkill skill in allRequiredSkills)
                        db.RequiredSkills.Remove(skill);

                // Delete proposal
                IEnumerable<Models.Proposal> proposals = from props in db.Proposals
                                                         where props.id == proposalId
                                                         select props;

                Models.Proposal proposal = null;

                try
                {
                    proposal = proposals.FirstOrDefault();
                    db.Proposals.Remove(proposal);
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error while deleting the proposal information...Please try again or contact the administrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterResponse");
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error while deleting the proposal...Please try again or contact the administrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterResponse");
                }

            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            ViewBag.Success = "YOUR PROPOSAL HAS BEEN DELETED WITH SUCCESS!!!";
            return View("RegisterResponse");
        }

        /// <summary>
        /// Shows all information about a proposal´s offer and permits its edition by a provider
        /// </summary>
        /// <param name="proposalId">Proposal´s identification number</param>
        /// <returns>html page for the proposal´s offer edition</returns>
        public ActionResult ProposalEdition(int proposalId)
        {
            if (Session["userId"] != null && ((int)Session["userType"] == 1 || (int)Session["userType"] == 3))
            {
                Models.PraxisDBEntities db = new Models.PraxisDBEntities();
                ModelViews.Proposals.Proposal currentProposal = new ModelViews.Proposals.Proposal();
                ViewBag.Title = "Proposal Edition";

                // Proposal record
                IEnumerable<Models.Proposal> proposals = from props in db.Proposals
                                                         where props.id == proposalId
                                                         select props;

                Models.Proposal proposal = null;

                try
                {
                    proposal = proposals.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error while accessing the proposal information...Please try again or contact the administrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterResponse");
                }

                currentProposal.id = proposal.id;
                currentProposal.title = proposal.title;
                currentProposal.description = proposal.description;
                currentProposal.startDate = proposal.startDate;
                currentProposal.endDate = proposal.endDate;
                currentProposal.validTo = proposal.validTo;
                currentProposal.payment = proposal.payment;
                currentProposal.vacancies = proposal.vacancies;
                currentProposal.globalSkills = proposal.globalSkills;
                currentProposal.jobRelatedSkills = proposal.jobRelatedSkills;
                currentProposal.benefits = proposal.benefits;
                currentProposal.superVisorFirstName = proposal.supervisorFirstName;
                currentProposal.superVisorLastName = proposal.supervisorLastName;
                currentProposal.superVisorEmail = proposal.supervisorEmail;

                currentProposal.targets = new String[proposal.targets.Count()];

                for (int i = 0; i < proposal.targets.Count(); i++)
                    currentProposal.targets[i] = proposal.targets[i].targetId.ToString();

                currentProposal.requiredSkills = new String[proposal.requiredSkills.Count()];

                for (int i = 0; i < proposal.requiredSkills.Count(); i++)
                    currentProposal.requiredSkills[i] = proposal.requiredSkills[i].skillId.ToString();

                currentProposal.promotedSkills = new String[proposal.promotedSkills.Count()];

                for (int i = 0; i < proposal.promotedSkills.Count(); i++)
                    currentProposal.promotedSkills[i] = proposal.promotedSkills[i].skillId.ToString();

                currentProposal.studyAreas = new String[proposal.studyAreas.Count()];

                for (int i = 0; i < proposal.studyAreas.Count(); i++)
                    currentProposal.studyAreas[i] = proposal.studyAreas[i].studyAreaId.ToString();

                currentProposal.studyDegrees = new String[proposal.studyDegrees.Count()];

                for (int i = 0; i < proposal.studyDegrees.Count(); i++)
                    currentProposal.studyDegrees[i] = proposal.studyDegrees[i].studyDegreeId.ToString();

                currentProposal.languages = new String[proposal.languages.Count()];

                for (int i = 0; i < proposal.languages.Count(); i++)
                    currentProposal.languages[i] = proposal.languages[i].languageId.ToString();

                currentProposal.city = Convert.ToString(proposal.cityId);
                currentProposal.institution = Convert.ToString(proposal.institutionId);
                currentProposal.host = Convert.ToString(proposal.hostId);
                currentProposal.contact = proposal.contact;


                return View(currentProposal);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Saves a proposal´s offer edited by a provider
        /// </summary>
        /// <param name="proposal">Proposal´s offer identification number</param>
        /// <returns>Html page with process´ result</returns>
        public ActionResult SaveProposalEdition(ModelViews.Proposals.Proposal proposal)
        {
            if (Session["userId"] != null && ((int)Session["userType"] == 1 || (int)Session["userType"] == 3))
            {

                if (ModelState.IsValid)
                {
                    ViewBag.Title = "Proposal´s update result";
                    PraxisDBEntities db = new PraxisDBEntities();

                    IEnumerable<Models.Proposal> proposals = from props in db.Proposals
                                                             where props.id == proposal.id
                                                             select props;

                    Models.Proposal proposalRecord = null;

                    try
                    {
                        proposalRecord = proposals.FirstOrDefault();
                    }
                    catch (NullReferenceException ex)
                    {
                        ViewBag.Error = "Occurred an error while accessing the proposal information...Please try again or contact the administrator.";
                        Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                        return View("RegisterResponse");
                    }

                    proposalRecord.supervisorFirstName = proposal.superVisorFirstName;
                    proposalRecord.supervisorLastName = proposal.superVisorLastName;
                    proposalRecord.supervisorEmail = proposal.superVisorEmail;
                    proposalRecord.title = proposal.title;
                    proposalRecord.description = proposal.description;
                    proposalRecord.startDate = proposal.startDate;
                    proposalRecord.endDate = proposal.endDate;
                    proposalRecord.validTo = proposal.validTo;

                    if (proposal.payment != null)
                        proposalRecord.payment = proposal.payment;

                    if (proposal.vacancies != null)
                        proposalRecord.vacancies = proposal.vacancies;

                    if (proposal.globalSkills != null)
                        proposalRecord.globalSkills = proposal.globalSkills;

                    if (proposalRecord.jobRelatedSkills != null)
                        proposalRecord.jobRelatedSkills = proposal.jobRelatedSkills;

                    if (proposal.benefits != null)
                        proposalRecord.benefits = proposal.benefits;

                    proposalRecord.contact = proposal.contact;
                    proposalRecord.cityId = Convert.ToInt32(proposal.city.Split(',')[0]);
                    proposalRecord.countryId = Convert.ToInt32(proposal.city.Split(',')[1]);

                    if (!proposal.institution.Equals("null institution"))
                        proposalRecord.institutionId = Convert.ToInt32(proposal.institution);

                    if (!proposal.host.Equals("null institution"))
                        proposalRecord.hostId = Convert.ToInt32(proposal.host);

                    db.Entry(proposalRecord).State = System.Data.Entity.EntityState.Modified;

                    // Delete old proposal´s required skills
                    IEnumerable<RequiredSkill> oldRequiredSkills = from skills in db.RequiredSkills
                                                                   where skills.proposalId == proposal.id
                                                                   select skills;

                    if (oldRequiredSkills.ToList() != null && oldRequiredSkills.ToList().Count() > 0)
                        foreach (RequiredSkill skill in oldRequiredSkills)
                            db.RequiredSkills.Remove(skill);

                    // Add new proposal´s Required Skills
                    foreach (String skill in proposal.requiredSkills)
                    {

                        db.RequiredSkills.Add(new RequiredSkill
                        {

                            proposalId = proposal.id,
                            skillId = Convert.ToInt32(skill)
                        });
                    }

                    // Delete old proposal´s Promoted Skills
                    IEnumerable<PromotedSkill> oldPromotedSkills = from skills in db.PromotedSkills
                                                                   where skills.proposalId == proposal.id
                                                                   select skills;

                    if (oldPromotedSkills.ToList() != null && oldPromotedSkills.ToList().Count() > 0)
                        foreach (PromotedSkill skill in oldPromotedSkills)
                            db.PromotedSkills.Remove(skill);

                    // Add new Promoted Skills
                    foreach (String skill in proposal.promotedSkills)
                    {

                        db.PromotedSkills.Add(new PromotedSkill
                        {

                            proposalId = proposal.id,
                            skillId = Convert.ToInt32(skill)
                        });
                    }

                    // Delete old proposal´s languages
                    IEnumerable<ProposalLanguage> oldLanguages = from langs in db.ProposalLanguages
                                                                 where langs.proposalId == proposal.id
                                                                 select langs;

                    if (oldLanguages.ToList() != null && oldLanguages.ToList().Count() > 0)
                        foreach (ProposalLanguage language in oldLanguages)
                            db.ProposalLanguages.Remove(language);

                    // Add new Languages
                    foreach (String languageId in proposal.languages)
                    {

                        db.ProposalLanguages.Add(new ProposalLanguage
                        {

                            proposalId = proposal.id,
                            languageId = Convert.ToInt32(languageId)
                        });
                    }

                    // Delete old proposal´s study areas
                    IEnumerable<ProposalStudyArea> oldStudyAreas = from areas in db.ProposalStudyAreas
                                                                   where areas.proposalId == proposal.id
                                                                   select areas;

                    if (oldStudyAreas.ToList() != null && oldStudyAreas.ToList().Count() > 0)
                        foreach (ProposalStudyArea area in oldStudyAreas)
                            db.ProposalStudyAreas.Remove(area);

                    // Add new Study Areas
                    foreach (String area in proposal.studyAreas)
                    {
                        db.ProposalStudyAreas.Add(new ProposalStudyArea
                        {

                            proposalId = proposal.id,
                            studyAreaId = Convert.ToInt32(area)
                        });
                    }

                    // Delete old proposal´s study degrees
                    IEnumerable<ProposalStudyDegree> oldStudyDegrees = from degrees in db.ProposalStudyDegrees
                                                                       where degrees.proposalId == proposal.id
                                                                       select degrees;

                    if (oldStudyDegrees.ToList() != null && oldStudyDegrees.ToList().Count() > 0)
                        foreach (ProposalStudyDegree degree in oldStudyDegrees)
                            db.ProposalStudyDegrees.Remove(degree);

                    // Add new Study degrees
                    foreach (String degree in proposal.studyDegrees)
                    {

                        db.ProposalStudyDegrees.Add(new ProposalStudyDegree
                        {

                            proposalId = proposal.id,
                            studyDegreeId = Convert.ToInt32(degree)
                        });
                    }

                    // Delete old proposal´s targets
                    IEnumerable<ProposalTarget> oldTargets = from targets in db.ProposalsTargets
                                                             where targets.proposalId == proposal.id
                                                             select targets;

                    if (oldTargets.ToList() != null && oldTargets.ToList().Count() > 0)
                        foreach (ProposalTarget target in oldTargets)
                            db.ProposalsTargets.Remove(target);

                    // Add new Targets
                    foreach (String target in proposal.targets)
                    {

                        db.ProposalsTargets.Add(new ProposalTarget
                        {

                            proposalId = proposal.id,
                            targetId = Convert.ToInt32(target)
                        });
                    }

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = "Occurred an error while updating your proposal...";
                        Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                        return View("RegisterResponse");
                    }

                    ViewBag.Success = "YOUR PROPOSAL HAS BEEN UPDATED SUCCESSFUL!!!";

                    return View("RegisterResponse");
                }
                else
                {
                    return View("ProposalEdition", proposal);
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Just an action method to dinamically create proposal offers
        /// </summary>
        /// <returns>Cretes proposal offers</returns>
        public String Populate()
        {

            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {

                PraxisDBEntities db1 = new PraxisDBEntities();
                PraxisDBEntities db2 = new PraxisDBEntities();
                Random rdm = new Random();

                Models.Proposal proposalRecord = new Models.Proposal();

                proposalRecord.supervisorFirstName = "Fernando" + Convert.ToString(rdm.Next(0, 200));
                proposalRecord.supervisorLastName = "Silva" + Convert.ToString(rdm.Next(0, 200));
                proposalRecord.supervisorEmail = "fjms.fjms@gmail.com";
                proposalRecord.title = Convert.ToString(rdm.Next(0, 200)) + "__James Bond 007__" + Convert.ToString(rdm.Next(0, 200));
                proposalRecord.description = "Secret Mission " + Convert.ToString(rdm.Next(0, 200));
                proposalRecord.startDate = DateTime.Now.AddMonths(rdm.Next(1, 13));
                proposalRecord.endDate = proposalRecord.startDate.AddMonths(6);
                proposalRecord.submitDate = proposalRecord.startDate.AddMonths(-2);
                proposalRecord.validTo = proposalRecord.startDate.AddDays(-1);
                proposalRecord.payment = Convert.ToString(rdm.Next(900, 3001)) + "€";
                proposalRecord.vacancies = "Bahamas_" + Convert.ToString(rdm.Next(1, 200));
                proposalRecord.globalSkills = "Global Skill > " + Convert.ToString(rdm.Next(1, 200));
                proposalRecord.jobRelatedSkills = "Job Skill > " + Convert.ToString(rdm.Next(1, 200));
                proposalRecord.benefits = "Benefit > " + Convert.ToString(rdm.Next(1, 200));
                proposalRecord.providerId = 23;
                proposalRecord.visible = true;
                proposalRecord.applyOnPraxis = true;
                proposalRecord.deleted = false;
                proposalRecord.contact = "fjms.fjms@gmail.com";
                proposalRecord.cityId = rdm.Next(1, 585);
                proposalRecord.countryId = rdm.Next(1, 240);
                proposalRecord.institutionId = rdm.Next(186, 371);
                proposalRecord.hostId = rdm.Next(186, 371);

                db1.Proposals.Add(proposalRecord);

                db1.SaveChanges();
                int proposalNewrecordId = proposalRecord.id;

                db2.RequiredSkills.Add(new RequiredSkill
                {

                    proposalId = proposalNewrecordId,
                    skillId = rdm.Next(1, 266)
                });

                db2.PromotedSkills.Add(new PromotedSkill
                {

                    proposalId = proposalNewrecordId,
                    skillId = rdm.Next(1, 266)
                });

                db2.ProposalLanguages.Add(new ProposalLanguage
                {

                    proposalId = proposalNewrecordId,
                    languageId = rdm.Next(1, 24)
                });

                db2.ProposalStudyAreas.Add(new ProposalStudyArea
                {

                    proposalId = proposalNewrecordId,
                    studyAreaId = rdm.Next(1, 84)
                });

                db2.ProposalStudyDegrees.Add(new ProposalStudyDegree
                {

                    proposalId = proposalNewrecordId,
                    studyDegreeId = rdm.Next(1, 4)
                });

                db2.ProposalsTargets.Add(new ProposalTarget
                {

                    proposalId = proposalNewrecordId,
                    targetId = rdm.Next(1, 3)
                });

                db2.SaveChanges();

                ts.Complete();
            }

            return "DONE!";

        }

        /// <summary>
        /// Sends a newsletter with the information about a proposal offer to all subscribers of the web application
        /// </summary>
        /// <param name="proposalId">Proposal´s identification number</param>
        private void SendNewsletter(int proposalId)
        {

            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<Subscriber> allSubscribers = from subs in db.Subscribers
                                                     select subs;

            IEnumerable<Models.Proposal> allProposals = from props in db.Proposals
                                                        where props.id == proposalId
                                                        select props;

            Models.Proposal currentProposal = null;

            try
            {
                currentProposal = allProposals.FirstOrDefault();

                if (allSubscribers.ToList() != null && allSubscribers.ToList().Count() > 0)
                {
                    int end = (int)(currentProposal.description.Length * 0.3);
                    String description = currentProposal.description.Substring(0, end) + "\n(Click the link below to see offer´s information...)";

                    String htmlDocument = "<HTML><BODY>";
                    htmlDocument += "<H2>" + currentProposal.title + "</H2>";
                    htmlDocument += "<HR />";
                    htmlDocument += "<P>" + description + "</P>";
                    htmlDocument += "<A href\"http://localhost:63178/Proposals/ProposalShow/" + currentProposal.id + "\">" + currentProposal.title + "</A>";
                    htmlDocument += "<HR />";
                    htmlDocument += "</BODY></HTML>";

                    Utils.sendEmailMessage(allSubscribers.Select(s => s.email).ToList(), "Newsletter", htmlDocument);
                }

            }
            catch (NullReferenceException ex)
            {
                Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
            }
        }

        /* JQUERY REQUESTS */

        /// <summary>
        /// Retrives all institutions that belongs to specific city
        /// </summary>
        /// <param name="cityIdCountryId">Infomation about the city´s identification number and country´s identification number - String format: "CITY_ID,COUNTRY_ID"</param>
        /// <returns>Dictionary: Key: Institution´s id, Value: Institution´s name</returns>
        public ActionResult InstitutionsOfCity(String cityIdCountryId)
        {
            if (Session["userId"] != null && ((int)Session["userType"] == 1 || (int)Session["userType"] == 3))
            {
                int cityId = Convert.ToInt32(cityIdCountryId.Split(',')[0]);

                PraxisDBEntities db = new PraxisDBEntities();

                IEnumerable<Institution> allInstituitions = from inst in db.Institutions
                                                            where inst.cityId == cityId
                                                            select inst;

                Dictionary<String, String> institutions = new Dictionary<String, String>();

                if (allInstituitions.ToList() != null && allInstituitions.ToList().Count() > 0)
                {
                    foreach (Institution inst in allInstituitions)
                        institutions.Add(Convert.ToString(inst.id), inst.name);
                }
                else
                {
                    if (institutions.Count() == 0)
                        institutions.Add("null institution", "No instituitions");
                }

                return Json(new { institutions = institutions }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
    }
}
