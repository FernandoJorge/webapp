﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Controllers
{
    /// <summary>
    /// Handles all requests of an authenticated provider
    /// </summary>
    public class ProviderController : Controller
    {
        /// <summary>
        /// Shows main board of management for the provider
        /// </summary>
        /// <returns>Html page with management board</returns>
        public ActionResult Index()
        {
            if (Session["userId"] != null && (int)Session["userType"] == 1)
            {

                ViewBag.Title = "Provider board";

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Shows all proposals´ offers made by the provider
        /// </summary>
        /// <param name="page">Pagination´s page number</param>
        /// <param name="pageSize">Pagination´s number of proposals to display by page</param>
        /// <returns>Html page with all proposals offers</returns>
        public ActionResult ProviderProposals(int page = 1, int pageSize = 5)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 1)
            {

                int userId = (int)Session["userID"];


                ViewBag.Title = "Your proposals";
                Models.ProviderProposals providerProps = new Models.ProviderProposals()
                {

                    userId = userId,
                    page = page,
                    pageSize = pageSize
                };

                return View(providerProps);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

    }
}
