﻿using PRAXISNETWORK.Models;
using PRAXISNETWORK.ModelViews.Register;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Controllers
{
    /// <summary>
    /// Handles all requests concerned with the user´s profile data
    /// </summary>
    public class RegisterController : Controller
    {
        /// <summary>
        /// Shows html page to choose between student and provider registration
        /// </summary>
        /// <returns>Html page</returns>
        public ActionResult Index()
        {
            ViewBag.Title = "Registration";

            return View();
        }

        /// <summary>
        /// Shows html page to fill the student´s data registration
        /// </summary>
        /// <returns>Html page</returns>
        public ActionResult Student()
        {
            ModelViews.Register.Applicant student = new ModelViews.Register.Applicant();
            ViewBag.Title = "Student registration";

            return View(student);
        }

        /// <summary>
        /// Shows html page to edit student´s data registration
        /// </summary>
        /// <returns>Html page</returns>
        public ActionResult StudentEdition()
        {
            if (Session["userId"] != null && (int)Session["userType"] == 2)
            {

                int userId = (int)Session["userId"];
                PraxisDBEntities db = new PraxisDBEntities();
                ModelViews.Register.Applicant student;
                ViewBag.Title = "Student edition";

                IEnumerable<User> users = from usrs in db.Users
                                          where usrs.id == userId
                                          select usrs;

                User user;

                try
                {
                    user = users.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error while accessing your profile...Please try again or contact the administrator...";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterResponse");
                }

                var countriesAndCities = from cts in db.Cities
                                         join ctrs in db.Countries on cts.countryId equals ctrs.id
                                         where cts.id == user.cityId
                                         select new
                                         {
                                             countryId = ctrs.id,
                                             cityId = cts.id,
                                         };
                try
                {
                    student = new ModelViews.Register.Applicant
                    {

                        about = user.about,
                        birthDate = user.birthDate,
                        city = (user.city == null ? null : Convert.ToString(countriesAndCities.FirstOrDefault().cityId)),
                        country = (user.city == null ? null : Convert.ToString(countriesAndCities.FirstOrDefault().countryId)),
                        email = user.email,
                        firstName = user.firstName,
                        gender = user.gender,
                        lastName = user.lastName,
                        picturePath = user.picture,
                        username = user.username
                    };
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error while delivering your profile...Please try again or contact the administrator...";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterResponse");
                }


                return View("StudentEdition", student);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Saves new student´s registration
        /// </summary>
        /// <param name="student">Model object with all student´s new data registration</param>
        /// <returns>Html page with process´ result</returns>
        public ActionResult SaveStudent(Applicant student)
        {
            if (ModelState.IsValid)
            {
                PraxisDBEntities db = new PraxisDBEntities();
                User studentRecord = new User();

                ViewBag.Title = "Register result";

                // HANDLE FILE UPLOAD
                if (student.picture != null && student.picture.ContentLength > 0)
                {
                    String dateTime = (DateTime.Now.Date + "_" + DateTime.Now.TimeOfDay).Replace(":", "_");
                    String fileName = Path.GetFileName(student.picture.FileName);
                    String relativePath = "~/Files/UsersPictures/" + dateTime + "_" + fileName;
                    String destinyPath = Server.MapPath(relativePath);

                    try
                    {
                        student.picture.SaveAs(destinyPath);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = "Occurred an error while saving your photo! Please try again or contact the adminsitrator.";
                        Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                        return View("RegisterResponse");
                    }

                    studentRecord.picture = relativePath;

                }

                studentRecord.username = student.username;
                studentRecord.firstName = student.firstName;
                studentRecord.lastName = student.lastName;
                studentRecord.gender = student.gender;
                studentRecord.birthDate = student.birthDate;
                studentRecord.email = student.email;
                studentRecord.password = Utils.encrypt(student.password);
                studentRecord.enabled = true;
                studentRecord.locked = false;
                studentRecord.userTypeId = 2;
                studentRecord.lastLogin = DateTime.Now;

                if (student.about != null)
                    studentRecord.about = student.about;

                if (student.city != "null city")
                    studentRecord.cityId = Convert.ToInt32(student.city);
                else studentRecord.cityId = null;

                db.Users.Add(studentRecord);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error while saving your registration! Please try again or contact the adminsitrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterReponse");
                }

                ViewBag.Success = "NEW STUDENT RECORD SAVED!!!";

                return View("RegisterResponse");
            }

            return View("Student", student);
        }

        /// <summary>
        /// Saves the edited information of a student´s registration
        /// </summary>
        /// <param name="student">Model object of the student´s data registration</param>
        /// <returns>Html page with process´ result</returns>
        public ActionResult UpdateStudent(Applicant student)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 2)
            {
                if (ModelState.IsValidField("firstName") &&
                    ModelState.IsValidField("lastName") &&
                    ModelState.IsValidField("gender") &&
                    ModelState.IsValidField("birthDate") &&
                    ModelState.IsValidField("about") &&
                    ModelState.IsValidField("password") &&
                    ModelState.IsValidField("passwordConfirmation") &&
                    ModelState.IsValidField("picture"))
                {
                    PraxisDBEntities db = new PraxisDBEntities();
                    int userId = (int)Session["userId"];

                    IEnumerable<User> users = from usrs in db.Users
                                              where usrs.id == userId
                                              select usrs;

                    User user;

                    try
                    {

                        user = users.FirstOrDefault();
                    }
                    catch (NullReferenceException ex)
                    {
                        ViewBag.Error = "Occurred an error whlie accessing your profile...Please try again or contact the administrator.";
                        Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                        return View("RegisterReponse");
                    }

                    ViewBag.Title = "Update profile result";

                    // HANDLE FILE UPLOAD
                    if (student.picture != null && student.picture.ContentLength > 0)
                    {
                        String dateTime = (DateTime.Now.Date + "_" + DateTime.Now.TimeOfDay).Replace(":", "_");
                        String fileName = Path.GetFileName(student.picture.FileName);
                        String relativePath = "~/Files/UsersPictures/" + dateTime + "_" + fileName;
                        String destinyPath = Server.MapPath(relativePath);

                        try
                        {
                            student.picture.SaveAs(destinyPath);
                        }
                        catch (Exception ex)
                        {
                            ViewBag.Error = "Occurred an error while saving your photo! Please try again or contact the adminsitrator.";
                            Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                            return View("RegisterResponse");
                        }

                        user.picture = relativePath;
                    }

                    user.firstName = student.firstName;
                    user.lastName = student.lastName;
                    user.gender = student.gender;
                    user.birthDate = student.birthDate;
                    user.password = Utils.encrypt(student.password);

                    if (student.about != null)
                        user.about = student.about;

                    if (student.city != "null city")
                        user.cityId = Convert.ToInt32(student.city);
                    else user.cityId = null;

                    // Specify as an update
                    db.Entry(user).State = System.Data.Entity.EntityState.Modified;

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = "Occurred an error trying to save your profile changes! Please try again or contact the adminsitrator.";
                        Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                        return View("RegisterResponse");
                    }

                    Session["userName"] = user.firstName + " " + user.lastName;
                }
                else
                {

                    return View("StudentEdition", student);
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            ViewBag.Success = "YOU PROFILE WAS SUCCESSFUL UDATED!!!";
            return View("RegisterResponse");
        }

        /// <summary>
        /// Shows student´s information
        /// </summary>
        /// <param name="id">Student´s identification number</param>
        /// <returns>Html page</returns>
        public ActionResult ShowStudent(int id)
        {
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<User> users = from usrs in db.Users
                                      where usrs.id == id && usrs.userTypeId == 2
                                      select usrs;

            User user = null;

            try
            {
                user = users.FirstOrDefault();
            }
            catch (NullReferenceException ex)
            {
                ViewBag.Error = "Occurred an error while accessing user´s profile...";
                Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message: " + ex.InnerException.Message + "\n\nNull record]");
                return View("RegisterResponse");
            }

            var citiesAndCountries = from cts in db.Cities
                                     join ctrs in db.Countries on cts.countryId equals ctrs.id
                                     where cts.id == user.cityId
                                     select new
                                     {
                                         city = cts.name,
                                         country = ctrs.name
                                     };

            ModelViews.Register.Applicant student = new ModelViews.Register.Applicant();

            student.username = user.username;
            student.firstName = user.firstName;
            student.lastName = user.lastName;
            student.email = user.email;
            student.city = (citiesAndCountries.FirstOrDefault() != null ? citiesAndCountries.FirstOrDefault().city : "No city");
            student.country = (citiesAndCountries.FirstOrDefault() != null ? citiesAndCountries.FirstOrDefault().country : "No country");

            ViewBag.Title = "Student information";

            return View(student);
        }

        /// <summary>
        /// Shows html page to fill provider´s data registration
        /// </summary>
        /// <returns>Html page</returns>
        public ActionResult Provider()
        {
            ModelViews.Register.Provider provider = new ModelViews.Register.Provider();

            ViewBag.Title = "Provider registration";

            return View(provider);
        }

        /// <summary>
        /// Shows html page to edit provider´s data registration
        /// </summary>
        /// <returns>Html page</returns>
        public ActionResult ProviderEdition()
        {
            if (Session["userId"] != null && (int)Session["userType"] == 1)
            {
                int userId = (int)Session["userID"];
                PraxisDBEntities db = new PraxisDBEntities();

                IEnumerable<User> users = from usrs in db.Users
                                          where usrs.id == userId
                                          select usrs;

                User user;

                try
                {

                    user = users.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error while accessing user´s profile...Please try again or contact the administrator";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterResponse");
                }

                ModelViews.Register.Provider provider = new ModelViews.Register.Provider();

                provider.username = user.username;
                provider.firstName = user.firstName;
                provider.lastName = user.lastName;

                if (user.institutionId != null)
                {
                    provider.isOldInstitution = "2";
                    provider.selectedInstitution = Convert.ToString(user.institutionId);
                    provider.city = null;
                }
                else
                {
                    provider.isOldInstitution = "1";
                    provider.selectedInstitution = null;

                    if (user.cityId == null)
                    {
                        provider.city = null;
                        provider.country = null;
                    }
                    else
                    {
                        provider.city = Convert.ToString(user.cityId);

                        IEnumerable<Country> countries = from ctrs in db.Countries
                                                         join cts in db.Cities on ctrs.id equals cts.countryId
                                                         where cts.id == user.cityId
                                                         select ctrs;

                        provider.country = Convert.ToString(countries.FirstOrDefault().id);
                    }
                }


                ViewBag.Title = "Provider editon";

                return View(provider);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Saves provider´s new data registration
        /// </summary>
        /// <param name="provider">Model object of the provider´s data registration</param>
        /// <returns>Html page with the process´ result</returns>
        public ActionResult SaveProvider(Provider provider)
        {
            PraxisDBEntities db = new PraxisDBEntities();
            User providerRecord = new User();

            ViewBag.Title = "Register result";

            if (ModelState.IsValidField("username") && ModelState.IsValidField("firstName") && ModelState.IsValidField("lastName") && ModelState.IsValidField("email") && ModelState.IsValidField("password"))
            {
                providerRecord.username = provider.username;
                providerRecord.firstName = provider.firstName;
                providerRecord.lastName = provider.lastName;
                providerRecord.email = provider.email;
                providerRecord.password = Utils.encrypt(provider.password);
                providerRecord.userTypeId = 1;
                providerRecord.enabled = true;
                providerRecord.locked = false;
                providerRecord.lastLogin = DateTime.Now;
            }
            else
            {
                return View("Provider", provider);
            }

            // Existent institution
            if (provider.isOldInstitution == "2")
            {
                providerRecord.institutionId = Convert.ToInt32(provider.selectedInstitution);
                providerRecord.cityId = null;
            }
            else if (provider.isOldInstitution == "1")
            {
                providerRecord.institutionId = null;

                if (provider.city != null)
                    providerRecord.cityId = Convert.ToInt32(provider.city);
                else providerRecord.cityId = null;
            }

            db.Users.Add(providerRecord);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Occurred an error while saving your registration! Please try again or contact the adminsitrator.";
                Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                return View("Provider");
            }

            ViewBag.Success = "NEW PROVIDER RECORD SAVED!!!";
            return View("RegisterResponse");

        }

        /// <summary>
        /// Saves the edition of the provider´s data registration
        /// </summary>
        /// <param name="provider">Model object of the provider´s data registration</param>
        /// <returns>HTml apge with the process´ result</returns>
        public ActionResult UpdateProvider(Provider provider)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 1)
            {
                if (ModelState.IsValidField("firstName") &&
                    ModelState.IsValidField("lastName") &&
                    ModelState.IsValidField("password") &&
                    ModelState.IsValidField("passwordConfirmation"))
                {
                    int userId = (int)Session["userID"];
                    PraxisDBEntities db = new PraxisDBEntities();

                    IEnumerable<User> users = from usrs in db.Users
                                              where usrs.id == userId
                                              select usrs;

                    User user;

                    try
                    {

                        user = users.FirstOrDefault();
                    }
                    catch (NullReferenceException ex)
                    {
                        ViewBag.Error = "Occurred an error while accessing user´s profile...Please try again or contact the administrator";
                        Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                        return View("RegisterResponse");
                    }

                    user.firstName = provider.firstName;
                    user.lastName = provider.lastName;
                    user.password = Utils.encrypt(provider.password);

                    if (provider.isOldInstitution.Equals("1"))
                    {
                        user.cityId = Convert.ToInt32(provider.city);
                        user.institutionId = null;
                    }
                    else if (provider.isOldInstitution.Equals("2"))
                    {
                        user.cityId = null;
                        user.institutionId = Convert.ToInt32(provider.selectedInstitution);
                    }

                    db.Entry(user).State = System.Data.Entity.EntityState.Modified;

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = "Occurred an error while updating your profile...Please try again or contact the administrator.";
                        Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                        return View("RegisterResponse");
                    }

                    Session["userName"] = user.firstName + " " + user.lastName;

                    ViewBag.Success = "YOUR PROFILE HAS BEEN UPDATED WITH SUCCESS!!!";
                    return View("RegisterResponse");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            return null;
        }

        /// <summary>
        /// Shows provider´s information
        /// </summary>
        /// <param name="id">Provider´s identification number</param>
        /// <returns>html page</returns>
        public ActionResult ShowProvider(int id)
        {
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<User> users = from usrs in db.Users
                                      where usrs.id == id && usrs.userTypeId == 1
                                      select usrs;

            User user = null;

            try
            {
                user = users.FirstOrDefault();
            }
            catch (NullReferenceException ex)
            {
                ViewBag.Error = "Occurred an error while accessing user´s profile...";
                Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message: " + ex.InnerException.Message + "\n\nNull record]");
                return View("RegisterResponse");
            }

            var citiesAndCountries = from cts in db.Cities
                                     join ctrs in db.Countries on cts.countryId equals ctrs.id
                                     where cts.id == user.cityId
                                     select new
                                     {
                                         city = cts.name,
                                         country = ctrs.name
                                     };

            ModelViews.Register.Provider provider = new ModelViews.Register.Provider();

            provider.username = user.username;
            provider.firstName = user.firstName;
            provider.lastName = user.lastName;
            provider.email = user.email;
            provider.city = (citiesAndCountries.FirstOrDefault() != null ? citiesAndCountries.FirstOrDefault().city : "No city");
            provider.country = (citiesAndCountries.FirstOrDefault() != null ? citiesAndCountries.FirstOrDefault().country : "No country");

            ViewBag.Title = "Provider information";

            return View(provider);
        }

        /// <summary>
        /// Deletes a user´s profile
        /// </summary>
        /// <returns>Html page with process´ result</returns>
        public ActionResult DeleteProfile()
        {
            if (Session["userId"] != null)
            {
                int userId = (int)Session["userId"];
                int userType = (int)Session["userType"];

                String[] result = deleteAllProfileData(userId, userType);

                if (!result[0].Equals("ack"))
                {
                    ViewBag.Error = result[0];
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + result[1] + "]");
                    return View("RegisterResponse");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

            Session.Clear();
            Session.Abandon();
            ViewBag.Success = "YOUR PROFILE WAS SUCCESSFUL DELETED!!! Click on 'REGISTER' link to register your new account...";
            return View("RegisterResponse");
        }

        /// <summary>
        /// Deletes the user´s profile photo
        /// </summary>
        /// <returns>Redirects to the 'StudentEdition' action method</returns>
        public ActionResult DeleteProfilePicture()
        {
            if (Session["userId"] != null)
            {
                PraxisDBEntities db = new PraxisDBEntities();
                int userId = (int)Session["userId"];

                IEnumerable<User> users = from usrs in db.Users
                                          where usrs.id == userId
                                          select usrs;

                User user;

                try
                {

                    user = users.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    ViewBag.Error = "Occurred an error while accessing your user´s profile...Please try again or contact the administrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterResponse");
                }

                String destinyPath = Server.MapPath(user.picture);

                try
                {
                    if (System.IO.File.Exists(destinyPath))
                        System.IO.File.Delete(destinyPath);
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error while deleting your photo! Please try again or contact the adminsitrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterResponse");
                }

                user.picture = null;

                // Specify as an update
                db.Entry(user).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Occurred an error while updating your profile´s photo! Please try again or contact the adminsitrator.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("RegisterResponse");
                }

                return RedirectToAction("StudentEdition", "Register");
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Deletes a profile  - Provider or Student - by a administrator
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <param name="userType">User´s type number</param>
        /// <returns>PartialView with users updated</returns>
        public ActionResult DeleteProfileByAdmin(int userId = -1, int userType = -1)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 3)
            {
                String[] result = deleteAllProfileData(userId, userType);

                if (!result[0].Equals("ack"))
                {
                    ViewBag.Error = result[0];
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + result[1] + "]");
                    return View("RegisterResponse");
                }
                else
                {
                    if (userType == 1)
                    {

                        Models.AllProviders allProviders = new Models.AllProviders();
                        allProviders.page = 1;
                        allProviders.pageSize = 5;

                        return PartialView("AllProvidersPartialView", allProviders);
                    }
                    else if (userType == 2)
                    {
                        Models.AllStudents allStudents = new Models.AllStudents();
                        allStudents.page = 1;
                        allStudents.pageSize = 5;

                        return PartialView("AllStudentsPartialView", allStudents);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return RedirectToAction("AdminLogin", "Login");
            }
        }

        /// <summary>
        /// Shows all providers registered
        /// </summary>
        /// <param name="page">Pagination´s page number</param>
        /// <param name="pageSize">Pagination´s number of providers per page</param>
        /// <returns>Html page with all providers registered</returns>
        public ActionResult AllProviders(int page = 1, int pageSize = 5)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 3)
            {
                Models.AllProviders allProviders = new Models.AllProviders();
                allProviders.page = page;
                allProviders.pageSize = pageSize;

                ViewBag.Title = "Providers registered";

                return View(allProviders);
            }
            else
            {
                return RedirectToAction("AdminLogin", "Login");
            }
        }

        /// <summary>
        /// Shows all students registered
        /// </summary>
        /// <param name="page">Pagination´s page number</param>
        /// <param name="pageSize">Pagination´s number of providers per page</param>
        /// <returns>Html page with all students registered</returns>
        public ActionResult AllStudents(int page = 1, int pageSize = 5)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 3)
            {
                Models.AllStudents allStudents = new Models.AllStudents();
                allStudents.page = page;
                allStudents.pageSize = pageSize;

                ViewBag.Title = "Students registered";

                return View(allStudents);
            }
            else
            {
                return RedirectToAction("AdminLogin", "Login");
            }
        }

        /// <summary>
        /// Deletes a profile - Provider or Student
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <param name="userType">User´s type number</param>
        /// <returns>String array: ["ack"]["ack"] if success; [ERROR_MESSAGE][EXCEPTION_MESSAGE] if error</returns>
        private String[] deleteAllProfileData(int userId, int userType)
        {
            String[] message = new String[2];
            PraxisDBEntities db = new PraxisDBEntities();

            // PROVIDER PROFILE
            if (userType == 1)
            {
                // Get all Provider´s proposals
                IEnumerable<Proposal> allProposals = from props in db.Proposals
                                                     where props.providerId == userId
                                                     select props;

                IEnumerable<ProposalLanguage> languages = null;
                IEnumerable<ProposalStudyArea> areas = null;
                IEnumerable<ProposalStudyDegree> degrees = null;
                IEnumerable<ProposalTarget> targets = null;
                IEnumerable<PromotedSkill> promoSkills = null;
                IEnumerable<RequiredSkill> requiredSkills = null;

                if (allProposals.ToList() != null && allProposals.ToList().Count() > 0)
                {
                    foreach (Proposal proposal in allProposals)
                    {

                        // Get all languages
                        languages = from langs in db.ProposalLanguages
                                    where langs.proposalId == proposal.id
                                    select langs;
                        // Get all study´s areas
                        areas = from ars in db.ProposalStudyAreas
                                where ars.proposalId == proposal.id
                                select ars;

                        // Get all study´s degrees
                        degrees = from dgrs in db.ProposalStudyDegrees
                                  where dgrs.proposalId == proposal.id
                                  select dgrs;

                        // Get all targets
                        targets = from targs in db.ProposalsTargets
                                  where targs.proposalId == proposal.id
                                  select targs;

                        // Get all promoted skills
                        promoSkills = from proSkills in db.PromotedSkills
                                      where proSkills.proposalId == proposal.id
                                      select proSkills;

                        // Get all required skills
                        requiredSkills = from reqSkills in db.RequiredSkills
                                         where reqSkills.proposalId == proposal.id
                                         select reqSkills;
                    }

                    // Delete languages
                    if (languages.ToList() != null && languages.ToList().Count() > 0)
                        foreach (ProposalLanguage lang in languages)
                            db.ProposalLanguages.Remove(lang);

                    // Delete study areas
                    if (areas.ToList() != null && areas.ToList().Count() > 0)
                        foreach (ProposalStudyArea area in areas)
                            db.ProposalStudyAreas.Remove(area);

                    // Delete study degrees
                    if (degrees.ToList() != null && degrees.ToList().Count() > 0)
                        foreach (ProposalStudyDegree degree in degrees)
                            db.ProposalStudyDegrees.Remove(degree);

                    // Delete targets
                    if (targets.ToList() != null && targets.ToList().Count() > 0)
                        foreach (ProposalTarget targ in targets)
                            db.ProposalsTargets.Remove(targ);

                    // Delete promoted skills
                    if (promoSkills.ToList() != null && promoSkills.ToList().Count() > 0)
                        foreach (PromotedSkill skill in promoSkills)
                            db.PromotedSkills.Remove(skill);

                    // Delete required skills
                    if (requiredSkills.ToList() != null && requiredSkills.ToList().Count() > 0)
                        foreach (RequiredSkill skill in requiredSkills)
                            db.RequiredSkills.Remove(skill);
                }

                // Delete user´s profile
                IEnumerable<User> users = from usrs in db.Users
                                          where usrs.id == userId
                                          select usrs;

                // Delete user´s profile
                User user = null;

                try
                {
                    user = users.FirstOrDefault();
                    db.Users.Remove(user);
                }
                catch (NullReferenceException ex)
                {
                    message[0] = "Occurred an error while accessing your profile....Please try again or contact the administrator.";
                    message[1] = ex.InnerException.Message;

                    return message;
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    message[0] = "Occurred an error while deleting your profile....Please try again or contact the administrator.";
                    message[1] = ex.InnerException.Message;

                    return message;
                }
            }

            // STUDENT PROFILE
            if (userType == 2)
            {
                // Delete any student´s skills from database
                IEnumerable<ApplicantSkill> allAppSkills = from appSkills in db.ApplicantSkills
                                                           where appSkills.userId == userId
                                                           select appSkills;

                if (allAppSkills.ToList() != null && allAppSkills.ToList().Count() > 0)
                    foreach (ApplicantSkill skill in allAppSkills)
                        db.ApplicantSkills.Remove(skill);

                // Delete any documents associated with any student´s applications from the server and database
                IEnumerable<ApplicationDocs> allAppDocs = from docs in db.ApplicationDocs
                                                          where docs.userId == userId
                                                          select docs;

                String parentDirectory = null;

                if (allAppSkills.ToList() != null && allAppSkills.ToList().Count() > 0)
                {
                    foreach (ApplicationDocs doc in allAppDocs)
                    {

                        try
                        {
                            String fullPath = Server.MapPath(doc.path);

                            if (System.IO.File.Exists(fullPath))
                            {
                                parentDirectory = System.IO.Directory.GetParent(fullPath).FullName;
                                System.IO.File.Delete(fullPath);
                            }
                        }
                        catch (Exception ex)
                        {
                            message[0] = "Occurred an error while deleting your application´s documents from server...Please try again or contact the administrator.";
                            message[1] = ex.InnerException.Message;

                            return message;
                        }

                        db.ApplicationDocs.Remove(doc);
                    }

                    try
                    {
                        if (System.IO.Directory.Exists(parentDirectory))
                            System.IO.Directory.Delete(parentDirectory);
                    }
                    catch (Exception ex)
                    {
                        message[0] = "Occurred an error while deleting your folder application´s documents from server...Please try again or contact the administrator.";
                        message[1] = ex.InnerException.Message;

                        return message;
                    }
                }

                // Delete all student´s applications from database
                IEnumerable<Application> allApplications = from applications in db.Applications
                                                           where applications.userId == userId
                                                           select applications;

                if (allApplications.ToList() != null && allApplications.ToList().Count() > 0)
                    foreach (Application app in allApplications)
                        db.Applications.Remove(app);

                // Delete actual student´s profile
                IEnumerable<User> allUsers = from users in db.Users
                                             where users.id == userId
                                             select users;

                User user;

                try
                {
                    user = allUsers.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {
                    message[0] = "Occurred an error while accessing your profile record...Please try again or contact the administrator.";
                    message[1] = ex.InnerException.Message;

                    return message;
                }

                db.Users.Remove(user);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    message[0] = "Occurred an error while deleting your profile record...Please try again or contact the administrator.";
                    message[1] = ex.InnerException.Message;

                    return message;
                }
            }

            message[0] = "ack";
            message[1] = "ack";

            return message;
        }

        /* JQUERY REQUESTS */

        /// <summary>
        /// Retrieves all cities that belong to country
        /// </summary>
        /// <param name="countryId">Country´s identification number</param>
        /// <returns>Dictionary: "Key: City´s identification number, Value: City´s name"</returns>
        public JsonResult CitiesOfCountry(int countryId = -1)
        {

            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<City> cities = from ct in db.Cities
                                       where ct.countryId == countryId
                                       orderby ct.name
                                       select ct;

            Dictionary<String, String> citiesCtr = new Dictionary<String, String>();

            if (cities.ToList() != null && cities.ToList().Count() > 0)
                foreach (City city in cities)
                {
                    citiesCtr.Add(Convert.ToString(city.id), city.name);
                }

            if (citiesCtr.Count() == 0)
                citiesCtr.Add("null city", "No cities");

            return Json(new { cities = citiesCtr }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Tests if user exists using the email address and the type of user - Student or Provider
        /// </summary>
        /// <param name="email">email address of the user</param>
        /// <param name="userType">Type of user</param>
        /// <returns>"exist" if user exists; "nexist" if user does not exist</returns>
        public JsonResult existsUser(String email, int userType)
        {

            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<User> usersTypes = from users in db.Users
                                           where users.userTypeId == userType
                                           select users;

            if (usersTypes.Count() > 0)
            {
                return Json(new { result = "exist" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = "nexist" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Retrives existent insititutions form database
        /// </summary>
        /// <returns>Partial view with existent institutions</returns>
        public PartialViewResult ExistentInstitutions()
        {
            ModelViews.Register.Provider provider = new ModelViews.Register.Provider();

            return PartialView("ProviderOldInstitutionPartialView", provider);
        }

        /// <summary>
        /// Retrives partial view to fill with localization information about a new proposal
        /// </summary>
        /// <returns>Partial View</returns>
        public PartialViewResult NewInstitution()
        {

            ModelViews.Register.Provider provider = new ModelViews.Register.Provider();

            return PartialView("ProviderNewInstitutionPartialView", provider);
        }

        /// <summary>
        /// Retrives existent insititutions form database in an edition modality
        /// </summary>
        /// <returns>Partial view</returns>
        public PartialViewResult ExistentEditionInstitutions()
        {
            int userId = (int)Session["userId"];
            PraxisDBEntities db = new PraxisDBEntities();
            ModelViews.Register.Provider provider = new ModelViews.Register.Provider();

            IEnumerable<User> users = from usrs in db.Users
                                      where usrs.id == userId
                                      select usrs;

            User user = users.FirstOrDefault();

            provider.selectedInstitution = Convert.ToString(user.institutionId);

            return PartialView("ProviderEditionOldInstitutionPartialView", provider);
        }

        /// <summary>
        /// Retrives partial view to fill with localization information about a new proposal in an edition modality
        /// </summary>
        /// <returns>Partial View</returns>
        public PartialViewResult NewEditionInstitution()
        {

            int userId = (int)Session["userID"];
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<User> users = from usrs in db.Users
                                      where usrs.id == userId
                                      select usrs;

            User user = null;

            try
            {

                user = users.FirstOrDefault();
            }
            catch (NullReferenceException ex)
            {

                // Continue
            }

            ModelViews.Register.Provider provider = new ModelViews.Register.Provider();

            provider.username = user.username;
            provider.firstName = user.firstName;
            provider.lastName = user.lastName;

            if (user.institutionId != null)
            {
                provider.isOldInstitution = "2";
                provider.selectedInstitution = Convert.ToString(user.institutionId);
                provider.city = null;
            }
            else
            {
                provider.isOldInstitution = "1";
                provider.selectedInstitution = null;

                if (user.cityId == null)
                {
                    provider.city = null;
                    provider.country = null;
                }
                else
                {
                    provider.city = Convert.ToString(user.cityId);

                    IEnumerable<Country> countries = from ctrs in db.Countries
                                                     join cts in db.Cities on ctrs.id equals cts.countryId
                                                     where cts.id == user.cityId
                                                     select ctrs;

                    provider.country = Convert.ToString(countries.FirstOrDefault().id);
                }
            }

            return PartialView("ProviderEditionNewInstitutionPartialView", provider);
        }

        /// <summary>
        /// Change the user´s locked status by tha administrator
        /// </summary>
        /// <param name="info">Value of the slected satus - String format: "USER_ID,NEW_STATUS"</param>
        /// <returns>Refresh the providers list through returning a PartialView</returns>
        public ActionResult ChangeLockedStatus(String info)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 3)
            {
                int userId = Convert.ToInt32(info.Split(',')[0]);
                String newStatus = info.Split(',')[1];
                String userType = info.Split(',')[2];

                PraxisDBEntities db = new PraxisDBEntities();

                IEnumerable<User> users = from usrs in db.Users
                                          where usrs.id == userId
                                          select usrs;

                User provider = null;

                try
                {
                    provider = users.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {

                    ViewBag.Error = "Occurred an error accessing user´s information record.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("AllProviders");
                }

                switch (newStatus)
                {

                    case "Yes":

                        provider.locked = true;
                        break;

                    case "No":

                        provider.locked = false;
                        break;
                }

                db.Entry(provider).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                    ViewBag.Error = "Occurred an error while saving new provider´s locked status.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("AllProviders");
                }



                if (userType.Equals("Provider"))
                {

                    Models.AllProviders allProviders = new Models.AllProviders();
                    allProviders.page = 1;
                    allProviders.pageSize = 5;

                    return PartialView("AllProvidersPartialView", allProviders);
                }
                else if (userType.Equals("Student"))
                {
                    Models.AllStudents allStudents = new Models.AllStudents();
                    allStudents.page = 1;
                    allStudents.pageSize = 5;

                    return PartialView("AllStudentsPartialView", allStudents);
                }
                else
                {
                    return null;
                }

            }
            else
            {
                return RedirectToAction("AdminLogin", "Login");
            }
        }

        /// <summary>
        /// Change the user´s enbled status by tha administrator
        /// </summary>
        /// <param name="info">Value of the slected satus - String format: "USER_ID,NEW_STATUS"</param>
        /// <returns>Refresh the providers list through returning a PartialView</returns>
        public ActionResult ChangeEnabledStatus(String info)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 3)
            {
                int userId = Convert.ToInt32(info.Split(',')[0]);
                String newStatus = info.Split(',')[1];
                String userType = info.Split(',')[2];

                PraxisDBEntities db = new PraxisDBEntities();

                IEnumerable<User> users = from usrs in db.Users
                                          where usrs.id == userId
                                          select usrs;

                User provider = null;

                try
                {
                    provider = users.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {

                    ViewBag.Error = "Occurred an error accessing user´s information record.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("AllProviders");
                }

                switch (newStatus)
                {

                    case "Yes":

                        provider.enabled = true;
                        break;

                    case "No":

                        provider.enabled = false;
                        break;
                }

                db.Entry(provider).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                    ViewBag.Error = "Occurred an error while saving new provider´s enabled status.";
                    Utils.writeLog("[Controller: " + RouteData.Values["controller"].ToString() + "]\n[Action method: " + RouteData.Values["action"].ToString() + "]\n[Error: " + ViewBag.Error + "]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
                    return View("AllProviders");
                }

                if (userType.Equals("Provider"))
                {

                    Models.AllProviders allProviders = new Models.AllProviders();
                    allProviders.page = 1;
                    allProviders.pageSize = 5;

                    return PartialView("AllProvidersPartialView", allProviders);
                }
                else if (userType.Equals("Student"))
                {
                    Models.AllStudents allStudents = new Models.AllStudents();
                    allStudents.page = 1;
                    allStudents.pageSize = 5;

                    return PartialView("AllStudentsPartialView", allStudents);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return RedirectToAction("AdminLogin", "Login");
            }
        }

    }
}
