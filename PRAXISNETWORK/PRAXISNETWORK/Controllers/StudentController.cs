﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Controllers
{
    /// <summary>
    /// Handles all requests of a student when authenticated
    /// </summary>
    public class StudentController : Controller
    {
        /// <summary>
        /// Shows html page with the student´s management board
        /// </summary>
        /// <returns>Html page student´s management board</returns>
        public ActionResult Index()
        {
            if (Session["userId"] != null && (int)Session["userType"] == 2)
            {

                ViewBag.Title = "Student board";

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        /// <summary>
        /// Shows all the proposal´s offer applications made by the student
        /// </summary>
        /// <param name="page">Pagination´s page number</param>
        /// <param name="pageSize">pagination´s number of proposal´s offer applications per page</param>
        /// <returns>Html page with all proposal´s offer applications</returns>
        public ActionResult StudentApplications(int page = 1, int pageSize = 5)
        {
            if (Session["userId"] != null && (int)Session["userType"] == 2)
            {

                int userId = (int)Session["userID"];
                

                ViewBag.Title = "Your applications";
                Models.StudentApplications studentApps = new Models.StudentApplications()
                {

                    userId = userId,
                    page = page,
                    pageSize = pageSize
                };

                return View(studentApps);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

    }
}
