﻿using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PRAXISNETWORK
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            // WCF services initialization

            //ServiceHost host = new ServiceHost(typeof(PraxisWebService));
            //host.AddServiceEndpoint(typeof(IPraxisWebService), new BasicHttpBinding(), "Soap");

            //try
            //{
            //    host.Open();

            //}catch(CommunicationException ex){
                
            //    Utils.writeLog(Server.MapPath("~/Files/Application/Logs.txt"), "[File: Global.asax.cs]\n[Exception message:\n\n" + ex.InnerException.Message + "]");
            //}
        }
    }
}