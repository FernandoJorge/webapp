namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TABLEUSERSINSTITUTION_IDnewattribute : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.USERS", "INSTITUTION_ID", c => c.Int());
            AlterColumn("dbo.INSTITUTIONS", "PHONE", c => c.String(maxLength: 255));
            CreateIndex("dbo.USERS", "INSTITUTION_ID");
            AddForeignKey("dbo.USERS", "INSTITUTION_ID", "dbo.INSTITUTIONS", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.USERS", "INSTITUTION_ID", "dbo.INSTITUTIONS");
            DropIndex("dbo.USERS", new[] { "INSTITUTION_ID" });
            AlterColumn("dbo.INSTITUTIONS", "PHONE", c => c.Int(nullable: false));
            DropColumn("dbo.USERS", "INSTITUTION_ID");
        }
    }
}
