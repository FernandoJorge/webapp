namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TABLEINSTITUTIONSCITY_IDpermissiontobenull : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.INSTITUTIONS", new[] { "CITY_ID" });
            AlterColumn("dbo.INSTITUTIONS", "CITY_ID", c => c.Int());
            CreateIndex("dbo.INSTITUTIONS", "CITY_ID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.INSTITUTIONS", new[] { "CITY_ID" });
            AlterColumn("dbo.INSTITUTIONS", "CITY_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.INSTITUTIONS", "CITY_ID");
        }
    }
}
