namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TABLESEMAIL_SERVICEandEMAIL_SSLS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EMAIL_SERVICE",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SMTP_SERVER = c.String(maxLength: 255),
                        SMTP_PORT = c.Int(nullable: false),
                        USERNAME = c.String(maxLength: 255),
                        PASSWORD = c.String(maxLength: 255),
                        emailSSLId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EMAIL_SSLS", t => t.emailSSLId)
                .Index(t => t.emailSSLId);
            
            CreateTable(
                "dbo.EMAIL_SSLS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DESCRIPTION = c.String(maxLength: 255),
                        SIGLE = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EMAIL_SERVICE", "emailSSLId", "dbo.EMAIL_SSLS");
            DropIndex("dbo.EMAIL_SERVICE", new[] { "emailSSLId" });
            DropTable("dbo.EMAIL_SSLS");
            DropTable("dbo.EMAIL_SERVICE");
        }
    }
}
