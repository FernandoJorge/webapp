namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TABLEEMAIL_SERVICESnewattributeFROM_EMAIL : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EMAIL_SERVICE", "FROM_EMAIL", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EMAIL_SERVICE", "FROM_EMAIL");
        }
    }
}
