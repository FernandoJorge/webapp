namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TABLEEMAIL_SERVICESEMAIL_SSL_IDnamechange : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.EMAIL_SERVICE", name: "emailSSLId", newName: "EMAIL_SSL_ID");
            RenameIndex(table: "dbo.EMAIL_SERVICE", name: "IX_emailSSLId", newName: "IX_EMAIL_SSL_ID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.EMAIL_SERVICE", name: "IX_EMAIL_SSL_ID", newName: "IX_emailSSLId");
            RenameColumn(table: "dbo.EMAIL_SERVICE", name: "EMAIL_SSL_ID", newName: "emailSSLId");
        }
    }
}
