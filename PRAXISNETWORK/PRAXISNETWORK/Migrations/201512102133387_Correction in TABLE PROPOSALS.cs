namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrectioninTABLEPROPOSALS : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PROPOSALS", "BENEFITS", c => c.String(maxLength: 2048));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PROPOSALS", "BENEFITS", c => c.String());
        }
    }
}
