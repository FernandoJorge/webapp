namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewcolumnCOUNTRY_IDinTABLEPROPOSALS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PROPOSALS", "COUNTRY_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.PROPOSALS", "COUNTRY_ID");
            AddForeignKey("dbo.PROPOSALS", "COUNTRY_ID", "dbo.COUNTRIES", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PROPOSALS", "COUNTRY_ID", "dbo.COUNTRIES");
            DropIndex("dbo.PROPOSALS", new[] { "COUNTRY_ID" });
            DropColumn("dbo.PROPOSALS", "COUNTRY_ID");
        }
    }
}
