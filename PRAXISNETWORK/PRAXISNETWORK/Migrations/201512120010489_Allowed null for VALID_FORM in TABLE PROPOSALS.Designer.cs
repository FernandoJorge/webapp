// <auto-generated />
namespace PRAXISNETWORK.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AllowednullforVALID_FORMinTABLEPROPOSALS : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AllowednullforVALID_FORMinTABLEPROPOSALS));
        
        string IMigrationMetadata.Id
        {
            get { return "201512120010489_Allowed null for VALID_FORM in TABLE PROPOSALS"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
