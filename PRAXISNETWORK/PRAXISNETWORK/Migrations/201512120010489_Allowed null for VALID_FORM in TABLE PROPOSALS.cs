namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AllowednullforVALID_FORMinTABLEPROPOSALS : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PROPOSALS", "VALID_FORM", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PROPOSALS", "VALID_FORM", c => c.DateTime(nullable: false));
        }
    }
}
