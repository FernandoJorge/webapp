namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class APPLICATION_DOCStable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.APPLICATION_DOCS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PATH = c.String(maxLength: 2048),
                        DESCRIPTION = c.String(maxLength: 255),
                        PROPOSAL_ID = c.Int(nullable: false),
                        USER_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.APPLICATIONS", t => new { t.PROPOSAL_ID, t.USER_ID })
                .Index(t => new { t.PROPOSAL_ID, t.USER_ID });
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.APPLICATION_DOCS", new[] { "PROPOSAL_ID", "USER_ID" }, "dbo.APPLICATIONS");
            DropIndex("dbo.APPLICATION_DOCS", new[] { "PROPOSAL_ID", "USER_ID" });
            DropTable("dbo.APPLICATION_DOCS");
        }
    }
}
