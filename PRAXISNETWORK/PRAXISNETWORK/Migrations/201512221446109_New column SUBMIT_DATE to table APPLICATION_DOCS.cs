namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewcolumnSUBMIT_DATEtotableAPPLICATION_DOCS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.APPLICATION_DOCS", "SUBMIT_DATE", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.APPLICATION_DOCS", "SUBMIT_DATE");
        }
    }
}
