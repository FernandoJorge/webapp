namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Stringlenghtchangeto6inGENDERattributeoftableUSER : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.USERS", "GENDER", c => c.String(maxLength: 6));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.USERS", "GENDER", c => c.String(maxLength: 1));
        }
    }
}
