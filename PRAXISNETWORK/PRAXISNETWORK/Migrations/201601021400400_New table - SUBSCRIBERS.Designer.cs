// <auto-generated />
namespace PRAXISNETWORK.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class NewtableSUBSCRIBERS : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(NewtableSUBSCRIBERS));
        
        string IMigrationMetadata.Id
        {
            get { return "201601021400400_New table - SUBSCRIBERS"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
