namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewtableSUBSCRIBERS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SUBSCRIBERS",
                c => new
                    {
                        EMAIL = c.String(nullable: false, maxLength: 255),
                        NAME = c.String(maxLength: 255),
                        SUBSCRIPITON_DATE = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EMAIL);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SUBSCRIBERS");
        }
    }
}
