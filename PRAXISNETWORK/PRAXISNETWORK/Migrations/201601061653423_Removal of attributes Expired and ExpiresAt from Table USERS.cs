namespace PRAXISNETWORK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovalofattributesExpiredandExpiresAtfromTableUSERS : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.USERS", "EXPIRED");
            DropColumn("dbo.USERS", "EXPIRES_AT");
        }
        
        public override void Down()
        {
            AddColumn("dbo.USERS", "EXPIRES_AT", c => c.DateTime());
            AddColumn("dbo.USERS", "EXPIRED", c => c.Boolean());
        }
    }
}
