﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRAXISNETWORK.ModelViews.Application
{
    public class ApplicantShow
    {
        public int id { get; set; }

        public String name { get; set; }

        public String email { get; set; }

        public String birthDate { get; set; }

        public String gender { get; set; }

        public String picture { get; set; }

        public String about { get; set; }

        public String country { get; set; }

        public String city { get; set; }

    }
}