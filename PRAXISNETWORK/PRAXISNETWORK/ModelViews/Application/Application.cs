﻿using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PRAXISNETWORK.ModelViews.Application
{
    public class Application
    {
        public int proposalId { get; set; }

        public int userId { get; set; }

        [DisplayName("Add a message(optional):")]
        [StringLength(2048, ErrorMessage = "Max. of 2048 characteres.")]
        [DataType(DataType.MultilineText)]
        public String message { get; set; }

        [DisplayName("Upload useful documents(Ex: Curriculum Vitae, certificates...):")]
        [MultipleUploadFormat(ErrorMessage = "One or multiple files have prohibited file format. Only images or pdf or txt files.")]
        public IEnumerable<HttpPostedFileBase> docsUploaded { get; set; }

        public List<Models.ApplicationDocs> docs { get; set; }
    }
}