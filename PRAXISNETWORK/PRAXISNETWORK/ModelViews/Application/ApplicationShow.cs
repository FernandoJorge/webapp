﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRAXISNETWORK.ModelViews.Application
{
    public class ApplicationShow
    {
        public int proposalId { get; set; }

        public int userId { get; set; }

        public String username { get; set; }

        public String message { get; set; }

        public List<Models.ApplicationDocs> docs { get; set; }
    }
}