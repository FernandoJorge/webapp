﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PRAXISNETWORK.ModelViews.Login
{
    public class Login
    {
        [Required(ErrorMessage = "Required field")]
        [StringLength(255, ErrorMessage = "Max of 255 characteres")]
        [DisplayName("Email or Username:")]
        public String usernameEmail { get; set; }

        [Required(ErrorMessage = "Required field")]
        [StringLength(255, MinimumLength = 8, ErrorMessage = "Minimum of 8 characters and Max of 255 characteres")]
        [DataType(DataType.Password)]
        [DisplayName("Password:")]
        public String password { get; set; }

        [DisplayName("(Check if your are a student)")]
        public Boolean isApplicant { get; set; }
    }
}