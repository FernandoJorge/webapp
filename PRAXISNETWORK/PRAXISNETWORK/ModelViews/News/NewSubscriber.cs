﻿using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.ModelViews.News
{
    public class NewSubscriber
    {
        [Required(ErrorMessage = "Required field.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid email address.")]
        [SubscriberExists(ErrorMessage = "Email address already exists.")]
        [DisplayName("Your email address:")]
        public String email { get; set; }

        [DisplayName("Your name(optional):")]
        public String name { get; set; }
    }
}