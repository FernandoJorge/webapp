﻿using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.ModelViews.Proposals
{
    public class Proposal
    {
        public int id { get; set; }

        [Required(ErrorMessage = "Required field")]
        [DisplayName("Target:")]
        public String[] targets { get; set; }

        [Required(ErrorMessage = "Required field")]
        [StringLength(255, ErrorMessage = "Max of 255 characteres")]
        [DisplayName("Title:")]
        public String title { get; set; }

        [Required(ErrorMessage = "Required field")]
        [StringLength(2048, ErrorMessage = "Max of 2048 characteres")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Description:")]
        public String description { get; set; }

        [Required(ErrorMessage = "Required field")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [ValidStartDate(ErrorMessage = "The start date must be grater than the actual date")]
        [DisplayName("Start date:")]
        public DateTime startDate { get; set; }

        [Required(ErrorMessage = "Required field")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [ValidEndDate("startDate", ErrorMessage = "The end date must be grater than the start date")]
        [DisplayName("End date:")]
        public DateTime endDate { get; set; }

        [Required(ErrorMessage = "Required field")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [ValidUntilDate("startDate", ErrorMessage = "Invalid validation date: ")]
        [DisplayName("Proposal is valid until:")]
        public DateTime validTo { get; set; }

        [StringLength(255, ErrorMessage = "Max of 255 characteres")]
        [DisplayName("Payment per month(optional):")]
        public String payment { get; set; }

        [StringLength(255, ErrorMessage = "Max of 255 characteres")]
        [DisplayName("Vacancies(optional):")]
        public String vacancies { get; set; }

        [StringLength(2048, ErrorMessage = "Max of 2048 characteres")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Global skills(optional):")]
        public String globalSkills { get; set; }

        [StringLength(2048, ErrorMessage = "Max of 2048 characteres")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Job related skills(optional):")]
        public String jobRelatedSkills { get; set; }

        [StringLength(2048, ErrorMessage = "Max of 2048 characteres")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Benefits(optional):")]
        public String benefits { get; set; }

        [Required(ErrorMessage = "Required field")]
        [StringLength(2048, ErrorMessage = "Max of 2048 characteres")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Contact:")]
        public String contact { get; set; }

        [Required(ErrorMessage = "Required field")]
        [StringLength(255, ErrorMessage = "Max of 255 characteres")]
        [DisplayName("First name:")]
        public String superVisorFirstName { get; set; }

        [Required(ErrorMessage = "Required field")]
        [StringLength(255, ErrorMessage = "MAx of 255 characteres")]
        [DisplayName("Last name:")]
        public String superVisorLastName { get; set; }

        [Required(ErrorMessage = "required field")]
        [StringLength(255, ErrorMessage = "MAx of 255 characteres")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid emaill address")]
        [DisplayName("Email address:")]
        public String superVisorEmail { get; set; }

        [Required(ErrorMessage = "required field")]
        [DisplayName("Required skills:")]
        public String[] requiredSkills { get; set; }

        [Required(ErrorMessage = "required field")]
        [DisplayName("Skills to promote:")]
        public String[] promotedSkills { get; set; }

        [Required(ErrorMessage = "required field")]
        [DisplayName("Study domain:")]
        public String[] studyAreas { get; set; }

        [Required(ErrorMessage = "required field")]
        [DisplayName("Student´s Required degrees:")]
        public String[] studyDegrees { get; set; }

        [Required(ErrorMessage = "required field")]
        [DisplayName("Student required languages:")]
        public String[] languages { get; set; }

        [DisplayName("City:")]
        public String city { get; set; }

        [DisplayName("Promoting institution:")]
        public String institution { get; set; }

        [DisplayName("Host´s institution:")]
        public String host { get; set; }

        public List<SelectListItem> AllTargets()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfTargets = new List<SelectListItem>();

            IEnumerable<Target> targets = from targs in db.Targets
                                          orderby targs.name
                                          select targs;

            int firstId;

            try
            {
                firstId = targets.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfTargets.Add(new SelectListItem { Value = "null target", Text = "No targets" });

                return ListOfTargets;
            }

            foreach (Target target in targets)
            {
                ListOfTargets.Add(new SelectListItem { Value = Convert.ToString(target.id), Text = target.name, Selected = (target.id == firstId ? true : false) });
            }

            return ListOfTargets;
        }

        public List<SelectListItem> AllTargetsSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfTargets = new List<SelectListItem>();
            List<string> allTargets = this.targets.ToList();

            IEnumerable<Target> targets = from targs in db.Targets
                                          orderby targs.name
                                          select targs;

            if (allTargets == null || allTargets.Count() == 0)
            {

                ListOfTargets.Add(new SelectListItem { Value = "null target", Text = "No targets" });

                return ListOfTargets;
            }

            foreach (Target target in targets)
            {

                ListOfTargets.Add(new SelectListItem { Value = Convert.ToString(target.id), Text = target.name, Selected = (allTargets.Contains(Convert.ToString(target.id)) ? true : false) });

            }

            return ListOfTargets;
        }

        public List<SelectListItem> AllSkills()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfSkills = new List<SelectListItem>();

            IEnumerable<Skill> skills = from allSkills in db.Skills
                                        orderby allSkills.name
                                        select allSkills;

            int firstId;

            try
            {
                firstId = skills.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfSkills.Add(new SelectListItem { Value = "null skill", Text = "No skills" });

                return ListOfSkills;
            }

            foreach (Skill skill in skills)
            {
                ListOfSkills.Add(new SelectListItem { Value = Convert.ToString(skill.id), Text = skill.name, Selected = (skill.id == firstId ? true : false) });
            }

            return ListOfSkills;
        }

        public List<SelectListItem> AllRequiredSkillsSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfSkills = new List<SelectListItem>();
            List<String> Lskills = this.requiredSkills.ToList();

            IEnumerable<Skill> skills = from allSkills in db.Skills
                                        orderby allSkills.name
                                        select allSkills;

            if (Lskills == null || Lskills.Count() == 0)
            {

                ListOfSkills.Add(new SelectListItem { Value = "null skill", Text = "No skills" });

                return ListOfSkills;
            }

            foreach (Skill skill in skills)
            {
                ListOfSkills.Add(new SelectListItem { Value = Convert.ToString(skill.id), Text = skill.name, Selected = (Lskills.Contains(Convert.ToString(skill.id)) ? true : false) });
            }

            return ListOfSkills;
        }

        public List<SelectListItem> AllPromotedSkillsSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfSkills = new List<SelectListItem>();
            List<String> Lskills = this.promotedSkills.ToList();

            IEnumerable<Skill> skills = from allSkills in db.Skills
                                        orderby allSkills.name
                                        select allSkills;

            if (Lskills == null || Lskills.Count() == 0)
            {

                ListOfSkills.Add(new SelectListItem { Value = "null skill", Text = "No skills" });

                return ListOfSkills;
            }

            foreach (Skill skill in skills)
            {
                ListOfSkills.Add(new SelectListItem { Value = Convert.ToString(skill.id), Text = skill.name, Selected = (Lskills.Contains(Convert.ToString(skill.id)) ? true : false) });
            }

            return ListOfSkills;
        }

        public List<SelectListItem> AllLanguages()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfLanguages = new List<SelectListItem>();

            IEnumerable<Language> languages = from langs in db.Languages
                                              orderby langs.name
                                              select langs;

            int firstId;

            try
            {
                firstId = languages.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfLanguages.Add(new SelectListItem { Value = "null language", Text = "No languages" });

                return ListOfLanguages;
            }

            foreach (Language language in languages)
            {
                ListOfLanguages.Add(new SelectListItem { Value = Convert.ToString(language.id), Text = language.name, Selected = (language.id == firstId ? true : false) });
            }

            return ListOfLanguages;
        }

        public List<SelectListItem> AllLanguagesSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfLanguages = new List<SelectListItem>();
            List<String> allLanguages = this.languages.ToList();

            IEnumerable<Language> languages = from langs in db.Languages
                                              orderby langs.name
                                              select langs;

            if (allLanguages == null || allLanguages.Count() == 0)
            {

                ListOfLanguages.Add(new SelectListItem { Value = "null language", Text = "No languages" });

                return ListOfLanguages;
            }

            foreach (Language language in languages)
            {
                ListOfLanguages.Add(new SelectListItem { Value = Convert.ToString(language.id), Text = language.name, Selected = (allLanguages.Contains(Convert.ToString(language.id)) ? true : false) });
            }

            return ListOfLanguages;
        }

        public List<SelectListItem> AllStudyAreas()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfStudyAreas = new List<SelectListItem>();

            IEnumerable<StudyArea> studyAreas = from areas in db.StudyAreas
                                                orderby areas.topic
                                                select areas;

            int firstId = 0;

            try
            {
                firstId = studyAreas.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfStudyAreas.Add(new SelectListItem { Value = "null area", Text = "No study domains" });

                return ListOfStudyAreas;
            }

            foreach (StudyArea area in studyAreas)
            {

                ListOfStudyAreas.Add(new SelectListItem { Value = Convert.ToString(area.id), Text = area.name, Selected = (area.id == firstId ? true : false) });
            }

            return ListOfStudyAreas;
        }

        public List<SelectListItem> AllStudyAreasSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfStudyAreas = new List<SelectListItem>();
            List<String> allAreas = this.studyAreas.ToList();

            IEnumerable<StudyArea> studyAreas = from areas in db.StudyAreas
                                                orderby areas.topic
                                                select areas;

            if (allAreas == null || allAreas.Count() == 0)
            {

                ListOfStudyAreas.Add(new SelectListItem { Value = "null area", Text = "No study domains" });

                return ListOfStudyAreas;
            }

            foreach (StudyArea area in studyAreas)
            {

                ListOfStudyAreas.Add(new SelectListItem { Value = Convert.ToString(area.id), Text = area.name, Selected = (allAreas.Contains(Convert.ToString(area.id)) ? true : false) });
            }

            return ListOfStudyAreas;
        }

        public List<SelectListItem> AllStudyDegrees()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfStudyDegrees = new List<SelectListItem>();

            IEnumerable<StudyDegree> studyDegrees = from degrees in db.StudyDegrees
                                                    orderby degrees.name
                                                    select degrees;

            int firstId = 0;

            try
            {
                firstId = studyDegrees.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfStudyDegrees.Add(new SelectListItem { Value = "null degree", Text = "No Study degrees" });

                return ListOfStudyDegrees;
            }

            foreach (StudyDegree degree in studyDegrees)
            {
                ListOfStudyDegrees.Add(new SelectListItem { Value = Convert.ToString(degree.id), Text = degree.name, Selected = (degree.id == firstId ? true : false) });
            }

            return ListOfStudyDegrees;
        }

        public List<SelectListItem> AllStudyDegreesSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfStudyDegrees = new List<SelectListItem>();
            List<String> allDegrees = this.studyDegrees.ToList();

            IEnumerable<StudyDegree> studyDegrees = from degrees in db.StudyDegrees
                                                    orderby degrees.name
                                                    select degrees;

            if (allDegrees == null || allDegrees.Count() == 0)
            {

                ListOfStudyDegrees.Add(new SelectListItem { Value = "null degree", Text = "No Study degrees" });

                return ListOfStudyDegrees;
            }

            foreach (StudyDegree degree in studyDegrees)
            {
                ListOfStudyDegrees.Add(new SelectListItem { Value = Convert.ToString(degree.id), Text = degree.name, Selected = (allDegrees.Contains(Convert.ToString(degree.id)) ? true : false) });
            }

            return ListOfStudyDegrees;
        }

        public List<SelectListItem> AllCities()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfCities = new List<SelectListItem>();

            IEnumerable<City> cities = from cts in db.Cities
                                       orderby cts.name
                                       select cts;

            int firstId = 0;

            try
            {
                firstId = cities.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfCities.Add(new SelectListItem { Value = "null city", Text = "No cities" });

                return ListOfCities;
            }

            foreach (City city in cities)
            {

                String valueIds = Convert.ToString(city.id) + "," + Convert.ToString(city.countryId);
                ListOfCities.Add(new SelectListItem { Value = valueIds, Text = city.name, Selected = (city.id == firstId ? true : false) });

            }

            return ListOfCities;
        }

        public List<SelectListItem> AllCitiesSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfCities = new List<SelectListItem>();

            int cityId = Convert.ToInt32(this.city);

            IEnumerable<City> cities = from cts in db.Cities
                                       orderby cts.name
                                       select cts;

            if (String.IsNullOrEmpty(this.city))
            {

                ListOfCities.Add(new SelectListItem { Value = "null city", Text = "No cities" });

                return ListOfCities;
            }

            foreach (City city in cities)
            {
                String valueIds = Convert.ToString(city.id) + "," + Convert.ToString(city.countryId);
                ListOfCities.Add(new SelectListItem { Value = valueIds, Text = city.name, Selected = (city.id == cityId ? true : false) });
            }

            return ListOfCities;
        }

        public List<SelectListItem> AllInstitutions()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfInstitutions = new List<SelectListItem>();

            IEnumerable<Institution> institutions = from inst in db.Institutions
                                                    orderby inst.name
                                                    select inst;

            int firstId = 0;

            try
            {
                firstId = institutions.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfInstitutions.Add(new SelectListItem { Value = "null institution", Text = "No institutions" });

                return ListOfInstitutions;
            }

            foreach (Institution institution in institutions)
            {

                ListOfInstitutions.Add(new SelectListItem { Value = Convert.ToString(institution.id), Text = institution.name, Selected = (institution.id == firstId ? true : false) });
            }

            return ListOfInstitutions;
        }

        public List<SelectListItem> AllInstitutionsSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfInstitutions = new List<SelectListItem>();

            IEnumerable<Institution> institutions = from inst in db.Institutions
                                                    orderby inst.name
                                                    select inst;

            if (String.IsNullOrEmpty(this.institution))
            {

                ListOfInstitutions.Add(new SelectListItem { Value = "null institution", Text = "No institutions" });

                return ListOfInstitutions;
            }

            foreach (Institution institution in institutions)
            {

                ListOfInstitutions.Add(new SelectListItem { Value = Convert.ToString(institution.id), Text = institution.name, Selected = (institution.id == Convert.ToUInt32(this.institution) ? true : false) });
            }

            return ListOfInstitutions;
        }

        public List<SelectListItem> AllHostSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfInstitutions = new List<SelectListItem>();

            IEnumerable<Institution> institutions = from inst in db.Institutions
                                                    orderby inst.name
                                                    select inst;

            if (String.IsNullOrEmpty(this.host))
            {

                ListOfInstitutions.Add(new SelectListItem { Value = "null institution", Text = "No institutions" });

                return ListOfInstitutions;
            }

            foreach (Institution institution in institutions)
            {

                ListOfInstitutions.Add(new SelectListItem { Value = Convert.ToString(institution.id), Text = institution.name, Selected = (institution.id == Convert.ToUInt32(this.host) ? true : false) });
            }

            return ListOfInstitutions;
        }

    }
}