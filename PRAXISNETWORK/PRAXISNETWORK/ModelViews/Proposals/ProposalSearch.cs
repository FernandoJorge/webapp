﻿using PagedList;
using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.ModelViews.Proposals
{
    public class ProposalSearch
    {
        [DisplayName("Looking for:")]
        public String[] targets { get; set; }

        [DisplayName("Study degree:")]
        public String[] studyDegrees { get; set; }

        [DisplayName("Duration:")]
        public String[] durations { get; set; }

        [DisplayName("Country:")]
        public String country { get; set; }

        [DisplayName("Study area:")]
        public String studyArea { get; set; }

        [DisplayName("Search for projects, internships, countries...")]
        public String searchField { get; set; }

        public int page { get; set;}
        public int pageSize { get; set; }

        public List<SelectListItem> AllTargets()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfTargets = new List<SelectListItem>();

            IEnumerable<Target> targets = from targs in db.Targets
                                          orderby targs.name
                                          select targs;

            int firstId;

            try
            {
                firstId = targets.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfTargets.Add(new SelectListItem { Value = "null target", Text = "No targets" });

                return ListOfTargets;
            }

            foreach (Target target in targets)
            {

                if (target.id == firstId)
                {

                    ListOfTargets.Add(new SelectListItem { Value = Convert.ToString(target.id), Text = target.name, Selected = true });
                }
                else
                {
                    ListOfTargets.Add(new SelectListItem { Value = Convert.ToString(target.id), Text = target.name });
                }
            }

            return ListOfTargets;
        }

        public List<SelectListItem> AllStudyDegrees()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfStudyDegrees = new List<SelectListItem>();

            IEnumerable<StudyDegree> studyDegrees = from degrees in db.StudyDegrees
                                                    orderby degrees.name
                                                    select degrees;

            int firstId = 0;

            try
            {
                firstId = studyDegrees.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfStudyDegrees.Add(new SelectListItem { Value = "null degree", Text = "No Study degrees" });

                return ListOfStudyDegrees;
            }

            foreach (StudyDegree degree in studyDegrees)
            {

                if (degree.id == firstId)
                {

                    ListOfStudyDegrees.Add(new SelectListItem { Value = Convert.ToString(degree.id), Text = degree.name, Selected = true });
                }
                else
                {
                    ListOfStudyDegrees.Add(new SelectListItem { Value = Convert.ToString(degree.id), Text = degree.name });
                }
            }

            return ListOfStudyDegrees;
        }

        public List<SelectListItem> AllStudyAreas()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfStudyAreas = new List<SelectListItem>();

            IEnumerable<StudyArea> studyAreas = from areas in db.StudyAreas
                                                orderby areas.topic
                                                select areas;

            int firstId = 0;

            try
            {
                firstId = studyAreas.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfStudyAreas.Add(new SelectListItem { Value = "null area", Text = "No study domains" });

                return ListOfStudyAreas;
            }

            foreach (StudyArea area in studyAreas)
            {

                if (area.id == firstId)
                {

                    ListOfStudyAreas.Add(new SelectListItem { Value = Convert.ToString(area.id), Text = area.name, Selected = true });
                }
                else
                {
                    ListOfStudyAreas.Add(new SelectListItem { Value = Convert.ToString(area.id), Text = area.name });
                }
            }

            return ListOfStudyAreas;
        }

        public List<SelectListItem> AllCountries()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> ListOfCountries = new List<SelectListItem>();

            IEnumerable<Country> countries = from ctr in db.Countries
                                             orderby ctr.name
                                             select ctr;

            int firstId;

            try
            {
                firstId = countries.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {

                ListOfCountries.Add(new SelectListItem { Value = "null country", Text = "No countries" });

                return ListOfCountries;
            }

            foreach (Country country in countries)
            {

                if (country.id == firstId)
                {

                    ListOfCountries.Add(new SelectListItem { Value = Convert.ToString(country.id), Text = country.name, Selected = true });
                }
                else
                {
                    ListOfCountries.Add(new SelectListItem { Value = Convert.ToString(country.id), Text = country.name });
                }
            }

            return ListOfCountries;
        }

        public List<SelectListItem> AllDurations()
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Value = "0" , Text = "< 1 month"},
                new SelectListItem{ Value = "1", Text = "1 - 3 month"},
                new SelectListItem{ Value = "2", Text = "3 - 6 month"},
                new SelectListItem{ Value = "3", Text = "> 6 month"}
            };
        }

        public PagedList<ProposalInfo> AllProposals()
        {
            List<ProposalInfo> ListOfProposalsInfo = new ProposalInfo().AllProposals();

            return new PagedList<ProposalInfo>(ListOfProposalsInfo, this.page, this.pageSize);
        }

        public PagedList<ProposalInfo> AllProposalsSearch()
        {
            List<ProposalInfo> ListOfProposalsInfo = new ProposalInfo().AllProposalsSearch(this.searchField);

            return new PagedList<ProposalInfo>(ListOfProposalsInfo, this.page, this.pageSize);
        }

        public PagedList<ProposalInfo> AllProposalsFilter()
        {
            List<ProposalInfo> ListOfProposalsInfo = new ProposalInfo().AllProposalsFilter(this.targets, this.studyDegrees, this.durations, this.country, this.studyArea);

            return new PagedList<ProposalInfo>(ListOfProposalsInfo, this.page, this.pageSize);
        }

    }
}