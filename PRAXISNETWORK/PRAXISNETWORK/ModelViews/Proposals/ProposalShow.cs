﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace PRAXISNETWORK.ModelViews.Proposals
{
    public class ProposalShow
    {
        public int id { get; set; }

        public int providerId { get; set; }

        public Boolean userAlreadyApplied { get; set; }

        public Boolean hasApplications { get; set; }

        [DisplayName("Title")]
        public String title { get; set; }

        [DisplayName("Description")]
        public String description { get; set; }

        [DisplayName("City")]
        public String city { get; set; }

        [DisplayName("Country")]
        public String country { get; set; }

        [DisplayName("Supervisor")]
        public String supervisorFirtLastName { get; set; }

        [DisplayName("Supervisor contact")]
        public String supervisorEmail { get; set; }

        [DisplayName("Starting date")]
        public String startDateText { get; set; }

        [DisplayName("Ending date")]
        public String endDateText { get; set; }

        [DisplayName("Valid until")]
        public String validationDateText { get; set; }

        public DateTime startDate { get; set; }

        public DateTime endDate { get; set; }

        public DateTime validationDate { get; set; }

        [DisplayName("Payment")]
        public String payment { get; set; }

        [DisplayName("Vacancies")]
        public String vacancies { get; set; }

        [DisplayName("Globals skill(s) required")]
        public String globalsSkills { get; set; }

        [DisplayName("Job related skill(s) required")]
        public String jobRelatedSkills { get; set; }

        [DisplayName("Benefits")]
        public String benefits { get; set; }

        [DisplayName("Contact")]
        public String contact { get; set; }

        [DisplayName("Institution promoting offer")]
        public String Institution { get; set; }

        [DisplayName("Host for the offer")]
        public String host { get; set; }

        [DisplayName("Language(s) required")]
        public String languages { get; set; }

        [DisplayName("Skill(s) the offer will promote")]
        public String promotedSkills { get; set; }

        [DisplayName("Offer type(s)")]
        public String targets { get; set; }

        [DisplayName("Study domain(s)")]
        public String studyAreas { get; set; }

        [DisplayName("Study degree(s)")]
        public String studyDegrees { get; set; }
    }
}