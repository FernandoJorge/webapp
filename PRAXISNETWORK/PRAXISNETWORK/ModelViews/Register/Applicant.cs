﻿using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.ModelViews.Register
{
    public class Applicant
    {
        /* FORM FIELDS */

        [Required(ErrorMessage = "Required field")]
        [UsernameExists(2, ErrorMessage = "The username already exists...")]
        [StringLength(50, ErrorMessage = "Max of 50 characters")]
        [DisplayName("Username:")]
        public String username { get; set; }

        [Required(ErrorMessage = "Required field")]
        [StringLength(255, ErrorMessage = "Max of 255 characters")]
        [DisplayName("First name:")]
        public String firstName { get; set; }

        [Required(ErrorMessage = "Required field")]
        [StringLength(255, ErrorMessage = "Max of 255 characters")]
        [DisplayName("Last name:")]
        public String lastName { get; set; }

        [Required(ErrorMessage = "Required field")]
        [DisplayName("Gender:")]
        public String gender { get; set; }

        [Required(ErrorMessage = "Required Field")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        [DisplayName("Birth date:")]
        public String birthDate { get; set; }

        [Required(ErrorMessage = "Required field")]
        [UserEmailExists(2, ErrorMessage = "The email address already exists...")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email address:")]
        public String email { get; set; }

        [DisplayName("Upload your photo(optional):")]
        [SingleUploadFormat(ErrorMessage = "File has a prohibited file format. Only images.")]
        public HttpPostedFileBase picture { get; set; }

        public String picturePath { get; set; }

        [StringLength(2048, ErrorMessage = "Max of 2048 characteres")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Tells us something about you(optional):")]
        public String about { get; set; }

        [Required(ErrorMessage = "Required Field")]
        [DataType(DataType.Password)]
        [StringLength(255, MinimumLength = 8, ErrorMessage = "At least 8 characteres and maximum 255")]
        [DisplayName("Password:")]
        public String password { get; set; }

        [Required(ErrorMessage = "Required Field")]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage = "Confirmation password doesn´t match")]
        [DisplayName("Password confirmation")]
        public String passwordConfirmation { get; set; }

        [DisplayName("Country:")]
        public String country { get; set; }

        [DisplayName("City:")]
        public String city { get; set; }

        /* VALUES FOR FIELDS */

        public Dictionary<String, List<SelectListItem>> ListsOfCountriesAndCities()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            Dictionary<String, List<SelectListItem>> CountriesAndCities = new Dictionary<String, List<SelectListItem>>();
            List<SelectListItem> countriesList = new List<SelectListItem>();
            List<SelectListItem> citiesOfSelectedCountryList = new List<SelectListItem>();

            IEnumerable<Country> countries = from ctr in db.Countries
                                             orderby ctr.name
                                             select ctr;
            /* COUNTRIES */

            int firstCountryId;

            try
            {
                firstCountryId = countries.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {
                countriesList.Add(new SelectListItem { Value = "null country", Text = "No countries", Selected = true });
                citiesOfSelectedCountryList.Add(new SelectListItem { Value = "null city", Text = "No cities" });

                CountriesAndCities.Add("countries", countriesList);
                CountriesAndCities.Add("cities", citiesOfSelectedCountryList);

                return CountriesAndCities;
            }

            foreach (Country country in countries)
            {

                if (!countriesList.Any(c => c.Text == country.name))
                {
                    countriesList.Add(new SelectListItem { Value = Convert.ToString(country.id), Text = country.name, Selected = (country.id == firstCountryId ? true : false) });
                }

            }

            // Adds countries to the dictionary
            CountriesAndCities.Add("countries", countriesList);

            /* CITIES */

            IEnumerable<City> citiesOfSelectedCountry = from ct in db.Cities
                                                        where ct.countryId == firstCountryId
                                                        orderby ct.name
                                                        select ct;

            int firstCityId;

            try
            {
                firstCityId = citiesOfSelectedCountry.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {
                citiesOfSelectedCountryList.Add(new SelectListItem { Value = "null city", Text = "No cities" });

                CountriesAndCities.Add("cities", citiesOfSelectedCountryList);

                return CountriesAndCities;
            }

            foreach (City city in citiesOfSelectedCountry)
            {

                if (!citiesOfSelectedCountryList.Any(c => c.Text == city.name))
                {
                    citiesOfSelectedCountryList.Add(new SelectListItem { Value = Convert.ToString(city.id), Text = city.name, Selected = (city.id == firstCityId ? true : false) });
                }
            }

            // Adds cities to the dictionary
            CountriesAndCities.Add("cities", citiesOfSelectedCountryList);


            return CountriesAndCities;
        }

        public Dictionary<String, List<SelectListItem>> ListsOfCountriesAndCitiesSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            Dictionary<String, List<SelectListItem>> CountriesAndCities = new Dictionary<String, List<SelectListItem>>();
            List<SelectListItem> countriesList = new List<SelectListItem>();
            List<SelectListItem> citiesOfSelectedCountryList = new List<SelectListItem>();

            IEnumerable<Country> countries = from ctr in db.Countries
                                             orderby ctr.name
                                             select ctr;
            /* COUNTRIES */

            int firstCountryId;
            int countryId = Convert.ToInt32(this.country);

            try
            {
                firstCountryId = countries.Where(c => c.id == countryId).FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {
                return ListsOfCountriesAndCities();
            }

            SelectList allCountries = new SelectList(countries, "id", "name", firstCountryId);

            foreach (Country country in countries)
            {

                if (!countriesList.Any(c => c.Text == country.name))
                {
                    countriesList.Add(new SelectListItem { Value = Convert.ToString(country.id), Text = country.name, Selected = (country.id == firstCountryId ? true : false) });
                }

            }

            // Adds countries to the dictionary
            CountriesAndCities.Add("countries", countriesList);

            /* CITIES */

            IEnumerable<City> citiesOfSelectedCountry = from ct in db.Cities
                                                        where ct.countryId == firstCountryId
                                                        orderby ct.name
                                                        select ct;

            int firstCityId;
            int cityId = Convert.ToInt32(this.city);

            try
            {
                firstCityId = citiesOfSelectedCountry.Where(c => c.id == cityId).FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {
                citiesOfSelectedCountryList.Add(new SelectListItem { Value = "null city", Text = "No cities" });

                CountriesAndCities.Add("cities", citiesOfSelectedCountryList);

                return CountriesAndCities;
            }

            foreach (City city in citiesOfSelectedCountry)
            {

                if (!citiesOfSelectedCountryList.Any(c => c.Text == city.name))
                {

                    citiesOfSelectedCountryList.Add(new SelectListItem { Value = Convert.ToString(city.id), Text = city.name, Selected = (city.id == firstCityId ? true : false) });
                }
            }

            // Adds cities to the dictionary
            CountriesAndCities.Add("cities", citiesOfSelectedCountryList);


            return CountriesAndCities;
        }
    }
}