﻿using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.ModelViews.Register
{
    public class Provider
    {
        [Required(ErrorMessage = "Required field")]
        [UsernameExists(1, ErrorMessage = "The username already exists...")]
        [StringLength(50, ErrorMessage = "Max of 50 characters")]
        [DisplayName("Username:")]
        public String username { get; set; }

        [Required(ErrorMessage = "Required field")]
        [StringLength(255, ErrorMessage = "Max of 255 characters")]
        [DisplayName("First name:")]
        public String firstName { get; set; }

        [Required(ErrorMessage = "Required field")]
        [StringLength(255, ErrorMessage = "Max of 255 characters")]
        [DisplayName("Last name:")]
        public String lastName { get; set; }

        [Required(ErrorMessage = "Required field")]
        [UserEmailExists(1, ErrorMessage = "The email address already exists...")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email address:")]
        public String email { get; set; }

        [Required(ErrorMessage = "Required Field")]
        [DataType(DataType.Password)]
        [StringLength(255, MinimumLength = 8, ErrorMessage = "At least 8 characteres and maximum 255")]
        [DisplayName("Password:")]
        public String password { get; set; }

        [Required(ErrorMessage = "Required Field")]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage = "Confirmation password doesn´t match")]
        [DisplayName("Password confirmation")]
        public String passwordConfirmation { get; set; }

        [DisplayName("Select the type of institution information:")]
        public String isOldInstitution { get; set; }

        [DisplayName("Country:")]
        public String country { get; set; }

        [DisplayName("City:")]
        public String city { get; set; }

        [DisplayName("Institutions on Praxis?")]
        public String selectedInstitution { get; set; }

        /* VALUES FOR FIELDS */

        public Dictionary<String, List<SelectListItem>> ListsOfCountriesAndCities()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            Dictionary<String, List<SelectListItem>> CountriesAndCities = new Dictionary<String, List<SelectListItem>>();
            List<SelectListItem> countriesList = new List<SelectListItem>();
            List<SelectListItem> citiesOfSelectedCountryList = new List<SelectListItem>();

            IEnumerable<Country> countries = from ctr in db.Countries
                                             orderby ctr.name
                                             select ctr;
            /* COUNTRIES */

            int firstCountryId;

            try
            {
                firstCountryId = countries.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {
                countriesList.Add(new SelectListItem { Value = "null country", Text = "No countries", Selected = true });
                citiesOfSelectedCountryList.Add(new SelectListItem { Value = "null city", Text = "No cities" });

                CountriesAndCities.Add("countries", countriesList);
                CountriesAndCities.Add("cities", citiesOfSelectedCountryList);

                return CountriesAndCities;
            }

            foreach (Country country in countries)
            {

                if (!countriesList.Any(c => c.Text == country.name))
                {
                    countriesList.Add(new SelectListItem { Value = Convert.ToString(country.id), Text = country.name, Selected = (country.id == firstCountryId ? true : false) });
                }

            }

            // Adds countries to the dictionary
            CountriesAndCities.Add("countries", countriesList);

            /* CITIES */

            IEnumerable<City> citiesOfSelectedCountry = from ct in db.Cities
                                                        where ct.countryId == firstCountryId
                                                        orderby ct.name
                                                        select ct;

            int firstCityId;

            try
            {
                firstCityId = citiesOfSelectedCountry.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {
                citiesOfSelectedCountryList.Add(new SelectListItem { Value = "null city", Text = "No cities" });

                CountriesAndCities.Add("cities", citiesOfSelectedCountryList);

                return CountriesAndCities;
            }

            foreach (City city in citiesOfSelectedCountry)
            {

                if (!citiesOfSelectedCountryList.Any(c => c.Text == city.name))
                {
                    citiesOfSelectedCountryList.Add(new SelectListItem { Value = Convert.ToString(city.id), Text = city.name, Selected = (city.id == firstCityId ? true : false) });
                }
            }

            // Adds cities to the dictionary
            CountriesAndCities.Add("cities", citiesOfSelectedCountryList);


            return CountriesAndCities;
        }

        public Dictionary<String, List<SelectListItem>> ListsOfCountriesAndCitiesSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            Dictionary<String, List<SelectListItem>> CountriesAndCities = new Dictionary<String, List<SelectListItem>>();
            List<SelectListItem> countriesList = new List<SelectListItem>();
            List<SelectListItem> citiesOfSelectedCountryList = new List<SelectListItem>();

            int countryId = Convert.ToInt32(this.country);

            IEnumerable<Country> countries = from ctr in db.Countries
                                             orderby ctr.name
                                             select ctr;
            /* COUNTRIES */

            int firstCountryId;

            try
            {
                firstCountryId = countries.Where(c => c.id == countryId).FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {
                return ListsOfCountriesAndCities();
            }

            foreach (Country country in countries)
            {

                if (!countriesList.Any(c => c.Text == country.name))
                {
                    countriesList.Add(new SelectListItem { Value = Convert.ToString(country.id), Text = country.name, Selected = (country.id == firstCountryId ? true : false) });
                }

            }

            // Adds countries to the dictionary
            CountriesAndCities.Add("countries", countriesList);

            /* CITIES */

            IEnumerable<City> citiesOfSelectedCountry = from ct in db.Cities
                                                        where ct.countryId == countryId
                                                        orderby ct.name
                                                        select ct;
            int cityId = Convert.ToInt32(this.city);
            int firstCityId;

            try
            {
                firstCityId = citiesOfSelectedCountry.Where(c => c.id == cityId).FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {
                citiesOfSelectedCountryList.Add(new SelectListItem { Value = "null city", Text = "No cities" });

                CountriesAndCities.Add("cities", citiesOfSelectedCountryList);

                return CountriesAndCities;
            }

            foreach (City city in citiesOfSelectedCountry)
            {

                if (!citiesOfSelectedCountryList.Any(c => c.Text == city.name))
                {
                    citiesOfSelectedCountryList.Add(new SelectListItem { Value = Convert.ToString(city.id), Text = city.name, Selected = (city.id == firstCityId ? true : false) });
                }
            }

            // Adds cities to the dictionary
            CountriesAndCities.Add("cities", citiesOfSelectedCountryList);


            return CountriesAndCities;
        }

        public List<SelectListItem> ListOfInstitutions()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> institutionsList = new List<SelectListItem>();

            IEnumerable<Institution> institutions = from inst in db.Institutions
                                                    orderby inst.name
                                                    select inst;

            int firstId;

            try
            {
                firstId = institutions.FirstOrDefault().id;
            }
            catch (NullReferenceException ex)
            {
                institutionsList.Add(new SelectListItem { Value = "null institution", Text = "There´s no institutions" });

                return institutionsList;
            }

            foreach (Institution inst in institutions)
            {
                institutionsList.Add(new SelectListItem { Value = Convert.ToString(inst.id), Text = inst.name, Selected = (inst.id == firstId ? true : false) });
            }

            return institutionsList;
        }

        public List<SelectListItem> ListOfInstitutionsSelected()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<SelectListItem> institutionsList = new List<SelectListItem>();

            int institutionId = Convert.ToInt32(this.selectedInstitution);

            IEnumerable<Institution> institutions = from inst in db.Institutions
                                                    orderby inst.name
                                                    select inst;

            int firstId;

            try
            {
                firstId = institutions.Where(i => i.id == institutionId).FirstOrDefault().id;
            }
            catch (NullReferenceException ex1)
            {
                try
                {
                    firstId = institutions.FirstOrDefault().id;
                }catch(NullReferenceException ex2){

                    institutionsList.Add(new SelectListItem { Value = "null institution", Text = "There´s no institutions" });

                    return institutionsList;
                }
            }

            foreach (Institution inst in institutions)
            {
                institutionsList.Add(new SelectListItem { Value = Convert.ToString(inst.id), Text = inst.name, Selected = (inst.id == firstId ? true : false) });
            }

            return institutionsList;
        }

        public Boolean existInstitutions()
        {
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<Institution> institutions = from inst in db.Institutions
                                                    select inst;

            try
            {
                Institution first = institutions.FirstOrDefault();
            }
            catch (NullReferenceException ex)
            {

                return false;
            }

            return true;
        }

        public List<SelectListItem> ListOfInstitutionsInfo()
        {
            List<SelectListItem> ListOfInstitutionsInfo = new List<SelectListItem>();

            ListOfInstitutionsInfo.Add(new SelectListItem
            {

                Value = "1",
                Text = "Without institution information"
            });

            ListOfInstitutionsInfo.Add(new SelectListItem
            {

                Value = "2",
                Text = "Available institution information"
            });

            return ListOfInstitutionsInfo;
        }
    }
}