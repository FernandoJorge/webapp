﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Models
{
    public class AllProviders
    {
        public int id { get; set; }

        public int page { get; set; }

        public int pageSize { get; set; }

        public String username { get; set; }

        public String email { get; set; }

        public String lastLogin { get; set; }

        public List<SelectListItem> enabled { get; set; }

        public List<SelectListItem> locked { get; set; }

        public PagedList<AllProviders> allProviders()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<AllProviders> providers = new List<AllProviders>();

            IEnumerable<User> allProviders = from users in db.Users
                                             where users.userTypeId == 1
                                             select users;

            if (allProviders.ToList() != null && allProviders.ToList().Count() > 0)
            {
                foreach (User provider in allProviders)
                {

                    providers.Add(new AllProviders()
                    {

                        id = provider.id,
                        username = provider.firstName + " " + provider.lastName,
                        email = provider.email,
                        lastLogin = provider.lastLogin.ToString(),
                        enabled = new List<SelectListItem>()
                        {
                            new SelectListItem(){Value = Convert.ToString(provider.id) + "," + "Yes", Text="Yes" , Selected = (provider.enabled == true?true:false)},
                            new SelectListItem(){Value = Convert.ToString(provider.id) + "," + "No", Text="No", Selected = (provider.enabled == true?false:true)},
                        },

                        locked = new List<SelectListItem>()
                        {
                            new SelectListItem(){Value = Convert.ToString(provider.id) + "," + "Yes", Text="Yes" , Selected = (provider.locked == true?true:false)},
                            new SelectListItem(){Value = Convert.ToString(provider.id) + "," + "No", Text="No", Selected = (provider.locked == true?false:true)},
                        }
                    });
                }
            }

            return new PagedList<AllProviders>(providers, this.page, this.pageSize);
        }
    }
}