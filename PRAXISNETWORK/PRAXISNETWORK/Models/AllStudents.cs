﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Models
{
    public class AllStudents
    {
        public int id { get; set; }

        public int page { get; set; }

        public int pageSize { get; set; }

        public String username { get; set; }

        public String email { get; set; }

        public String lastLogin { get; set; }

        public List<SelectListItem> enabled { get; set; }

        public List<SelectListItem> locked { get; set; }

        public PagedList<AllStudents> allProviders()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<AllStudents> students = new List<AllStudents>();

            IEnumerable<User> allStudents = from users in db.Users
                                             where users.userTypeId == 2
                                             select users;

            if (allStudents.ToList() != null && allStudents.ToList().Count() > 0)
            {
                foreach (User student in allStudents)
                {

                    students.Add(new AllStudents()
                    {

                        id = student.id,
                        username = student.firstName + " " + student.lastName,
                        email = student.email,
                        lastLogin = student.lastLogin.ToString(),
                        enabled = new List<SelectListItem>()
                        {
                            new SelectListItem(){Value = Convert.ToString(student.id) + "," + "Yes", Text="Yes" , Selected = (student.enabled == true?true:false)},
                            new SelectListItem(){Value = Convert.ToString(student.id) + "," + "No", Text="No", Selected = (student.enabled == true?false:true)},
                        },

                        locked = new List<SelectListItem>()
                        {
                            new SelectListItem(){Value = Convert.ToString(student.id) + "," + "Yes", Text="Yes" , Selected = (student.locked == true?true:false)},
                            new SelectListItem(){Value = Convert.ToString(student.id) + "," + "No", Text="No", Selected = (student.locked == true?false:true)},
                        }
                    });
                }
            }

            return new PagedList<AllStudents>(students, this.page, this.pageSize);
        }
    }
}