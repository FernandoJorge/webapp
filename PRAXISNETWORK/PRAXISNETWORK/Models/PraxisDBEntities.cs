﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace PRAXISNETWORK.Models
{
    // Database context
    public class PraxisDBEntities : DbContext
    {
        public PraxisDBEntities()
            : base("name=PraxisDB")
        {
            Database.SetInitializer<PraxisDBEntities>(new CreateDatabaseIfNotExists<PraxisDBEntities>());
        }

        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<InstitutionType> InstitutionTypes { get; set; }
        public DbSet<Institution> Institutions { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Proposal> Proposals { get; set; }
        public DbSet<DocType> DocTypes { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Certificate> Certificates { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Cv> Cvs { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<ApplicantSkill> ApplicantSkills { get; set; }
        public DbSet<RequiredSkill> RequiredSkills { get; set; }
        public DbSet<PromotedSkill> PromotedSkills { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<ApplicationDocs> ApplicationDocs { get; set; }
        public DbSet<Target> Targets { get; set; }
        public DbSet<ProposalTarget> ProposalsTargets { get; set; }
        public DbSet<StudyArea> StudyAreas { get; set; }
        public DbSet<StudyDegree> StudyDegrees { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<ProposalStudyArea> ProposalStudyAreas { get; set; }
        public DbSet<ProposalStudyDegree> ProposalStudyDegrees { get; set; }
        public DbSet<ProposalLanguage> ProposalLanguages { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Media> Medias { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Testimonial> Testimonials { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<MediaGallery> MediaGalleries { get; set; }
        public DbSet<EventType> EventTypes { get; set; }
        public DbSet<Alert> Alerts { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<CfePage> CfePages { get; set; }
        public DbSet<NewsEvents> NewsEvents { get; set; }
        public DbSet<SearchRequest> SearchRequests { get; set; }
        public DbSet<RelatedDocumentation> RelatedDocumentations { get; set; }
        public DbSet<Config> Config { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<EmailSSL> EmailSSLs { get; set; }
        public DbSet<EmailService> EmailServices { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            //Configure domain classes using Fluent API here

            //modelBuilder.Entity<City>()
            //.HasMany(p => p.proposals)
            //.WithRequired(c => c.city)
            //.WillCascadeOnDelete(true);

            //modelBuilder.Entity<City>()
            //    .HasMany<Proposal>(c => c.cityProposals)
            //    .WithOptional(x => x.city)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();


        }
    }

    /* CONCEPTUAL MODEL */

    [Table("COUNTRIES")]
    public class Country
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

    }

    [Table("CITIES")]
    public class City
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

        [ForeignKey("country")]
        [Column("COUNTRY_ID")]
        public int countryId { get; set; }

        public virtual Country country { get; set; }

        public virtual List<Proposal> proposals { get; set; }

        public City()
        {
            proposals = new List<Proposal>();
        }
    }

    [Table("INSTITUTION_TYPES")]
    public class InstitutionType
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

    }

    [Table("INSTITUTIONS")]
    public class Institution
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("PHONE")]
        [StringLength(255)]
        public string phone { get; set; }

        [Column("EMAIL")]
        [StringLength(255)]
        public string email { get; set; }

        [Column("CERTIFYING_TOKEN")]
        [StringLength(255)]
        public string certifyingToken { get; set; }

        [Column("DELEGATE_TO_USER")]
        public int delegateToUser { get; set; }

        [Column("RESTRICTED_TO")]
        public int restrictedTo { get; set; }

        [Column("MAIN_TARGET")]
        public int mainTarget { get; set; }

        [Column("ASSOCIATED_TO")]
        public int associatedTo { get; set; }

        [Column("NAME")]
        [StringLength(100)]
        public string name { get; set; }

        [Column("CONTACT_FIRST_NAME")]
        [StringLength(255)]
        public string contactFirstName { get; set; }

        [Column("CONTACT_LAST_NAME")]
        [StringLength(255)]
        public string contactLastName { get; set; }

        [Column("VAT_NUMBER")]
        [StringLength(255)]
        public string vatNumber { get; set; }

        [Column("BRAND")]
        [StringLength(255)]
        public string brand { get; set; }

        [Column("PICTURE")]
        [StringLength(255)]
        public string picture { get; set; }

        [Column("WEBSITE")]
        [StringLength(255)]
        public string website { get; set; }

        [Column("STATUS")]
        [StringLength(255)]
        public string status { get; set; }

        [Column("CERTIFIED_BY")]
        [StringLength(255)]
        public string certifiedBy { get; set; }

        [Column("CERTIFIED")]
        public Boolean certified { get; set; }

        [Column("CERTIFYING_INSTITUTION_ID")]
        public int certiyingInstitutionId { get; set; }

        [ForeignKey("institutionType")]
        [Column("INSTITUTION_TYPE_ID")]
        public int institutionTypeId { get; set; }

        public virtual InstitutionType institutionType { get; set; }

        [ForeignKey("city")]
        [Column("CITY_ID")]
        public int? cityId { get; set; }

        public virtual City city { get; set; }

        [InverseProperty("institution")]
        public virtual List<Proposal> institutionProposals { get; set; }

        [InverseProperty("host")]
        public virtual List<Proposal> hostProposals { get; set; }


        public virtual List<User> users { get; set; }

        public Institution()
        {
            institutionProposals = new List<Proposal>();
            hostProposals = new List<Proposal>();
            users = new List<User>();
        }
    }

    [Table("USER_TYPES")]
    public class UserType
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("TYPE")]
        [StringLength(255)]
        public string Type { get; set; }

    }

    [Table("USERS")]
    public class User
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("USERNAME")]
        [StringLength(255)]
        public string username { get; set; }

        [Column("EMAIL")]
        [StringLength(255)]
        public string email { get; set; }

        [Column("ENABLED")]
        public Boolean? enabled { get; set; }

        [Column("SALT")]
        [StringLength(255)]
        public string salt { get; set; }

        [Column("PASSWORD")]
        [StringLength(255)]
        public string password { get; set; }

        [Column("LAST_LOGIN")]
        public DateTime? lastLogin { get; set; }

        [Column("LOCKED")]
        public Boolean? locked { get; set; }

        [Column("CONFIRMATION_TOKEN")]
        [StringLength(255)]
        public string confirmationToken { get; set; }

        [Column("PASSWORD_REQUEST_AT")]
        public DateTime? passwordRequestAt { get; set; }

        [Column("ROLES")]
        [StringLength(2048)]
        public string roles { get; set; }

        [Column("CREDENTIALS_EXPIRED")]
        public Boolean? credentialsExpired { get; set; }

        [Column("CREDENTIALS_EXPIRED_AT")]
        public DateTime? credentialsExpiredAt { get; set; }

        [Column("FIRST_NAME")]
        [StringLength(255)]
        public string firstName { get; set; }

        [Column("LAST_NAME")]
        [StringLength(255)]
        public string lastName { get; set; }

        [Column("BIRTH_DATE")]
        [StringLength(255)]
        public string birthDate { get; set; }

        [Column("GENDER")]
        [StringLength(6)]
        public string gender { get; set; }

        [Column("WEBSITE")]
        [StringLength(255)]
        public string website { get; set; }

        [Column("PICTURE")]
        [StringLength(255)]
        public string picture { get; set; }

        [Column("ABOUT")]
        [StringLength(2048)]
        public string about { get; set; }

        [ForeignKey("city")]
        [Column("CITY_ID")]
        public int? cityId { get; set; }

        public virtual City city { get; set; }

        [ForeignKey("userType")]
        [Column("USER_TYPE_ID")]
        public int userTypeId { get; set; }

        public virtual UserType userType { get; set; }

        [ForeignKey("institution")]
        [Column("INSTITUTION_ID")]
        public int? institutionId { get; set; }

        public virtual Institution institution { get; set; }

        public virtual List<Document> documents { get; set; }
        public virtual List<Certificate> certificates { get; set; }
        public virtual List<Experience> experiences { get; set; }
        public virtual List<Attachment> attachments { get; set; }
        public virtual Cv cv { get; set; }
        public virtual List<ApplicantSkill> applicantSkills { get; set; }
        public virtual List<Application> applications { get; set; }
        public virtual List<Proposal> proposalsMade { get; set; }

        public User()
        {
            documents = new List<Document>();
            certificates = new List<Certificate>();
            experiences = new List<Experience>();
            attachments = new List<Attachment>();
            applicantSkills = new List<ApplicantSkill>();
            applications = new List<Application>();
            proposalsMade = new List<Proposal>();
        }
    }

    [Table("PROPOSALS")]
    public class Proposal
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("SUPERVISOR_FIRST_NAME")]
        [StringLength(255)]
        public string supervisorFirstName { get; set; }

        [Column("SUPERVISOR_LAST_NAME")]
        [StringLength(255)]
        public string supervisorLastName { get; set; }

        [Column("SUPERVISOR_EMAIL")]
        [StringLength(255)]
        public string supervisorEmail { get; set; }

        [Column("TITLE")]
        [StringLength(255)]
        public string title { get; set; }

        [Column("STATUS")]
        [StringLength(255)]
        public string status { get; set; }

        [Column("DESCRIPTION")]
        [StringLength(2048)]
        public string description { get; set; }

        [Column("START_DATE")]
        public DateTime startDate { get; set; }

        [Column("END_DATE")]
        public DateTime endDate { get; set; }

        [Column("SUBMIT_DATE")]
        public DateTime submitDate { get; set; }

        [Column("VALID_FORM")]
        public DateTime? validForm { get; set; }

        [Column("VALID_TO")]
        public DateTime validTo { get; set; }

        [Column("WORKLOAD")]
        [StringLength(255)]
        public string workload { get; set; }

        [Column("PAYMENT")]
        [StringLength(255)]
        public string payment { get; set; }

        [Column("VACANCIES")]
        [StringLength(255)]
        public string vacancies { get; set; }

        [Column("PICTURE")]
        [StringLength(255)]
        public string picture { get; set; }

        [Column("STARS")]
        [StringLength(255)]
        public string stars { get; set; }

        [Column("PATH")]
        [StringLength(255)]
        public string path { get; set; }

        [Column("PROVIDED_BY_ID")]
        public int providedById { get; set; }

        [Column("VISIBLE")]
        public Boolean visible { get; set; }

        [Column("GLOBAL_SKILLS")]
        [StringLength(2048)]
        public string globalSkills { get; set; }

        [Column("JOB_RELATED_SKILLS")]
        [StringLength(2048)]
        public string jobRelatedSkills { get; set; }

        [Column("BENEFITS")]
        [StringLength(2048)]
        public string benefits { get; set; }

        [Column("CONTACT")]
        [StringLength(2048)]
        public string contact { get; set; }

        [Column("DURATION")]
        public int duration { get; set; }

        [Column("APPLY_ON_PRAXIS")]
        public Boolean applyOnPraxis { get; set; }

        [Column("HOST_FREE_TEXT")]
        [StringLength(255)]
        public string hostFreeText { get; set; }

        [Column("DELETED")]
        public Boolean deleted { get; set; }

        [ForeignKey("provider")]
        [Column("PROVIDER_ID")]
        public int providerId { get; set; }

        public virtual User provider { get; set; }

        [ForeignKey("city")]
        [Column("CITY_ID")]
        public int cityId { get; set; }

        public virtual City city { get; set; }

        [ForeignKey("country")]
        [Column("COUNTRY_ID")]
        public int countryId { get; set; }

        public virtual Country country { get; set; }


        [Column("INSTITUTION_ID")]
        public int institutionId { get; set; }

        [ForeignKey("institutionId")]
        [InverseProperty("institutionProposals")]
        public virtual Institution institution { get; set; }

        [Column("HOST_ID")]
        public int hostId { get; set; }

        [ForeignKey("hostId")]
        [InverseProperty("hostProposals")]
        public virtual Institution host { get; set; }

        // Colections
        public virtual List<RequiredSkill> requiredSkills { get; set; }
        public virtual List<PromotedSkill> promotedSkills { get; set; }
        public virtual List<ProposalTarget> targets { get; set; }
        public virtual List<ProposalStudyArea> studyAreas { get; set; }
        public virtual List<ProposalStudyDegree> studyDegrees { get; set; }
        public virtual List<ProposalLanguage> languages { get; set; }

        public Proposal()
        {
            requiredSkills = new List<RequiredSkill>();
            promotedSkills = new List<PromotedSkill>();
            targets = new List<ProposalTarget>();
            studyAreas = new List<ProposalStudyArea>();
            studyDegrees = new List<ProposalStudyDegree>();
            languages = new List<ProposalLanguage>();
        }
    }

    [Table("DOC_TYPES")]
    public class DocType
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }
    }

    [Table("DOCUMENTS")]
    public class Document
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("TITLE")]
        [StringLength(255)]
        public string title { get; set; }

        [Column("PATH")]
        [StringLength(255)]
        public string path { get; set; }

        [Column("CREATED_AT")]
        public DateTime createdAt { get; set; }

        [Column("IS_PRIVATE")]
        public Boolean isPrivate { get; set; }

        [Column("URL")]
        [StringLength(255)]
        public string url { get; set; }

        [Column("DESCRIPTION")]
        [StringLength(2048)]
        public string description { get; set; }

        [Column("CITATIONS")]
        [StringLength(2048)]
        public string citations { get; set; }

        [ForeignKey("user")]
        [Column("USER_ID")]
        public int userId { get; set; }

        public virtual User user { get; set; }

        [ForeignKey("docType")]
        [Column("DOC_TYPE_ID")]
        public int docTypeId { get; set; }

        public virtual DocType docType { get; set; }

    }

    [Table("CERTIFICATES")]
    public class Certificate
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

        [Column("DATE")]
        public DateTime date { get; set; }

        [Column("MARK")]
        [StringLength(255)]
        public string mark { get; set; }

        [ForeignKey("user")]
        [Column("USER_ID")]
        public int userId { get; set; }

        public virtual User user { get; set; }

    }

    [Table("EXPERIENCES")]
    public class Experience
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("DESCRIPTION")]
        [StringLength(2048)]
        public string description { get; set; }

        [ForeignKey("user")]
        [Column("USER_ID")]
        public int userId { get; set; }

        public virtual User user { get; set; }

    }

    [Table("ATTACHMENTS")]
    public class Attachment
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

        [Column("PATH")]
        [StringLength(255)]
        public string path { get; set; }

        [ForeignKey("user")]
        [Column("USER_ID")]
        public int userId { get; set; }

        public virtual User user { get; set; }

    }

    [Table("CVS")]
    public class Cv
    {
        [Key, ForeignKey("user")]
        [Column("USER_ID")]
        public int userId { get; set; }

        [Column("VIDEO_CV")]
        [StringLength(255)]
        public string videoCV { get; set; }

        [Column("PDF_CV")]
        [StringLength(255)]
        public string pdfCV { get; set; }

        [Column("EDUCATION")]
        [StringLength(255)]
        public string education { get; set; }

        [Column("VISIBILITY")]
        [StringLength(255)]
        public string visibility { get; set; }

        public virtual User user { get; set; }

    }

    [Table("SKILLS")]
    public class Skill
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("DESCRIPTION")]
        [StringLength(2048)]
        public string description { get; set; }

        [Column("TYPE")]
        [StringLength(255)]
        public string type { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }
    }

    [Table("APPLICANT_SKILLS")]
    public class ApplicantSkill
    {
        [Key, ForeignKey("user")]
        [Column("USER_ID", Order = 1)]
        public int userId { get; set; }

        [Key, ForeignKey("skill")]
        [Column("SKILL_ID", Order = 2)]
        public int skillId { get; set; }

        public virtual User user { get; set; }
        public virtual Skill skill { get; set; }

    }

    [Table("REQUIRED_SKILLS")]
    public class RequiredSkill
    {
        [Key, ForeignKey("proposal")]
        [Column("PROPOSAL_ID", Order = 1)]
        public int proposalId { get; set; }

        [Key, ForeignKey("skill")]
        [Column("SKILL_ID", Order = 2)]
        public int skillId { get; set; }


        public virtual Proposal proposal { get; set; }
        public virtual Skill skill { get; set; }

    }

    [Table("PROMOTED_SKILLS")]
    public class PromotedSkill
    {
        [Key, ForeignKey("proposal")]
        [Column("PROPOSAL_ID", Order = 1)]
        public int proposalId { get; set; }

        [Key, ForeignKey("skill")]
        [Column("SKILL_ID", Order = 2)]
        public int skillId { get; set; }

        public virtual Proposal proposal { get; set; }
        public virtual Skill skill { get; set; }

    }

    [Table("APPLICATIONS")]
    public class Application
    {
        [Key, ForeignKey("proposal")]
        [Column("PROPOSAL_ID", Order = 1)]
        public int proposalId { get; set; }

        [Key, ForeignKey("user")]
        [Column("USER_ID", Order = 2)]
        public int userId { get; set; }

        [Column("STATUS")]
        [StringLength(255)]
        public string status { get; set; }

        [Column("APPLICATION_DATE")]
        public DateTime applicationDate { get; set; }

        [Column("MESSAGE")]
        [StringLength(255)]
        public string message { get; set; }

        [Column("STAR")]
        public Boolean star { get; set; }

        public virtual Proposal proposal { get; set; }
        public virtual User user { get; set; }
        public virtual List<ApplicationDocs> applicationDocs { get; set; }

        public Application()
        {
            applicationDocs = new List<ApplicationDocs>();
        }

    }

    [Table("APPLICATION_DOCS")]
    public class ApplicationDocs
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("SUBMIT_DATE")]
        public DateTime submitDate { get; set; }

        [Column("PATH")]
        [StringLength(2048)]
        public string path { get; set; }

        [Column("DESCRIPTION")]
        [StringLength(255)]
        public string description { get; set; }

        [Column("PROPOSAL_ID")]
        public int proposalId { get; set; }

        [Column("USER_ID")]
        public int userId { get; set; }

        [ForeignKey("proposalId,userId")]
        public virtual Application application { get; set; }
    }

    [Table("TARGETS")]
    public class Target
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

    }

    [Table("PROPOSAL_TARGET")]
    public class ProposalTarget
    {
        [Key, ForeignKey("proposal")]
        [Column("PROPOSAL_ID", Order = 1)]
        public int proposalId { get; set; }

        [Key, ForeignKey("target")]
        [Column("TARGET_ID", Order = 2)]
        public int targetId { get; set; }


        public virtual Proposal proposal { get; set; }
        public virtual Target target { get; set; }

    }

    [Table("STUDY_AREAS")]
    public class StudyArea
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

        [Column("TOPIC")]
        [StringLength(255)]
        public string topic { get; set; }

    }

    [Table("STUDY_DEGREES")]
    public class StudyDegree
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }
    }

    [Table("LANGUAGES")]
    public class Language
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }
    }

    [Table("PROPOSAL_STUDY_AREA")]
    public class ProposalStudyArea
    {

        [Key, ForeignKey("proposal")]
        [Column("PROPOSAL_ID", Order = 1)]
        public int proposalId { get; set; }

        [Key, ForeignKey("studyArea")]
        [Column("STUDY_AREA_ID", Order = 2)]
        public int studyAreaId { get; set; }

        public virtual Proposal proposal { get; set; }
        public virtual StudyArea studyArea { get; set; }

    }

    [Table("PROPOSAL_STUDY_DEGREES")]
    public class ProposalStudyDegree
    {
        [Key, ForeignKey("proposal")]
        [Column("PROPOSAL_ID", Order = 1)]
        public int proposalId { get; set; }

        [Key, ForeignKey("studyDegree")]
        [Column("STUDY_DEGREE_ID", Order = 2)]
        public int studyDegreeId { get; set; }

        public virtual Proposal proposal { get; set; }
        public virtual StudyDegree studyDegree { get; set; }

    }

    [Table("PROPOSAL_LANGUAGES")]
    public class ProposalLanguage
    {

        [Key, ForeignKey("proposal")]
        [Column("PROPOSAL_ID", Order = 1)]
        public int proposalId { get; set; }

        [Key, ForeignKey("language")]
        [Column("LANGUAGE_ID", Order = 2)]
        public int languageId { get; set; }

        public virtual Proposal proposal { get; set; }
        public virtual Language language { get; set; }


    }

    [Table("CATEGORIES")]
    public class Category
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

        [Column("DESCRIPTION")]
        [StringLength(255)]
        public string description { get; set; }
    }

    [Table("MEDIA")]
    public class Media
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

        [Column("DESCRIPTION")]
        [StringLength(2048)]
        public string description { get; set; }

        [Column("ENABLED")]
        public Boolean enabled { get; set; }

        [Column("PROVIDER_NAME")]
        [StringLength(255)]
        public string providerName { get; set; }

        [Column("PROVIDER_STATUS")]
        public int providerStatus { get; set; }

        [Column("PROVIDER_REFERENCE")]
        [StringLength(255)]
        public string providerReference { get; set; }

        [Column("PROVIDER_METADATA")]
        [StringLength(2048)]
        public string providerMetadata { get; set; }

        [Column("WIDTH")]
        public int width { get; set; }

        [Column("HEIGHT")]
        public int height { get; set; }

        [Column("LENGTH")]
        public Decimal length { get; set; }

        [Column("CONTENT_TYPE")]
        [StringLength(64)]
        public string contentType { get; set; }

        [Column("CONTENT_SIZE")]
        public int contentSize { get; set; }

        [Column("COPYRIGHT")]
        [StringLength(255)]
        public string copyright { get; set; }

        [Column("AUTHOR_NAME")]
        [StringLength(255)]
        public string authorName { get; set; }

        [Column("CONTEXT")]
        [StringLength(64)]
        public string context { get; set; }

        [Column("CDN_IS_FLUSHABLE")]
        public Boolean cdnIsFlushable { get; set; }

        [Column("CDN_FLUSH_AT")]
        public DateTime cdnFlushAt { get; set; }

        [Column("CDN_STATUS")]
        public int cdnStatus { get; set; }

        [Column("UPDATED_AT")]
        public DateTime updatedAt { get; set; }

        [Column("CREATED_AT")]
        public DateTime createdAt { get; set; }

        [InverseProperty("picture")]
        public virtual List<News> newsPictures { get; set; }

        [InverseProperty("file")]
        public virtual List<News> newsFiles { get; set; }

        [InverseProperty("picture")]
        public virtual List<Testimonial> testimonialPictures { get; set; }

        [InverseProperty("file")]
        public virtual List<Testimonial> testimonialFiles { get; set; }

        public Media()
        {
            newsPictures = new List<News>();
            newsFiles = new List<News>();
            testimonialPictures = new List<Testimonial>();
            testimonialFiles = new List<Testimonial>();
        }

    }

    [Table("NEWS")]
    public class News
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("TITLE")]
        [StringLength(255)]
        public string title { get; set; }

        [Column("TEASER")]
        [StringLength(2048)]
        public string teaser { get; set; }

        [Column("CONTENT")]
        [StringLength(2048)]
        public string content { get; set; }

        [Column("CREATION_DATE")]
        public DateTime cretionDate { get; set; }

        [Column("MODIFICATION_DATE")]
        public DateTime modificationDate { get; set; }

        [Column("VISIBILITY")]
        public Boolean visibility { get; set; }

        [Column("INHOME")]
        public Boolean inhome { get; set; }

        [Column("PICTURE_ID")]
        public int pictureId { get; set; }

        [ForeignKey("pictureId")]
        [InverseProperty("newsPictures")]
        public virtual Media picture { get; set; }

        [Column("FILE_ID")]
        public int fileId { get; set; }

        [ForeignKey("fileId")]
        [InverseProperty("newsFiles")]
        public virtual Media file { get; set; }

        [ForeignKey("category")]
        [Column("CATEGORY_ID")]
        public int categoryId { get; set; }

        public virtual Category category { get; set; }

    }

    [Table("TESTIMONIALS")]
    public class Testimonial
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("TITLE")]
        [StringLength(255)]
        public string title { get; set; }

        [Column("TYPE")]
        [StringLength(255)]
        public string type { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

        [Column("TEASER")]
        [StringLength(2048)]
        public string teaser { get; set; }

        [Column("CONTENT")]
        [StringLength(2048)]
        public string content { get; set; }

        [Column("CREATION_DATE")]
        public DateTime cretionDate { get; set; }

        [Column("MODIFICATION_DATE")]
        public DateTime modificationDate { get; set; }

        [Column("VISIBILITY")]
        public Boolean visibility { get; set; }

        [Column("PICTURE_ID")]
        public int pictureId { get; set; }

        [ForeignKey("pictureId")]
        [InverseProperty("testimonialPictures")]
        public virtual Media picture { get; set; }

        [Column("FILE_ID")]
        public int fileId { get; set; }

        [ForeignKey("fileId")]
        [InverseProperty("testimonialFiles")]
        public virtual Media file { get; set; }

    }

    [Table("GALLERIES")]
    public class Gallery
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        [StringLength(255)]
        public string name { get; set; }

        [Column("CONTEXT")]
        [StringLength(64)]
        public string context { get; set; }

        [Column("DEFAULT_FORMAT")]
        [StringLength(255)]
        public string defaultFormat { get; set; }

        [Column("ENABLED")]
        public Boolean enabled { get; set; }

        [Column("UPDATED_AT")]
        public DateTime updatedAt { get; set; }

        [Column("CREATED_AT")]
        public DateTime createdAt { get; set; }
    }

    [Table("MEDIA_GALLERY")]
    public class MediaGallery
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("POSITION")]
        public int position { get; set; }

        [Column("ENABLED")]
        public Boolean enabled { get; set; }

        [Column("UPDATED_AT")]
        public DateTime updatedAt { get; set; }

        [Column("CREATED_AT")]
        public DateTime createdAt { get; set; }

        [ForeignKey("media")]
        [Column("MEDIA_ID")]
        public int mediaId { get; set; }

        public virtual Media media { get; set; }

        [ForeignKey("gallery")]
        [Column("GALLERY_ID")]
        public int galleryId { get; set; }

        public virtual Gallery gallery { get; set; }

    }

    [Table("EVENT_TYPES")]
    public class EventType
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("TYPE")]
        [StringLength(255)]
        public string type { get; set; }
    }

    [Table("ALERTS")]
    public class Alert
    {

        [Key]
        [Column("USER_ID")]
        public int userId { get; set; }

        [Column("STATUS")]
        [StringLength(255)]
        public string status { get; set; }

        [Column("KEYWORDS")]
        [StringLength(255)]
        public string keywords { get; set; }

        [Column("HOST_ID")]
        public int hostId { get; set; }

        [Column("CITY_ID")]
        public int cityId { get; set; }

        [Column("HOSTED_TYPE_ID")]
        public int hostedTypeId { get; set; }

        [Column("TARGET_ID")]
        public int targetId { get; set; }

        [Column("STUDY_AREA_ID")]
        public int studyAreaId { get; set; }

        [Column("START_DATE")]
        public DateTime startDate { get; set; }

        [Column("END_DATE")]
        public DateTime endDate { get; set; }

        [Column("WORKLOAD")]
        [StringLength(255)]
        public string workload { get; set; }

        [Column("PAYMENT")]
        [StringLength(255)]
        public string payment { get; set; }
    }

    [Table("EVENTS")]
    public class Event
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("CITY_ID")]
        public int cityId { get; set; }

        [Column("TITLE")]
        [StringLength(255)]
        public string title { get; set; }

        [Column("TYPE")]
        [StringLength(255)]
        public string type { get; set; }

        [Column("URL")]
        [StringLength(255)]
        public string url { get; set; }

        [Column("DESCRIPTION")]
        [StringLength(2048)]
        public string description { get; set; }

        [Column("LAST_VIEWED")]
        [StringLength(255)]
        public string lastViewed { get; set; }

        [Column("START_DATE")]
        public DateTime startDate { get; set; }

        [Column("END_DATE")]
        public DateTime endDate { get; set; }

        [Column("UPLOAD_DATE")]
        public DateTime uploadDate { get; set; }

        [Column("HITS")]
        [StringLength(255)]
        public string hits { get; set; }
    }

    [Table("CFE_PAGES")]
    public class CfePage
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("TITLE")]
        [StringLength(255)]
        public string title { get; set; }

        [Column("CONTENT")]
        [StringLength(2048)]
        public string content { get; set; }

        [Column("ALIAS")]
        [StringLength(255)]
        public string alias { get; set; }

        [Column("URL")]
        [StringLength(255)]
        public string url { get; set; }

        [ForeignKey("parent")]
        [Column("PARENT_ID")]
        public int parentId { get; set; }

        public virtual CfePage parent { get; set; }

    }

    [Table("NEWS_EVENTS")]
    public class NewsEvents
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("TITLE")]
        [StringLength(255)]
        public string title { get; set; }

        [Column("TEASER")]
        [StringLength(2048)]
        public string teaser { get; set; }

        [Column("CONTENT")]
        [StringLength(2048)]
        public string content { get; set; }

        [Column("CREATION_DATE")]
        public DateTime creationDate { get; set; }

        [Column("MODIFICATION_DATE")]
        public DateTime modificationDate { get; set; }

        [Column("VISIBILITY")]
        public Boolean visibility { get; set; }
    }

    [Table("SEARCH_REQUESTS")]
    public class SearchRequest
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("DATE")]
        public DateTime date { get; set; }

        [Column("N_RESULTS")]
        public int nResults { get; set; }

        [Column("SESSION_DATA")]
        [StringLength(2048)]
        public string sessionData { get; set; }

        [Column("SESSION_ALL_DATA")]
        [StringLength(2048)]
        public string sessionAllData { get; set; }

        [Column("DATA_RESULT")]
        [StringLength(2048)]
        public string dataResult { get; set; }
    }

    [Table("RELATED_DOCUMENTATIONS")]
    public class RelatedDocumentation
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("USER_ID")]
        public int userId { get; set; }

        [Column("PROPOSAL_ID")]
        public int proposalId { get; set; }

        [Column("TITLE")]
        [StringLength(255)]
        public string title { get; set; }

        [Column("DESCRIPTION")]
        [StringLength(2048)]
        public string description { get; set; }

        [Column("URL")]
        [StringLength(2048)]
        public string url { get; set; }
    }

    [Table("CONFIG")]
    public class Config
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("MAX_APPLICATIONS")]
        public int maxApplications { get; set; }

        [Column("MAX_PROPOSALS")]
        public int maxProposals { get; set; }

        [Column("DEFAULT_VALIDITY")]
        public int defaultValidity { get; set; }

        [Column("CONTACT_EMAIL")]
        [StringLength(255)]
        public string contactEmail { get; set; }
    }

    [Table("REQUESTS")]
    public class Request
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("DATE")]
        public DateTime date { get; set; }

        [Column("N_RESULTS")]
        public int nResults { get; set; }

        [Column("SESSION_DATA")]
        [StringLength(2048)]
        public string sessionData { get; set; }
    }

    [Table("DOCUMENT_TYPES")]
    public class DocumentType
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("TYPE")]
        [StringLength(255)]
        public string type { get; set; }
    }

    [Table("EMAIL_SERVICE")]
    public class EmailService
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("SMTP_SERVER")]
        [StringLength(255)]
        public string smtpServer { get; set; }

        [Column("SMTP_PORT")]
        public int smtpPort { get; set; }

        [Column("FROM_EMAIL")]
        [StringLength(255)]
        public string fromEmail { get; set; }

        [Column("USERNAME")]
        [StringLength(255)]
        public string username { get; set; }

        [Column("PASSWORD")]
        [StringLength(255)]
        public string password { get; set; }

        [Column("EMAIL_SSL_ID")]
        [ForeignKey("emailSSLEncryption")]
        public int emailSSLId { get; set; }

        public virtual EmailSSL emailSSLEncryption { get; set; }
    }

    [Table("EMAIL_SSLS")]
    public class EmailSSL
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("DESCRIPTION")]
        [StringLength(255)]
        public string description { get; set; }

        [Column("SIGLE")]
        public string sigle { get; set; }
    }

    [Table("SUBSCRIBERS")]
    public class Subscriber
    {
        [Key]
        [StringLength(255)]
        [Column("EMAIL")]
        public string email { get; set; }

        [StringLength(255)]
        [Column("NAME")]
        public string name { get; set; }

        [Column("SUBSCRIPITON_DATE")]
        public DateTime subscriptionDate { get; set; }
    }

}