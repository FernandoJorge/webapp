﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Models
{
    public class ProposalApplications
    {
        public int page { get; set; }

        public int pageSize { get; set; }

        public int proposalId { get; set; }

        public int userId { get; set; }

        public String username { get; set; }

        public String applicationDate { get; set; }

        public List<SelectListItem> status { get; set; }

        public PagedList<ProposalApplications> allProposalApplications()
        {
            Models.PraxisDBEntities db = new Models.PraxisDBEntities();
            List<ProposalApplications> applications = new List<ProposalApplications>();

            var allApplications = from apps in db.Applications
                                  join users in db.Users on apps.userId equals users.id
                                  where apps.proposalId == this.proposalId && apps.userId == this.userId
                                  select new
                                  {

                                      proposalId = apps.proposalId,
                                      userId = apps.userId,
                                      username = users.firstName + " " + users.lastName,
                                      applicationDate = apps.applicationDate,
                                      status = apps.status
                                  };

            if (allApplications.ToList() != null && allApplications.ToList().Count() > 0)
                foreach (var app in allApplications)
                {

                    ProposalApplications proposal = new ProposalApplications();

                    proposal.proposalId = app.proposalId;
                    proposal.userId = app.userId;
                    proposal.username = app.username;
                    proposal.applicationDate = app.applicationDate.Date.ToString("yyyy-MM-dd");
                    proposal.status = new List<SelectListItem>(){

                            new SelectListItem(){Value = Utils.ApplicationStatus.PENDING.ToString() + "," + app.proposalId + "," + app.userId, Text = Utils.ApplicationStatus.PENDING.ToString(), Selected = (app.status.Equals(Utils.ApplicationStatus.PENDING.ToString())? true:false)},
                            new SelectListItem(){Value = Utils.ApplicationStatus.REJECTED.ToString() + "," + app.proposalId + "," + app.userId, Text = Utils.ApplicationStatus.REJECTED.ToString(), Selected = (app.status.Equals(Utils.ApplicationStatus.REJECTED.ToString())? true:false)},
                            new SelectListItem(){Value = Utils.ApplicationStatus.ASSIGNED.ToString() + "," + app.proposalId + "," + app.userId, Text = Utils.ApplicationStatus.ASSIGNED.ToString(), Selected = (app.status.Equals(Utils.ApplicationStatus.ASSIGNED.ToString())? true:false)}
                        };

                    applications.Add(proposal);
                }

            return new PagedList<ProposalApplications>(applications, this.page, this.pageSize);
        }
    }
}