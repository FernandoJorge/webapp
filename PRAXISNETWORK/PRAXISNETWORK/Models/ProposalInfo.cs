﻿using PRAXISNETWORK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRAXISNETWORK.Models
{
    public class ProposalInfo
    {
        public int id { get; set; }
        public String title { get; set; }
        public String studyAreas { get; set; }
        public String studyDegrees { get; set; }
        public String institution { get; set; }
        public String city { get; set; }

        public List<ProposalInfo> AllProposals()
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<ProposalInfo> ListOfProposals = new List<ProposalInfo>();

            var allProposals = from props in db.Proposals
                               join insts in db.Institutions on props.institutionId equals insts.id
                               join cts in db.Cities on props.cityId equals cts.id
                               select new ProposalInfo
                               {

                                   id = props.id,
                                   title = props.title,
                                   institution = insts.name,
                                   city = cts.name

                               };

            ListOfProposals = allProposals.ToList();

            foreach (ProposalInfo info in ListOfProposals)
            {

                IEnumerable<StudyArea> areas = from ars in db.StudyAreas
                                               join propStAreas in db.ProposalStudyAreas on info.id equals propStAreas.proposalId
                                               where ars.id == propStAreas.studyAreaId
                                               select ars;

                IEnumerable<StudyDegree> degrees = from dgrs in db.StudyDegrees
                                                   join propStDegrees in db.ProposalStudyDegrees on info.id equals propStDegrees.proposalId
                                                   where dgrs.id == propStDegrees.studyDegreeId
                                                   select dgrs;

                foreach (StudyArea area in areas)
                    info.studyAreas += area.name + "; ";

                foreach (StudyDegree degree in degrees)
                    info.studyDegrees += degree.name + "; ";
            }

            return ListOfProposals;
        }

        public List<ProposalInfo> AllProposalsSearch(String searchField)
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<ProposalInfo> ListOfProposals = new List<ProposalInfo>();

            var allProposals = from proposals in db.Proposals
                               join cities in db.Cities on proposals.cityId equals cities.id
                               join countries in db.Countries on cities.countryId equals countries.id
                               join institutions in db.Institutions on proposals.institutionId equals institutions.id
                               join hosts in db.Institutions on proposals.hostId equals hosts.id
                               join propAreas in db.ProposalStudyAreas on proposals.id equals propAreas.proposalId
                               join areas in db.StudyAreas on propAreas.studyAreaId equals areas.id
                               join propDegrees in db.ProposalStudyDegrees on proposals.id equals propDegrees.proposalId
                               join degrees in db.StudyDegrees on propDegrees.studyDegreeId equals degrees.id
                               join propTargets in db.ProposalsTargets on proposals.id equals propTargets.proposalId
                               join targets in db.Targets on propTargets.targetId equals targets.id
                               join propLanguages in db.ProposalLanguages on proposals.id equals propLanguages.proposalId
                               join languages in db.Languages on propLanguages.languageId equals languages.id
                               where proposals.title.Contains(searchField) || proposals.description.Contains(searchField) ||
                               proposals.payment.Contains(searchField) || proposals.vacancies.Contains(searchField) ||
                               proposals.globalSkills.Contains(searchField) || proposals.benefits.Contains(searchField) ||
                               proposals.jobRelatedSkills.Contains(searchField) || cities.name.Contains(searchField) ||
                               countries.name.Contains(searchField) || institutions.name.Contains(searchField) ||
                               hosts.name.Contains(searchField) || areas.topic.Contains(searchField) ||
                               degrees.name.Contains(searchField) || targets.name.Contains(searchField) ||
                               languages.name.Contains(searchField)
                               select new ProposalInfo
                               {
                                   id = proposals.id,
                                   city = cities.name,
                                   title = proposals.title,
                                   institution = institutions.name
                               };

            ListOfProposals = allProposals.GroupBy(p => p.id).Select(p => p.FirstOrDefault()).ToList();

            foreach (ProposalInfo info in ListOfProposals)
            {

                IEnumerable<StudyArea> areas = from ars in db.StudyAreas
                                               join propStAreas in db.ProposalStudyAreas on info.id equals propStAreas.proposalId
                                               where ars.id == propStAreas.studyAreaId
                                               select ars;

                IEnumerable<StudyDegree> degrees = from dgrs in db.StudyDegrees
                                                   join propStDegrees in db.ProposalStudyDegrees on info.id equals propStDegrees.proposalId
                                                   where dgrs.id == propStDegrees.studyDegreeId
                                                   select dgrs;

                foreach (StudyArea area in areas)
                    info.studyAreas += area.name + "; ";

                foreach (StudyDegree degree in degrees)
                    info.studyDegrees += degree.name + "; ";
            }

            return ListOfProposals;
        }

        public List<ProposalInfo> AllProposalsFilter(String[] targets, String[] degrees, String[] durations, String country, String area)
        {
            PraxisDBEntities db = new PraxisDBEntities();
            List<ProposalInfo> ListOfProposals = new List<ProposalInfo>();

            int countryId = Convert.ToInt32(country);
            int areaId = Convert.ToInt32(area);

            var allProposals = (from proposals in db.Proposals
                                join cities in db.Cities on proposals.cityId equals cities.id
                                join countries in db.Countries on cities.countryId equals countries.id
                                join institutions in db.Institutions on proposals.institutionId equals institutions.id
                                join propAreas in db.ProposalStudyAreas on proposals.id equals propAreas.proposalId
                                join propDegrees in db.ProposalStudyDegrees on proposals.id equals propDegrees.proposalId
                                join propTargets in db.ProposalsTargets on proposals.id equals propTargets.proposalId
                                where proposals.countryId == countryId || propAreas.studyAreaId == areaId
                                select new
                                {
                                    id = proposals.id,
                                    city = cities.name,
                                    countryId = countries.id,
                                    title = proposals.title,
                                    institution = institutions.name,
                                    degreeId = propDegrees.studyDegreeId,
                                    targetId = propTargets.targetId,
                                    startDate = proposals.startDate,
                                    endDate = proposals.endDate
                                }).ToList();

            List<ProposalInfo> ListProps = new List<ProposalInfo>();

            try
            {
                foreach (String target in targets)
                {
                    int targetId = Convert.ToInt32(target);

                    foreach (var proposal in allProposals)
                    {

                        if (proposal.targetId == targetId)
                        {

                            ListProps.Add(new ProposalInfo
                            {

                                id = proposal.id,
                                city = proposal.city,
                                institution = proposal.institution,
                                title = proposal.title
                            });
                        }
                    }
                }
            }
            catch (NullReferenceException ex)
            {
                // Continue
            }

            try
            {
                foreach (String degree in degrees)
                {
                    int degreeId = Convert.ToInt32(degree);

                    foreach (var proposal in allProposals)
                    {

                        if (proposal.degreeId == degreeId)
                        {

                            ListProps.Add(new ProposalInfo
                            {

                                id = proposal.id,
                                city = proposal.city,
                                institution = proposal.institution,
                                title = proposal.title
                            });
                        }
                    }
                }
            }
            catch (NullReferenceException ex)
            {
                // Continue
            }

            // Durations
            try
            {
                foreach (String duration in durations)
                {

                    int durationId = Convert.ToInt32(duration);

                    foreach (var proposal in allProposals)
                    {
                        int days = proposal.endDate.Subtract(proposal.startDate).Days;

                        if ((durationId == 0 && days < 30) ||
                            (durationId == 1 && days >= 30 && days < 90) ||
                            (durationId == 2 && days >= 90 && days < 180) ||
                            (durationId == 3 && days > 180))
                        {
                            ListProps.Add(new ProposalInfo
                            {

                                id = proposal.id,
                                city = proposal.city,
                                institution = proposal.institution,
                                title = proposal.title
                            });
                        }
                    }
                }
            }
            catch (NullReferenceException ex)
            {
                // Continue
            }

            if (allProposals.Count() > 0 && ListProps.Count() == 0)
            {
                foreach (var proposal in allProposals)
                {
                    ListProps.Add(new ProposalInfo
                    {

                        id = proposal.id,
                        city = proposal.city,
                        institution = proposal.institution,
                        title = proposal.title
                    });
                }
            }

            ListOfProposals = ListProps.GroupBy(p => p.id).Select(p => p.FirstOrDefault()).ToList();

            foreach (ProposalInfo info in ListOfProposals)
            {

                IEnumerable<StudyArea> allAreas = from ars in db.StudyAreas
                                                  join propStAreas in db.ProposalStudyAreas on info.id equals propStAreas.proposalId
                                                  where ars.id == propStAreas.studyAreaId
                                                  select ars;

                IEnumerable<StudyDegree> allDegrees = from dgrs in db.StudyDegrees
                                                      join propStDegrees in db.ProposalStudyDegrees on info.id equals propStDegrees.proposalId
                                                      where dgrs.id == propStDegrees.studyDegreeId
                                                      select dgrs;

                foreach (StudyArea currentArea in allAreas)
                    info.studyAreas += currentArea.name + "; ";

                foreach (StudyDegree currentDegree in allDegrees)
                    info.studyDegrees += currentDegree.name + "; ";
            }

            return ListOfProposals;
        }
    }
}