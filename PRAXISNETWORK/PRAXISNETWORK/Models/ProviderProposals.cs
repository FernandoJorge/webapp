﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRAXISNETWORK.Models
{
    public class ProviderProposals
    {
        public int userId { get; set; }

        public int page { get; set; }

        public int pageSize { get; set; }

        public int id { get; set; }

        public String title { get; set; }

        public String startDate { get; set; }

        public String endDate { get; set; }

        public String submitDate { get; set; }

        public int numberOfApplications { get; set; }

        public PagedList<ProviderProposals> allProviderProposals()
        {
            Models.PraxisDBEntities db = new Models.PraxisDBEntities();
            List<ProviderProposals> prProps = new List<ProviderProposals>();

            var allProposals = from props in db.Proposals
                               where props.providerId == this.userId
                               orderby props.submitDate descending
                               select new
                               {
                                   id = props.id,
                                   title = props.title,
                                   startDate = props.startDate,
                                   endDate = props.endDate,
                                   submitDate = props.submitDate
                               };

            int count = allProposals.Count();

            if (allProposals != null && allProposals.Count() > 0)
                foreach (var prop in allProposals)
                {

                    IEnumerable<Application> allApps = from apps in db.Applications
                                                       where apps.proposalId == prop.id
                                                       select apps;

                    ProviderProposals proposal = new ProviderProposals();

                    proposal.id = prop.id;
                    proposal.title = prop.title;
                    proposal.startDate = prop.startDate.Date.ToString("yyyy-MM-dd");
                    proposal.endDate = prop.endDate.Date.ToString("yyyy-MM-dd");
                    proposal.submitDate = prop.submitDate.Date.ToString("yyyy-MM-dd");

                    try
                    {
                        proposal.numberOfApplications = allApps.Count();
                    }
                    catch (NullReferenceException ex)
                    {
                        // Continue
                    }

                    prProps.Add(proposal);
                }

            return new PagedList<ProviderProposals>(prProps, this.page, this.pageSize);

        }
    }
}