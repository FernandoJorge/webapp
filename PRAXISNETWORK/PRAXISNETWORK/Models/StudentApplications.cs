﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRAXISNETWORK.Models
{
    public class StudentApplications
    {
        public int userId { get; set; }

        public int page { get; set; }

        public int pageSize { get; set; }

        public int id { get; set; }

        public String title { get; set; }

        public String startDate { get; set; }

        public String endDate { get; set; }

        public String submitDate { get; set; }

        public String country { get; set; }

        public String city { get; set; }

        public String status { get; set; }

        public PagedList<Models.StudentApplications> allStudentApplications()
        {
            Models.PraxisDBEntities db = new Models.PraxisDBEntities();
            List<StudentApplications> stApps = new List<StudentApplications>();

            var allApplications = from props in db.Proposals
                                  join cts in db.Cities on props.cityId equals cts.id
                                  join ctrs in db.Countries on cts.countryId equals ctrs.id
                                  join apps in db.Applications on props.id equals apps.proposalId
                                  where apps.userId == this.userId
                                  orderby apps.applicationDate descending
                                  select new
                                  {
                                      id = props.id,
                                      title = props.title,
                                      startDate = props.startDate,
                                      endDate = props.endDate,
                                      submitDate = apps.applicationDate,
                                      status = apps.status,
                                      city = cts.name,
                                      country = ctrs.name
                                  };

            if (allApplications != null && allApplications.Count() > 0)
                foreach (var prop in allApplications)
                {

                    stApps.Add(new StudentApplications()
                    {

                        id = prop.id,
                        title = prop.title,
                        startDate = prop.startDate.Date.ToString("yyyy-MM-dd"),
                        endDate = prop.endDate.Date.ToString("yyyy-MM-dd"),
                        submitDate = prop.submitDate.Date.ToString("yyyy-MM-dd"),
                        status = prop.status,
                        city = prop.city,
                        country = prop.country
                    });
                }

            return new PagedList<StudentApplications>(stApps, this.page, this.pageSize);

        }
    }
}