﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;

namespace PRAXISNETWORK.Models
{
    public static class Utils
    {
        const string passphrase = "Password@123";  //constant string Pass key

        public enum FileFormats
        {
            ALL, ONLY_IMAGES, ONLY_DOCS
        };

        public enum ApplicationStatus {
        
            PENDING, REJECTED, ASSIGNED
        }

        public static string encrypt(string message)
        {
            byte[] results;
            UTF8Encoding utf8 = new UTF8Encoding();
            //to create the object for UTF8Encoding  class
            //TO create the object for MD5CryptoServiceProvider 
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] deskey = md5.ComputeHash(utf8.GetBytes(passphrase));
            //to convert to binary passkey
            //TO create the object for  TripleDESCryptoServiceProvider 
            TripleDESCryptoServiceProvider desalg = new TripleDESCryptoServiceProvider();
            desalg.Key = deskey;//to  pass encode key
            desalg.Mode = CipherMode.ECB;
            desalg.Padding = PaddingMode.PKCS7;
            byte[] encrypt_data = utf8.GetBytes(message);
            //to convert the string to utf encoding binary 

            try
            {


                //To transform the utf binary code to md5 encrypt    
                ICryptoTransform encryptor = desalg.CreateEncryptor();
                results = encryptor.TransformFinalBlock(encrypt_data, 0, encrypt_data.Length);

            }
            finally
            {
                //to clear the allocated memory
                desalg.Clear();
                md5.Clear();
            }
            //to convert to 64 bit string from converted md5 algorithm binary code
            return Convert.ToBase64String(results);

        }

        public static string decrypt(string message)
        {
            byte[] results;
            UTF8Encoding utf8 = new UTF8Encoding();
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] deskey = md5.ComputeHash(utf8.GetBytes(passphrase));
            TripleDESCryptoServiceProvider desalg = new TripleDESCryptoServiceProvider();
            desalg.Key = deskey;
            desalg.Mode = CipherMode.ECB;
            desalg.Padding = PaddingMode.PKCS7;
            byte[] decrypt_data = Convert.FromBase64String(message);
            try
            {
                //To transform the utf binary code to md5 decrypt
                ICryptoTransform decryptor = desalg.CreateDecryptor();
                results = decryptor.TransformFinalBlock(decrypt_data, 0, decrypt_data.Length);
            }
            finally
            {
                desalg.Clear();
                md5.Clear();

            }
            //TO convert decrypted binery code to string
            return utf8.GetString(results);
        }

        public static List<String> uploadFileFormats(FileFormats fileFormat)
        {
            List<String> permitedExtensions = null;

            switch (fileFormat)
            {

                case FileFormats.ALL:
                    permitedExtensions = new List<String>()
                                {
                                    "bmp", "gif", "jpg", "jpeg", "png", "psd", "pspimage", "thm", "tif", "yuv",
                                    "pdf", "txt"
                                };
                    break;

                case FileFormats.ONLY_DOCS:
                    permitedExtensions = new List<String>()
                                {
                                    "pdf", "txt"
                                };

                    break;

                case FileFormats.ONLY_IMAGES:
                    permitedExtensions = new List<String>()
                                {
                                    "bmp", "gif", "jpg", "jpeg", "png", "psd", "pspimage", "thm", "tif", "yuv",
                                };
                    break;
            }

            return permitedExtensions;
        }

        public static Boolean sendEmailMessage(List<String> emailAddresses, String emailSubject, String emailBody)
        {
            PraxisDBEntities db = new PraxisDBEntities();

            IEnumerable<EmailService> emailServices = from services in db.EmailServices
                                                      select services;

            if (emailServices.Count() == 0)
                return false;

            try
            {

                SmtpClient smtpServer = new SmtpClient
                {

                    Host = emailServices.FirstOrDefault().smtpServer,
                    Port = emailServices.FirstOrDefault().smtpPort,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new System.Net.NetworkCredential(emailServices.FirstOrDefault().username, emailServices.FirstOrDefault().password),
                    EnableSsl = true,
                    UseDefaultCredentials = false
                };

                MailMessage emailMessage = new MailMessage();
                emailMessage.From = new MailAddress(emailServices.FirstOrDefault().fromEmail);

                foreach (String address in emailAddresses)
                    emailMessage.To.Add(new MailAddress(address));

                emailMessage.Subject = emailSubject;
                emailMessage.Body = emailBody;

                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                smtpServer.Send(emailMessage);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public static void writeLog(String log)
        {
            StreamWriter strW = File.AppendText(HttpContext.Current.Server.MapPath("~/Files/Application/Logs.txt"));
            strW.WriteLine("----------------------------------------------------------------------------------------------");
            strW.WriteLine("---------------------------------LOG START----------------------------------------------------");
            strW.WriteLine("----------------------------------------------------------------------------------------------");
            strW.WriteLine("[" + DateTime.Now.ToString("yyyy-MM-dd") + "]");
            strW.WriteLine(log);
            strW.WriteLine("----------------------------------------------------------------------------------------------");
            strW.WriteLine("---------------------------------LOG END------------------------------------------------------");
            strW.WriteLine("----------------------------------------------------------------------------------------------");
            strW.WriteLine();
            strW.WriteLine();
            strW.Close();
        }
    }
}