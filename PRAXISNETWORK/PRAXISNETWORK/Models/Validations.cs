﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRAXISNETWORK.Models
{
    public class UserEmailExistsAttribute : ValidationAttribute, IClientValidatable
    {
        String userEmail;
        int userType;

        public UserEmailExistsAttribute(int userType)
        {
            this.userType = userType;
        }

        // MODEL STATE VALIDATION
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                PraxisDBEntities db = new PraxisDBEntities();
                String email = value.ToString();

                IEnumerable<User> usersTypes = from users in db.Users
                                               where users.userTypeId == userType && users.email == email
                                               select users;
                if (usersTypes.Count() > 0)
                {
                    userEmail = usersTypes.FirstOrDefault().email;

                    return new ValidationResult(ErrorMessage);
                }
            }

            return ValidationResult.Success;
        }

        // CLIENT SIDE VALIDATION
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("useremail", userEmail);
            rule.ValidationType = "useremailexists";

            yield return rule;
        }
    }

    public class SubscriberExistsAttribute : ValidationAttribute, IClientValidatable
    {
        String userEmail = null;

        // MODEL STATE VALIDATION
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                PraxisDBEntities db = new PraxisDBEntities();
                String email = value.ToString();

                IEnumerable<Subscriber> subscribers = from subs in db.Subscribers
                                                      where subs.email == email
                                                      select subs;
                if (subscribers.Count() > 0)
                {
                    userEmail = subscribers.FirstOrDefault().email;

                    return new ValidationResult(ErrorMessage);
                }
            }

            return ValidationResult.Success;
        }

        // CLIENT SIDE VALIDATION
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("useremail", userEmail);
            rule.ValidationType = "subscriberexists";

            yield return rule;
        }
    }

    public class UsernameExistsAttribute : ValidationAttribute, IClientValidatable
    {
        String userName;
        int userType;

        public UsernameExistsAttribute(int userType)
        {
            this.userType = userType;
        }

        // MODEL STATE VALIDATION
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                PraxisDBEntities db = new PraxisDBEntities();
                String userN = value.ToString();

                IEnumerable<User> usersTypes = from users in db.Users
                                               where users.userTypeId == userType && users.username == userN
                                               select users;
                if (usersTypes.Count() > 0)
                {
                    userName = usersTypes.FirstOrDefault().username;

                    return new ValidationResult(ErrorMessage);
                }
            }

            return ValidationResult.Success;
        }

        // CLIENT SIDE VALIDATION
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("username", userName);
            rule.ValidationType = "usernameexists";

            yield return rule;
        }
    }

    public class ValidStartDateAttribute : ValidationAttribute, IClientValidatable
    {
        String result = "valid";

        // MODEL STATE VALIDATION
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime startDate = (DateTime)value;

                // Test if start date is greater than actual date
                if (DateTime.Compare(startDate, DateTime.Now) <= 0)
                {

                    result = "invalid";
                    return new ValidationResult(ErrorMessage);
                }

            }

            return ValidationResult.Success;
        }

        // CLIENT SIDE VALIDATION
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("result", result);
            rule.ValidationType = "startdate";

            yield return rule;
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ValidEndDateAttribute : ValidationAttribute, IClientValidatable
    {

        String startDate;
        String result = "valid";

        public ValidEndDateAttribute(String startDate)
        {
            this.startDate = startDate;
        }

        // MODEL STATE VALIDATION
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime endDate = (DateTime)value;

                DateTime startDateProperty = (DateTime)validationContext.ObjectInstance.GetType().GetProperty(startDate).GetValue(validationContext.ObjectInstance, null);

                // Test if start date is greater than actual date
                if (DateTime.Compare(startDateProperty, endDate) >= 0)
                {

                    result = "invalid";
                    return new ValidationResult(ErrorMessage);
                }

            }

            return ValidationResult.Success;
        }

        // CLIENT SIDE VALIDATION
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("result", result);
            rule.ValidationType = "enddate";

            yield return rule;
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ValidUntilDateAttribute : ValidationAttribute, IClientValidatable
    {
        String startDate;
        String result = "valid";

        public ValidUntilDateAttribute(String startDate)
        {
            this.startDate = startDate;
        }

        // MODEL STATE VALIDATION
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime ValidDate = (DateTime)value;

                DateTime startDateProperty = (DateTime)validationContext.ObjectInstance.GetType().GetProperty(startDate).GetValue(validationContext.ObjectInstance, null);

                // Test if start date is greater than actual date
                if (DateTime.Compare(ValidDate, DateTime.Now) <= 0)
                {

                    result = "invalid";
                    return new ValidationResult(ErrorMessage + "The validation date must be grater than the actual date");
                }

                if (DateTime.Compare(startDateProperty, ValidDate) <= 0)
                {

                    result = "invalid";
                    return new ValidationResult(ErrorMessage + "The validation date must be earlier than start date");
                }

            }

            return ValidationResult.Success;
        }

        // CLIENT SIDE VALIDATION
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("result", result);
            rule.ValidationType = "untildate";

            yield return rule;
        }
    }

    public class SingleUploadFormatAttribute : ValidationAttribute, IClientValidatable
    {
        String permission = null;
        List<String> permitedExtensions = Utils.uploadFileFormats(Utils.FileFormats.ONLY_IMAGES);

        // MODEL STATE VALIDATION
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                HttpPostedFileBase file = (HttpPostedFileBase)value;

                if (file.ContentLength > 0)
                {
                    String fileNameExt = Path.GetFileName(file.FileName).Split('.').Last();

                    foreach (String ext in permitedExtensions)
                    {

                        if (ext.Equals(fileNameExt, StringComparison.OrdinalIgnoreCase))
                        {
                            permission = "ack";
                            return ValidationResult.Success;
                        }
                    }
                }
            }
            else
            {
                return ValidationResult.Success;
            }

            permission = "nack";
            return new ValidationResult(ErrorMessage);
        }

        // CLIENT SIDE VALIDATION
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("permission", permission);
            rule.ValidationType = "singleuploadformat";

            yield return rule;
        }
    }

    public class MultipleUploadFormatAttribute : ValidationAttribute, IClientValidatable
    {
        String permission = null;
        List<String> permitedExtensions = Utils.uploadFileFormats(Utils.FileFormats.ALL);

        // MODEL STATE VALIDATION
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            List<Boolean> allExists = new List<Boolean>();

            if (value != null)
            {
                IEnumerable<HttpPostedFileBase> files = (IEnumerable<HttpPostedFileBase>)value;

                foreach (HttpPostedFileBase file in files)
                {

                    if (file.ContentLength > 0)
                    {
                        String fileNameExt = Path.GetFileName(file.FileName).Split('.').Last();

                        if (permitedExtensions.Contains(fileNameExt.ToLower()))
                            allExists.Add(true);
                        else allExists.Add(false);
                    }
                }
            }
            else
            {
                return ValidationResult.Success;
            }

            if (allExists.Contains(false))
            {

                permission = "nack";
                return new ValidationResult(ErrorMessage);

            }

            permission = "ack";
            return ValidationResult.Success;
        }

        // CLIENT SIDE VALIDATION
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("permission", permission);
            rule.ValidationType = "multipleuploadformat";

            yield return rule;
        }
    }
}