﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace PRAXISNETWORK
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PraxisWebService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PraxisWebService.svc or PraxisWebService.svc.cs at the Solution Explorer and start debugging.
    //[AspNetCompatibilityRequirements(RequirementsMode=AspNetCompatibilityRequirementsMode.Required)]
    public class PraxisWebService : IPraxisWebService
    {
        /// <summary>
        /// Retrieves all offes praxis offers
        /// </summary>
        /// <param name="startDate">Offer´s start date - Optional parameter</param>
        /// <param name="endDate">Offer´s end date - Optional parameter</param>
        /// <returns>Xml document with all offer´s information</returns>
        public String praxisOffers(String startDate, String endDate)
        {
            XDocument xmlAllProposals = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"));
            String dateRegex = @"\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])";

            if ((!Regex.IsMatch(startDate, dateRegex, RegexOptions.IgnoreCase) || (!Regex.IsMatch(endDate, dateRegex, RegexOptions.IgnoreCase))))
            {

                xmlAllProposals.Add(new XElement("error", "dates must follow the correct format: Ex: 2012-03-17"));

                return xmlAllProposals.ToString();
            }

            Models.PraxisDBEntities db = new Models.PraxisDBEntities();

            List<ProposalInfo> proposals = null;

            if (String.IsNullOrEmpty(startDate) && String.IsNullOrEmpty(endDate))
            {
                proposals = (from props in db.Proposals
                             join cities in db.Cities on props.cityId equals cities.id
                             join countries in db.Countries on cities.countryId equals countries.id
                             join institituions in db.Institutions on props.institutionId equals institituions.id
                             join hosts in db.Institutions on props.hostId equals hosts.id
                             join providers in db.Users on props.providerId equals providers.id
                             into info
                             from p in info.DefaultIfEmpty()
                             select new ProposalInfo()
                             {
                                 id = props.id,
                                 title = props.title,
                                 description = props.description,
                                 city = cities.name,
                                 country = countries.name,
                                 institution = institituions.name,
                                 host = hosts.name,
                                 startDate = props.startDate.Date.ToString(),
                                 endDate = props.endDate.Date.ToString(),
                                 validTo = props.validTo.Date.ToString(),
                                 supervisorFirstName = props.supervisorFirstName,
                                 superVisorLastName = props.supervisorLastName,
                                 superVisorEmail = props.supervisorEmail,
                                 payment = props.payment,
                                 vacancies = props.vacancies,
                                 globalSkills = props.globalSkills,
                                 benefits = props.benefits,
                                 contact = props.contact
                             }).ToList();
            }
            else if (!String.IsNullOrEmpty(startDate) && String.IsNullOrEmpty(endDate))
            {

                proposals = (from props in db.Proposals
                             join cities in db.Cities on props.cityId equals cities.id
                             join countries in db.Countries on cities.countryId equals countries.id
                             join institituions in db.Institutions on props.institutionId equals institituions.id
                             join hosts in db.Institutions on props.hostId equals hosts.id
                             join providers in db.Users on props.providerId equals providers.id
                             into info
                             from p in info.DefaultIfEmpty()
                             select new ProposalInfo()
                             {
                                 id = props.id,
                                 title = props.title,
                                 description = props.description,
                                 city = cities.name,
                                 country = countries.name,
                                 institution = institituions.name,
                                 host = hosts.name,
                                 startDate = props.startDate.Date.ToString(),
                                 endDate = props.endDate.Date.ToString(),
                                 validTo = props.validTo.Date.ToString(),
                                 supervisorFirstName = props.supervisorFirstName,
                                 superVisorLastName = props.supervisorLastName,
                                 superVisorEmail = props.supervisorEmail,
                                 payment = props.payment,
                                 vacancies = props.vacancies,
                                 globalSkills = props.globalSkills,
                                 benefits = props.benefits,
                                 contact = props.contact
                             }).Where(p => p.startDate.CompareTo(startDate) >= 0).ToList();
            }
            else if (!String.IsNullOrEmpty(startDate) && !String.IsNullOrEmpty(endDate))
            {

                proposals = (from props in db.Proposals
                             join cities in db.Cities on props.cityId equals cities.id
                             join countries in db.Countries on cities.countryId equals countries.id
                             join institituions in db.Institutions on props.institutionId equals institituions.id
                             join hosts in db.Institutions on props.hostId equals hosts.id
                             join providers in db.Users on props.providerId equals providers.id
                             into info
                             from p in info.DefaultIfEmpty()
                             select new ProposalInfo()
                             {
                                 id = props.id,
                                 title = props.title,
                                 description = props.description,
                                 city = cities.name,
                                 country = countries.name,
                                 institution = institituions.name,
                                 host = hosts.name,
                                 startDate = props.startDate.Date.ToString(),
                                 endDate = props.endDate.Date.ToString(),
                                 validTo = props.validTo.Date.ToString(),
                                 supervisorFirstName = props.supervisorFirstName,
                                 superVisorLastName = props.supervisorLastName,
                                 superVisorEmail = props.supervisorEmail,
                                 payment = props.payment,
                                 vacancies = props.vacancies,
                                 globalSkills = props.globalSkills,
                                 benefits = props.benefits,
                                 contact = props.contact
                             }).Where(p => p.startDate.CompareTo(startDate) >= 0 && p.endDate.CompareTo(endDate) <= 0).ToList();
            }

            if (proposals != null && proposals.Count() > 0)
            {
                XElement xmlProposals = new XElement("proposals");

                foreach (ProposalInfo proposal in proposals)
                {
                    IEnumerable<Models.Target> allTargets = from targets in db.Targets
                                                            join propTargets in db.ProposalsTargets on targets.id equals propTargets.targetId
                                                            where propTargets.proposalId == proposal.id
                                                            select targets;

                    IEnumerable<Models.StudyDegree> allDegrees = from degrees in db.StudyDegrees
                                                                 join propDegrees in db.ProposalStudyDegrees on degrees.id equals propDegrees.studyDegreeId
                                                                 where propDegrees.proposalId == proposal.id
                                                                 select degrees;

                    IEnumerable<Models.StudyArea> allAreas = from areas in db.StudyAreas
                                                             join propAreas in db.ProposalStudyAreas on areas.id equals propAreas.studyAreaId
                                                             where propAreas.proposalId == proposal.id
                                                             select areas;

                    IEnumerable<Models.Language> allLanguages = from languages in db.Languages
                                                                join propLanguages in db.ProposalLanguages on languages.id equals propLanguages.languageId
                                                                where propLanguages.proposalId == proposal.id
                                                                select languages;

                    IEnumerable<Models.Skill> allPromotedSkills = from skills in db.Skills
                                                                  join promotedSkills in db.PromotedSkills on skills.id equals promotedSkills.skillId
                                                                  where promotedSkills.proposalId == proposal.id
                                                                  select skills;

                    IEnumerable<Models.Skill> allRequiredSkills = from skills in db.Skills
                                                                  join requiredSkills in db.RequiredSkills on skills.id equals requiredSkills.skillId
                                                                  where requiredSkills.proposalId == proposal.id
                                                                  select skills;

                    XElement xmlTargets = new XElement("targets");
                    XElement xmlDegrees = new XElement("study_degrees");
                    XElement xmlAreas = new XElement("study_areas");
                    XElement xmlLanguages = new XElement("languages");
                    XElement xmlPromotedSkills = new XElement("skills_to_promote");
                    XElement xmlRequiredSkills = new XElement("required_skills");


                    if (allTargets != null && allTargets.Count() > 0)
                        foreach (Models.Target target in allTargets)
                            xmlTargets.Add(new XElement("target", target.name));

                    if (allDegrees != null && allDegrees.Count() > 0)
                        foreach (Models.StudyDegree degree in allDegrees)
                            xmlDegrees.Add(new XElement("study_degree"), degree.name);

                    if (allAreas != null && allAreas.Count() > 0)
                        foreach (Models.StudyArea area in allAreas)
                            xmlAreas.Add(new XElement("study_area"), area.name);

                    if (allLanguages != null && allLanguages.Count() > 0)
                        foreach (Models.Language language in allLanguages)
                            xmlLanguages.Add(new XElement("language"), language.name);

                    if (allPromotedSkills != null && allPromotedSkills.Count() > 0)
                        foreach (Models.Skill skill in allPromotedSkills)
                            xmlPromotedSkills.Add(new XElement("skill_to_promote"), skill.name);

                    if (allRequiredSkills != null && allRequiredSkills.Count() > 0)
                        foreach (Models.Skill skill in allRequiredSkills)
                            xmlRequiredSkills.Add(new XElement("required_skill"), skill.name);

                    XElement xmlProposal = new XElement("proposal",
                                            new XElement("proposal_id", Convert.ToString(proposal.id)),
                                            new XElement("title", proposal.title),
                                            new XElement("description", proposal.description),
                                            new XElement("city", proposal.city),
                                            new XElement("country", proposal.country),
                                            new XElement("institution", proposal.institution),
                                            new XElement("host", proposal.host),
                                            new XElement("start_date", proposal.startDate),
                                            new XElement("end_date", proposal.endDate),
                                            new XElement("validTo", proposal.validTo),
                                            new XElement("payment", proposal.payment),
                                            new XElement("vacancies", proposal.vacancies),
                                            new XElement("global_skills", proposal.globalSkills),
                                            new XElement("job_related_skills", proposal.jobRelatedSkills),
                                            new XElement("benefits", proposal.benefits),
                                            new XElement("contact", proposal.contact),
                                            new XElement("supervisor_firstname", proposal.supervisorFirstName),
                                            new XElement("supervisor_lastname", proposal.superVisorLastName),
                                            new XElement("supervisor_email", proposal.superVisorEmail),
                                            xmlTargets,
                                            xmlDegrees,
                                            xmlAreas,
                                            xmlLanguages,
                                            xmlPromotedSkills,
                                            xmlRequiredSkills);

                    xmlProposals.Add(xmlProposal);
                }

                xmlAllProposals.Add(xmlProposals);
            }
            else
            {
                xmlAllProposals.Add(new XElement("proposals", "There is no proposals"));

                return xmlAllProposals.ToString();
            }

            return xmlAllProposals.ToString();
        }

        public class ProposalInfo
        {
            public int id;
            public String title;
            public String description;
            public String city;
            public String country;
            public String institution;
            public String host;
            public String startDate;
            public String endDate;
            public String validTo;
            public String payment;
            public String vacancies;
            public String globalSkills;
            public String jobRelatedSkills;
            public String benefits;
            public String contact;
            public String supervisorFirstName;
            public String superVisorLastName;
            public String superVisorEmail;
        }
    }
}
