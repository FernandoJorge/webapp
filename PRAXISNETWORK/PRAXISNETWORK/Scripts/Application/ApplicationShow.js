﻿$(document).ready(function () {

    $(".fancybox_image").fancybox({

        fitToView: true,
        openEffect: 'elastic',
        closeButton: true,
        overlayColor: '##99CCFF',
        modal: false,
    });

    $(".fancybox_doc").fancybox({

        fitToView: true,
        openEffect: 'elastic',
        closeButton: true,
        overlayColor: '##99CCFF',
        modal: false,
        type: 'iframe',
        iframe: {
            preload: false // fixes issue with iframe and IE
        }
    });
});