﻿$.validator.unobtrusive.adapters.addSingleVal("multipleuploadformat", "permission");

$.validator.addMethod("multipleuploadformat", function (value, element, parameters) {

    if (parameters["permission"] == "nack") {
        return false;
    }

    return true;
});

$(document).ready(function () {

    var numberOfFiles = 1;
    var maxFiles = 5;

    $("#newDoc").on("click", function () {

        var docsDiv = $("#files");

        if (numberOfFiles < maxFiles) {

            var inputHtml = "<div id='" + numberOfFiles + "'><br /><input type='file' name='docsUploaded' id='docsUploaded' /></div>";

            docsDiv.append(inputHtml);
            numberOfFiles++;

            $("#removeDoc").show();

            if (numberOfFiles == maxFiles)
                $("#newDoc").hide();
        }

    });

    $("#removeDoc").on("click", function () {

        if (numberOfFiles > 1) {

            $("#" + (numberOfFiles - 1)).remove();
            numberOfFiles--;
        }

        if (numberOfFiles == 1)
            $("#removeDoc").hide();

        if (numberOfFiles < maxFiles)
            $("#newDoc").show();

    });
});