﻿$.validator.unobtrusive.adapters.addSingleVal("singleuploadformat", "permission");

$.validator.addMethod("singleuploadformat", function (value, element, parameters) {

    if (parameters["permission"] == "nack") {
        return false;
    }

    return true;
});

$(document).ready(function () {

    var formatFiles = ["bmp", "gif", "jpg", "jpeg", "png", "psd", "pspimage", "thm", "tif", "yuv", "pdf", "txt"];

    $("#fileUpload").prop("disabled", true);

    $(".fancybox_image").fancybox({

        fitToView: true,
        openEffect: 'elastic',
        closeButton: true,
        overlayColor: '##99CCFF',
        modal: false,
    });

    $(".fancybox_doc").fancybox({

        fitToView: true,
        openEffect: 'elastic',
        closeButton: true,
        overlayColor: '##99CCFF',
        modal: false,
        type: 'iframe',
        iframe: {
            preload: false // fixes issue with iframe and IE
        }
    });

    $("#docsUploaded").on("change", function () {

        var path = $("#docsUploaded").val().toLowerCase();
        var exists = false;

        if (path != '') {

            var pathArray = path.split('.');
            var ext = pathArray[pathArray.length - 1];

            formatFiles.forEach(function (format) {

                if (ext == format)
                    exists = true;
            });
        }

        if (!exists) {

            $("#docsUploaded").val('');
            $("#errorMessage").empty();
            $("#errorMessage").html("<text>Prohibited file format. Only images or pdf or txt files.</text>");
            $("#fileUpload").prop("disabled", true);
        } else {

            $("#errorMessage").empty();
            $("#fileUpload").prop("disabled", false);
        }
    });
});