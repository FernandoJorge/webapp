﻿$(document).ready(function () {

    $(".fancybox_image").fancybox({

        fitToView: true,
        openEffect: 'elastic',
        closeButton: true,
        overlayColor: '##99CCFF',
        modal: false,
    });
});