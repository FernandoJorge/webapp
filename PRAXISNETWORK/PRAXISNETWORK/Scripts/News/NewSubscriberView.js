﻿$.validator.unobtrusive.adapters.addSingleVal("subscriberexists", "useremail");

$.validator.addMethod("useremailexists", function (value, element, parameters) {

    if (value && value == parameters["useremail"]) {
        return false;
    }

    return true;
});