﻿$.validator.unobtrusive.adapters.addSingleVal("startdate", "result");
$.validator.unobtrusive.adapters.addSingleVal("enddate", "result");
$.validator.unobtrusive.adapters.addSingleVal("untildate", "result");

$.validator.addMethod("startdate", function (value, element, parameters) {

    if (value && parameters["result"] == "invalid") {
        return false;
    }

    return true;
});

$.validator.addMethod("enddate", function (value, element, parameters) {

    if (value && parameters["result"] == "invalid") {
        return false;
    }

    return true;
});

$.validator.addMethod("untildate", function (value, element, parameters) {

    if (value && parameters["result"] == "invalid") {
        return false;
    }

    return true;
});

function getInstitutionsOfCity(cityIdCountryId) {

    $.ajax({

        url: "InstitutionsOfCity",
        type: "GET",
        data: { cityIdCountryId: cityIdCountryId },
        success: function (response) {

            $("#institution").empty();

            for (var inst in response["institutions"]) {

                debugger;

                var value = inst;
                var text = response["institutions"][inst];
                $("#institution").append($("<option></option>").attr("value", value).text(text));
            }

            sortSelect("#institution", "text", "asc");

        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);
            alert("Error comunicating with server...");
        }
    });
}

var sortSelect = function (select, attr, order) {

    if (attr === 'text') {

        if (order === 'asc') {
            $(select).html($(select).children('option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
            e.preventDefault();
        }// end asc

        if (order === 'desc') {
            $(select).html($(select).children('option').sort(function (y, x) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
            e.preventDefault();
        }// end desc
    }

};

$(document).ready(function () {

    var cityIdCountryId = $("#city").val();

    getInstitutionsOfCity(cityIdCountryId);

    $("#city").on("change", function () {

        var cityIdCountryId = $("#city").val();

        getInstitutionsOfCity(cityIdCountryId);
    });
});