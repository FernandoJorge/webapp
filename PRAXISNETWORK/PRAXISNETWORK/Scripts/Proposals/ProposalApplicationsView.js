﻿function ChangeApplicationStatus(dropList) {

    var info = $("#drop_" + dropList).val();

    $.ajax({

        url: "/Application/ChangeApplicationStatus",
        type: "GET",
        data: { info: info },
        success: function (response) {

            $("#applications").empty();
            $("#applications").html(response);

        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);
            alert("Error comunicating with server...");
        }
    });
}