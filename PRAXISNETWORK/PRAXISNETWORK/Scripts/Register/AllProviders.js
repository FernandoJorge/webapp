﻿function ChangeLockedStatus(dropList) {

    var info = $("#dropLocked_" + dropList).val();

    $.ajax({

        url: "/Register/ChangeLockedStatus",
        type: "GET",
        data: { info: info + ",Provider" },
        success: function (response) {

            $("#providers").empty();
            $("#providers").html(response);

        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);
            alert("Error comunicating with server...");
        }
    });
}

function ChangeEnabledStatus(dropList) {

    var info = $("#dropEnabled_" + dropList).val();

    $.ajax({

        url: "/Register/ChangeEnabledStatus",
        type: "GET",
        data: { info: info + ",Provider" },
        success: function (response) {

            $("#providers").empty();
            $("#providers").html(response);

        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);
            alert("Error comunicating with server...");
        }
    });
}

function updateProviderId(providerId) {

    debugger;
    this.providerId = providerId;
}

function deleteProviderConfirmation() {

    $.ajax({

        url: "/Register/DeleteProfileByAdmin",
        type: "GET",
        data: { userID: this.providerId, userType: 1 },
        success: function (response) {

            $("#providers").empty();
            $("#providers").html(response);

        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);
            alert("Error comunicating with server...");
        }
    });
}

$(document).ready(function () {

    var providerId;
});