﻿function ChangeLockedStatus(dropList) {

    var info = $("#dropLocked_" + dropList).val();

    $.ajax({

        url: "/Register/ChangeLockedStatus",
        type: "GET",
        data: { info: info + ",Student" },
        success: function (response) {

            $("#students").empty();
            $("#students").html(response);

        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);
            alert("Error comunicating with server...");
        }
    });
}

function ChangeEnabledStatus(dropList) {

    var info = $("#dropEnabled_" + dropList).val();

    $.ajax({

        url: "/Register/ChangeEnabledStatus",
        type: "GET",
        data: { info: info + ",Student" },
        success: function (response) {

            $("#students").empty();
            $("#students").html(response);

        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);
            alert("Error comunicating with server...");
        }
    });
}

function updateStudentId(studentId) {

    this.studentId = studentId;
}

function deleteStudentConfirmation() {

    $.ajax({

        url: "/Register/DeleteProfileByAdmin",
        type: "GET",
        data: { userID: studentId, userType: 2 },
        success: function (response) {

            $("#students").empty();
            $("#students").html(response);

        },
        error: function (xhr, status, error) {

            console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);
            alert("Error comunicating with server...");
        }
    });
}

$(document).ready(function () {

    var studentId;
});