﻿$.validator.unobtrusive.adapters.addSingleVal("useremailexists", "useremail");
$.validator.unobtrusive.adapters.addSingleVal("usernameexists", "username");
$.validator.unobtrusive.adapters.addSingleVal("singleuploadformat", "permission");

$.validator.addMethod("singleuploadformat", function (value, element, parameters) {

    if (parameters["permission"] == "nack") {
        return false;
    }

    return true;
});

$.validator.addMethod("useremailexists", function (value, element, parameters) {

    if (value && value == parameters["useremail"]) {
        return false;
    }

    return true;
});

$.validator.addMethod("usernameexists", function (value, element, parameters) {

    if (value && value == parameters["username"]) {
        return false;
    }

    return true;
});

var sortSelect = function (select, attr, order) {

    if (attr === 'text') {

        if (order === 'asc') {
            $(select).html($(select).children('option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
            e.preventDefault();
        }// end asc

        if (order === 'desc') {
            $(select).html($(select).children('option').sort(function (y, x) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
            e.preventDefault();
        }// end desc
    }

};

$(document).ready(function () {

    $(".fancybox_image").fancybox({

        fitToView: true,
        openEffect: 'elastic',
        closeButton: true,
        overlayColor: '##99CCFF',
        modal: false,
    });

    // UPDATE CITIES DROPDOWN
    $("#country").on("change", function () {

        var countryId = $("#country").val();

        $.ajax({

            url: "CitiesOfCountry",
            type: "GET",
            data: { countryId: countryId },
            success: function (response) {

                $("#city").empty();

                for (var city in response["cities"]) {

                    var value = city;
                    var text = response["cities"][city];
                    $("#city").append($("<option></option>").attr("value", value).text(text));
                }

                sortSelect("#city", "text", "asc");

            },
            error: function (xhr, status, error) {

                console.log(" xhr.responseText: " + xhr.responseText + " \n\nSTATUS:\n " + status + " \n\nERROR:\n " + error);
                alert("Error comunicating with server...");
            }
        });
    })
});